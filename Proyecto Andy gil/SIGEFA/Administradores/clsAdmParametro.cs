﻿using SIGEFA.Interfaces;
using SIGEFA.InterMySql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SIGEFA.Administradores
{
	class clsAdmParametro
	{
		IParametro MParametro = new MysqlParametro();

		public Boolean consultarParametroVenta(Int32 codigo)
		{
			try
			{
				return MParametro.ConsultarParametroVenta(codigo);
			}
			catch (Exception ex)
			{
				DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, 
										"Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return false;
			}
		}

		public Boolean actualizaParamentroVenta(String valorParametro)
		{
			try
			{
				return MParametro.ActualizarParametroVenta(valorParametro);
			}
			catch (Exception ex)
			{
				DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message,
										"Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return false;
			}
		}

        public Boolean actualizaDocumentoVenta(String valorParametro)
        {
            try
            {
                return MParametro.actualizaDocumentoVenta(valorParametro);
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message,
                                        "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
        }
    }
}
