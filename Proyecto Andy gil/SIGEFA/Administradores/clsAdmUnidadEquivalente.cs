﻿using SIGEFA.Interfaces;
using SIGEFA.InterMySql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGEFA.Administradores
{
    class clsAdmUnidadEquivalente
    {
        private IUnidadEquivalente iuniequ = new MysqlUnidadEquivalente();


        public DataTable listar_unidad_equivalente() {

            return iuniequ.listar_unidad_equivalente();
        }
    }
}
