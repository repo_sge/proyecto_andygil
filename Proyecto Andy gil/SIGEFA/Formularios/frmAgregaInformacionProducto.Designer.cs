﻿namespace SIGEFA.Formularios
{
	partial class frmAgregaInformacionProducto
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAgregaInformacionProducto));
			this.lblSerie = new System.Windows.Forms.Label();
			this.txtSerie = new System.Windows.Forms.TextBox();
			this.txtNChasis = new System.Windows.Forms.TextBox();
			this.lblNroChasis = new System.Windows.Forms.Label();
			this.txtModelo = new System.Windows.Forms.TextBox();
			this.lblModelo = new System.Windows.Forms.Label();
			this.txtMarca = new System.Windows.Forms.TextBox();
			this.lblMarca = new System.Windows.Forms.Label();
			this.txtColor = new System.Windows.Forms.TextBox();
			this.lblColor = new System.Windows.Forms.Label();
			this.superValidator1 = new DevComponents.DotNetBar.Validator.SuperValidator();
			this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
			this.highlighter1 = new DevComponents.DotNetBar.Validator.Highlighter();
			this.btnGuardar = new System.Windows.Forms.Button();
			this.btnCancelar = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
			this.SuspendLayout();
			// 
			// lblSerie
			// 
			this.lblSerie.AutoSize = true;
			this.lblSerie.Location = new System.Drawing.Point(22, 17);
			this.lblSerie.Name = "lblSerie";
			this.lblSerie.Size = new System.Drawing.Size(156, 13);
			this.lblSerie.TabIndex = 0;
			this.lblSerie.Text = "SERIE DE MOTOR O CHASIS:";
			// 
			// txtSerie
			// 
			this.txtSerie.Location = new System.Drawing.Point(25, 33);
			this.txtSerie.Name = "txtSerie";
			this.txtSerie.Size = new System.Drawing.Size(310, 20);
			this.txtSerie.TabIndex = 1;
			this.txtSerie.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSerie_KeyDown);
			this.txtSerie.Validating += new System.ComponentModel.CancelEventHandler(this.txtSerie_Validating);
			// 
			// txtNChasis
			// 
			this.txtNChasis.Location = new System.Drawing.Point(25, 80);
			this.txtNChasis.Name = "txtNChasis";
			this.txtNChasis.Size = new System.Drawing.Size(310, 20);
			this.txtNChasis.TabIndex = 3;
			this.txtNChasis.Validating += new System.ComponentModel.CancelEventHandler(this.txtNChasis_Validating);
			// 
			// lblNroChasis
			// 
			this.lblNroChasis.AutoSize = true;
			this.lblNroChasis.Location = new System.Drawing.Point(22, 64);
			this.lblNroChasis.Name = "lblNroChasis";
			this.lblNroChasis.Size = new System.Drawing.Size(82, 13);
			this.lblNroChasis.TabIndex = 2;
			this.lblNroChasis.Text = "Nº DE CHASIS:";
			// 
			// txtModelo
			// 
			this.txtModelo.Location = new System.Drawing.Point(25, 130);
			this.txtModelo.Name = "txtModelo";
			this.txtModelo.Size = new System.Drawing.Size(310, 20);
			this.txtModelo.TabIndex = 5;
			this.txtModelo.Validating += new System.ComponentModel.CancelEventHandler(this.txtModelo_Validating);
			// 
			// lblModelo
			// 
			this.lblModelo.AutoSize = true;
			this.lblModelo.Location = new System.Drawing.Point(22, 114);
			this.lblModelo.Name = "lblModelo";
			this.lblModelo.Size = new System.Drawing.Size(56, 13);
			this.lblModelo.TabIndex = 4;
			this.lblModelo.Text = "MODELO:";
			// 
			// txtMarca
			// 
			this.txtMarca.Location = new System.Drawing.Point(25, 177);
			this.txtMarca.Name = "txtMarca";
			this.txtMarca.Size = new System.Drawing.Size(310, 20);
			this.txtMarca.TabIndex = 7;
			this.txtMarca.Validating += new System.ComponentModel.CancelEventHandler(this.txtMarca_Validating);
			// 
			// lblMarca
			// 
			this.lblMarca.AutoSize = true;
			this.lblMarca.Location = new System.Drawing.Point(22, 161);
			this.lblMarca.Name = "lblMarca";
			this.lblMarca.Size = new System.Drawing.Size(48, 13);
			this.lblMarca.TabIndex = 6;
			this.lblMarca.Text = "MARCA:";
			// 
			// txtColor
			// 
			this.txtColor.Location = new System.Drawing.Point(25, 224);
			this.txtColor.Name = "txtColor";
			this.txtColor.Size = new System.Drawing.Size(310, 20);
			this.txtColor.TabIndex = 9;
			this.txtColor.Validating += new System.ComponentModel.CancelEventHandler(this.txtColor_Validating);
			// 
			// lblColor
			// 
			this.lblColor.AutoSize = true;
			this.lblColor.Location = new System.Drawing.Point(22, 208);
			this.lblColor.Name = "lblColor";
			this.lblColor.Size = new System.Drawing.Size(47, 13);
			this.lblColor.TabIndex = 8;
			this.lblColor.Text = "COLOR:";
			// 
			// superValidator1
			// 
			this.superValidator1.ContainerControl = this;
			this.superValidator1.ErrorProvider = this.errorProvider1;
			this.superValidator1.Highlighter = this.highlighter1;
			// 
			// errorProvider1
			// 
			this.errorProvider1.ContainerControl = this;
			this.errorProvider1.Icon = ((System.Drawing.Icon)(resources.GetObject("errorProvider1.Icon")));
			// 
			// highlighter1
			// 
			this.highlighter1.ContainerControl = this;
			// 
			// btnGuardar
			// 
			this.btnGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnGuardar.Image = global::SIGEFA.Properties.Resources.save;
			this.btnGuardar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnGuardar.Location = new System.Drawing.Point(139, 262);
			this.btnGuardar.Name = "btnGuardar";
			this.btnGuardar.Size = new System.Drawing.Size(93, 28);
			this.btnGuardar.TabIndex = 10;
			this.btnGuardar.Text = "GUARDAR";
			this.btnGuardar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.btnGuardar.UseVisualStyleBackColor = true;
			this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
			// 
			// btnCancelar
			// 
			this.btnCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnCancelar.Image = global::SIGEFA.Properties.Resources.x_button;
			this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnCancelar.Location = new System.Drawing.Point(238, 262);
			this.btnCancelar.Name = "btnCancelar";
			this.btnCancelar.Size = new System.Drawing.Size(97, 28);
			this.btnCancelar.TabIndex = 11;
			this.btnCancelar.Text = "CANCELAR";
			this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.btnCancelar.UseVisualStyleBackColor = true;
			this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
			// 
			// frmAgregaInformacionProducto
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(368, 312);
			this.ControlBox = false;
			this.Controls.Add(this.btnCancelar);
			this.Controls.Add(this.btnGuardar);
			this.Controls.Add(this.txtColor);
			this.Controls.Add(this.lblColor);
			this.Controls.Add(this.txtMarca);
			this.Controls.Add(this.lblMarca);
			this.Controls.Add(this.txtModelo);
			this.Controls.Add(this.lblModelo);
			this.Controls.Add(this.txtNChasis);
			this.Controls.Add(this.lblNroChasis);
			this.Controls.Add(this.txtSerie);
			this.Controls.Add(this.lblSerie);
			this.DoubleBuffered = true;
			this.Name = "frmAgregaInformacionProducto";
			this.ShowIcon = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "AGREGUE INFORMACIÓN DEL PRODUCTO";
			this.Load += new System.EventHandler(this.frmAgregaInformacionProducto_Load);
			((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblSerie;
		private System.Windows.Forms.TextBox txtSerie;
		private System.Windows.Forms.TextBox txtNChasis;
		private System.Windows.Forms.Label lblNroChasis;
		private System.Windows.Forms.TextBox txtModelo;
		private System.Windows.Forms.Label lblModelo;
		private System.Windows.Forms.TextBox txtMarca;
		private System.Windows.Forms.Label lblMarca;
		private System.Windows.Forms.TextBox txtColor;
		private System.Windows.Forms.Label lblColor;
		private System.Windows.Forms.Button btnGuardar;
		private DevComponents.DotNetBar.Validator.SuperValidator superValidator1;
		private System.Windows.Forms.ErrorProvider errorProvider1;
		private DevComponents.DotNetBar.Validator.Highlighter highlighter1;
		private System.Windows.Forms.Button btnCancelar;
	}
}