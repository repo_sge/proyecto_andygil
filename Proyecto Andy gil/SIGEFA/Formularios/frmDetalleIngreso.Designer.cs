﻿namespace SIGEFA.Formularios
{
    partial class frmDetalleIngreso
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDetalleIngreso));
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.txtUltimoPrecioCompra = new System.Windows.Forms.TextBox();
			this.label12 = new System.Windows.Forms.Label();
			this.checkProrrateo = new System.Windows.Forms.CheckBox();
			this.cmbUnidad = new System.Windows.Forms.ComboBox();
			this.txtReferencia = new System.Windows.Forms.TextBox();
			this.txtDscto3 = new System.Windows.Forms.TextBox();
			this.label11 = new System.Windows.Forms.Label();
			this.txtDscto2 = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.txtPrecioNeto = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.txtDscto1 = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.txtPrecio = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.txtCantidad = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.txtStock = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.txtControlStock = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.txtDescripcion = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.txtUnidad = new System.Windows.Forms.TextBox();
			this.txtCodigo = new System.Windows.Forms.TextBox();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.btnSalir = new System.Windows.Forms.Button();
			this.btnGuardar = new System.Windows.Forms.Button();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.pictureBox1);
			this.groupBox1.Controls.Add(this.txtUltimoPrecioCompra);
			this.groupBox1.Controls.Add(this.label12);
			this.groupBox1.Controls.Add(this.checkProrrateo);
			this.groupBox1.Controls.Add(this.cmbUnidad);
			this.groupBox1.Controls.Add(this.txtReferencia);
			this.groupBox1.Controls.Add(this.txtDscto3);
			this.groupBox1.Controls.Add(this.label11);
			this.groupBox1.Controls.Add(this.txtDscto2);
			this.groupBox1.Controls.Add(this.label10);
			this.groupBox1.Controls.Add(this.txtPrecioNeto);
			this.groupBox1.Controls.Add(this.label9);
			this.groupBox1.Controls.Add(this.txtDscto1);
			this.groupBox1.Controls.Add(this.label7);
			this.groupBox1.Controls.Add(this.txtPrecio);
			this.groupBox1.Controls.Add(this.label8);
			this.groupBox1.Controls.Add(this.label6);
			this.groupBox1.Controls.Add(this.txtCantidad);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.txtStock);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.txtControlStock);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.txtDescripcion);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.groupBox1.Location = new System.Drawing.Point(12, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(829, 180);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Ingresar Artículo";
			// 
			// pictureBox1
			// 
			this.pictureBox1.BackgroundImage = global::SIGEFA.Properties.Resources.question;
			this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.pictureBox1.Location = new System.Drawing.Point(298, 142);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(26, 22);
			this.pictureBox1.TabIndex = 24;
			this.pictureBox1.TabStop = false;
			this.toolTip1.SetToolTip(this.pictureBox1, "MARQUE LA OPCIÓN PRORRATEO PARA CALCULAR EL PRECIO UNITARIO EN BASE A LA CANTIDAD" +
        " Y EL PRECIO TOTAL");
			// 
			// txtUltimoPrecioCompra
			// 
			this.txtUltimoPrecioCompra.Enabled = false;
			this.txtUltimoPrecioCompra.Location = new System.Drawing.Point(12, 142);
			this.txtUltimoPrecioCompra.Name = "txtUltimoPrecioCompra";
			this.txtUltimoPrecioCompra.Size = new System.Drawing.Size(162, 22);
			this.txtUltimoPrecioCompra.TabIndex = 23;
			this.txtUltimoPrecioCompra.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label12.Location = new System.Drawing.Point(12, 123);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(163, 16);
			this.label12.TabIndex = 22;
			this.label12.Text = "Último Precio Compra:";
			// 
			// checkProrrateo
			// 
			this.checkProrrateo.AutoSize = true;
			this.checkProrrateo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.checkProrrateo.Location = new System.Drawing.Point(189, 145);
			this.checkProrrateo.Name = "checkProrrateo";
			this.checkProrrateo.Size = new System.Drawing.Size(103, 17);
			this.checkProrrateo.TabIndex = 21;
			this.checkProrrateo.Text = "PRORRATEO";
			this.checkProrrateo.UseVisualStyleBackColor = true;
			this.checkProrrateo.CheckedChanged += new System.EventHandler(this.checkProrrateo_CheckedChanged);
			// 
			// cmbUnidad
			// 
			this.cmbUnidad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbUnidad.FormattingEnabled = true;
			this.cmbUnidad.Location = new System.Drawing.Point(528, 39);
			this.cmbUnidad.Name = "cmbUnidad";
			this.cmbUnidad.Size = new System.Drawing.Size(140, 24);
			this.cmbUnidad.TabIndex = 3;
			this.cmbUnidad.SelectionChangeCommitted += new System.EventHandler(this.cmbUnidad_SelectionChangeCommitted);
			// 
			// txtReferencia
			// 
			this.txtReferencia.BackColor = System.Drawing.Color.PeachPuff;
			this.txtReferencia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtReferencia.Location = new System.Drawing.Point(12, 41);
			this.txtReferencia.Name = "txtReferencia";
			this.txtReferencia.Size = new System.Drawing.Size(100, 22);
			this.txtReferencia.TabIndex = 1;
			this.txtReferencia.TextChanged += new System.EventHandler(this.txtReferencia_TextChanged);
			this.txtReferencia.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtReferencia_KeyDown);
			this.txtReferencia.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtReferencia_KeyPress);
			this.txtReferencia.Leave += new System.EventHandler(this.txtReferencia_Leave);
			// 
			// txtDscto3
			// 
			this.txtDscto3.Location = new System.Drawing.Point(528, 94);
			this.txtDscto3.Name = "txtDscto3";
			this.txtDscto3.Size = new System.Drawing.Size(97, 22);
			this.txtDscto3.TabIndex = 10;
			this.txtDscto3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDscto3_KeyPress);
			this.txtDscto3.Leave += new System.EventHandler(this.txtDscto3_Leave);
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label11.Location = new System.Drawing.Point(525, 78);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(85, 16);
			this.label11.TabIndex = 20;
			this.label11.Text = "% Dscto 3 :";
			// 
			// txtDscto2
			// 
			this.txtDscto2.Location = new System.Drawing.Point(420, 94);
			this.txtDscto2.Name = "txtDscto2";
			this.txtDscto2.Size = new System.Drawing.Size(102, 22);
			this.txtDscto2.TabIndex = 9;
			this.txtDscto2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDscto2_KeyPress);
			this.txtDscto2.Leave += new System.EventHandler(this.txtDscto2_Leave);
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label10.Location = new System.Drawing.Point(420, 75);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(85, 16);
			this.label10.TabIndex = 18;
			this.label10.Text = "% Dscto 2 :";
			// 
			// txtPrecioNeto
			// 
			this.txtPrecioNeto.Location = new System.Drawing.Point(631, 94);
			this.txtPrecioNeto.Name = "txtPrecioNeto";
			this.txtPrecioNeto.Size = new System.Drawing.Size(183, 22);
			this.txtPrecioNeto.TabIndex = 11;
			this.txtPrecioNeto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrecioNeto_KeyPress);
			this.txtPrecioNeto.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPrecioNeto_KeyUp);
			this.txtPrecioNeto.Leave += new System.EventHandler(this.txtPrecioNeto_Leave);
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label9.Location = new System.Drawing.Point(628, 75);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(97, 16);
			this.label9.TabIndex = 16;
			this.label9.Text = "Precio Total:";
			// 
			// txtDscto1
			// 
			this.txtDscto1.Location = new System.Drawing.Point(330, 94);
			this.txtDscto1.Name = "txtDscto1";
			this.txtDscto1.Size = new System.Drawing.Size(84, 22);
			this.txtDscto1.TabIndex = 8;
			this.txtDscto1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDscto_KeyPress);
			this.txtDscto1.Leave += new System.EventHandler(this.txtDscto_Leave);
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label7.Location = new System.Drawing.Point(329, 75);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(85, 16);
			this.label7.TabIndex = 14;
			this.label7.Text = "% Dscto 1 :";
			// 
			// txtPrecio
			// 
			this.txtPrecio.Location = new System.Drawing.Point(207, 94);
			this.txtPrecio.Name = "txtPrecio";
			this.txtPrecio.Size = new System.Drawing.Size(117, 22);
			this.txtPrecio.TabIndex = 7;
			this.txtPrecio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrecio_KeyPress);
			this.txtPrecio.Leave += new System.EventHandler(this.txtPrecio_Leave);
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label8.Location = new System.Drawing.Point(204, 75);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(119, 16);
			this.label8.TabIndex = 12;
			this.label8.Text = "Precio Unitario :";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(525, 22);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(66, 16);
			this.label6.TabIndex = 10;
			this.label6.Text = "Unidad :";
			// 
			// txtCantidad
			// 
			this.txtCantidad.Location = new System.Drawing.Point(121, 94);
			this.txtCantidad.MaxLength = 15;
			this.txtCantidad.Name = "txtCantidad";
			this.txtCantidad.Size = new System.Drawing.Size(80, 22);
			this.txtCantidad.TabIndex = 6;
			this.txtCantidad.TextChanged += new System.EventHandler(this.txtCantidad_TextChanged);
			this.txtCantidad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCantidad_KeyPress);
			this.txtCantidad.Leave += new System.EventHandler(this.txtCantidad_Leave);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(118, 75);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(78, 16);
			this.label5.TabIndex = 8;
			this.label5.Text = "Cantidad :";
			// 
			// txtStock
			// 
			this.txtStock.Enabled = false;
			this.txtStock.Location = new System.Drawing.Point(677, 41);
			this.txtStock.Name = "txtStock";
			this.txtStock.Size = new System.Drawing.Size(137, 22);
			this.txtStock.TabIndex = 4;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(674, 22);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(134, 16);
			this.label4.TabIndex = 6;
			this.label4.Text = "Stock Disponible :";
			// 
			// txtControlStock
			// 
			this.txtControlStock.Location = new System.Drawing.Point(12, 94);
			this.txtControlStock.Name = "txtControlStock";
			this.txtControlStock.Size = new System.Drawing.Size(100, 22);
			this.txtControlStock.TabIndex = 5;
			this.txtControlStock.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtControlStock_KeyPress);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(12, 75);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(96, 16);
			this.label3.TabIndex = 4;
			this.label3.Text = "Serie / Lote :";
			// 
			// txtDescripcion
			// 
			this.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtDescripcion.Enabled = false;
			this.txtDescripcion.Location = new System.Drawing.Point(118, 41);
			this.txtDescripcion.Name = "txtDescripcion";
			this.txtDescripcion.Size = new System.Drawing.Size(404, 22);
			this.txtDescripcion.TabIndex = 2;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(115, 22);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(99, 16);
			this.label2.TabIndex = 2;
			this.label2.Text = "Descripción :";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(12, 22);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(92, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Referencia :";
			// 
			// txtUnidad
			// 
			this.txtUnidad.Enabled = false;
			this.txtUnidad.Location = new System.Drawing.Point(416, 205);
			this.txtUnidad.Name = "txtUnidad";
			this.txtUnidad.Size = new System.Drawing.Size(90, 20);
			this.txtUnidad.TabIndex = 14;
			this.txtUnidad.Visible = false;
			// 
			// txtCodigo
			// 
			this.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtCodigo.Location = new System.Drawing.Point(310, 205);
			this.txtCodigo.Name = "txtCodigo";
			this.txtCodigo.Size = new System.Drawing.Size(100, 20);
			this.txtCodigo.TabIndex = 1;
			this.txtCodigo.Visible = false;
			this.txtCodigo.TextChanged += new System.EventHandler(this.txtCodigo_TextChanged);
			this.txtCodigo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCodigo_KeyDown);
			this.txtCodigo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodigo_KeyPress);
			// 
			// imageList1
			// 
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
			this.imageList1.Images.SetKeyName(0, "Write Document.png");
			this.imageList1.Images.SetKeyName(1, "New Document.png");
			this.imageList1.Images.SetKeyName(2, "Remove Document.png");
			this.imageList1.Images.SetKeyName(3, "document-print.png");
			this.imageList1.Images.SetKeyName(4, "guardar-documento-icono-7840-48.png");
			this.imageList1.Images.SetKeyName(5, "exit.png");
			// 
			// btnSalir
			// 
			this.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnSalir.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
			this.btnSalir.Location = new System.Drawing.Point(721, 206);
			this.btnSalir.Name = "btnSalir";
			this.btnSalir.Size = new System.Drawing.Size(120, 32);
			this.btnSalir.TabIndex = 13;
			this.btnSalir.Text = "CANCELAR";
			this.btnSalir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.btnSalir.UseVisualStyleBackColor = true;
			this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
			// 
			// btnGuardar
			// 
			this.btnGuardar.Enabled = false;
			this.btnGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnGuardar.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardar.Image")));
			this.btnGuardar.Location = new System.Drawing.Point(535, 206);
			this.btnGuardar.Name = "btnGuardar";
			this.btnGuardar.Size = new System.Drawing.Size(180, 32);
			this.btnGuardar.TabIndex = 12;
			this.btnGuardar.Text = "AGREGAR A LISTA";
			this.btnGuardar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.btnGuardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.btnGuardar.UseVisualStyleBackColor = true;
			this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
			// 
			// frmDetalleIngreso
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.btnSalir;
			this.ClientSize = new System.Drawing.Size(855, 252);
			this.Controls.Add(this.btnSalir);
			this.Controls.Add(this.btnGuardar);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.txtCodigo);
			this.Controls.Add(this.txtUnidad);
			this.DoubleBuffered = true;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmDetalleIngreso";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Detalle Ingreso";
			this.Load += new System.EventHandler(this.frmDetalleIngreso_Load);
			this.Shown += new System.EventHandler(this.frmDetalleIngreso_Shown);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtStock;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.TextBox txtReferencia;
        public System.Windows.Forms.TextBox txtUnidad;
        public System.Windows.Forms.TextBox txtCantidad;
        public System.Windows.Forms.TextBox txtControlStock;
        public System.Windows.Forms.TextBox txtDscto1;
        public System.Windows.Forms.TextBox txtPrecio;
        public System.Windows.Forms.TextBox txtPrecioNeto;
        public System.Windows.Forms.TextBox txtDscto3;
        public System.Windows.Forms.TextBox txtDscto2;
        public System.Windows.Forms.TextBox txtDescripcion;
        public System.Windows.Forms.Button btnGuardar;
        public System.Windows.Forms.ComboBox cmbUnidad;
		private System.Windows.Forms.CheckBox checkProrrateo;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.TextBox txtUltimoPrecioCompra;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.PictureBox pictureBox1;
	}
}