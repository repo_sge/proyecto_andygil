﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SIGEFA.Administradores;
using SIGEFA.Entidades;

namespace SIGEFA.Formularios
{
    public partial class frmOrdenesdeVenta : DevComponents.DotNetBar.Office2007Form
    {
        clsAdmPedido AdmPedido = new clsAdmPedido();
        clsPedido pedido = new clsPedido();
		public static BindingSource data = new BindingSource();

		public frmOrdenesdeVenta()
        {
            InitializeComponent();
        }

        private void frmOrdenesdeVenta_Load(object sender, EventArgs e)
        {
            dtpDesde.Value = DateTime.Now.AddDays(-6);
            CargaLista();
        }

        public void CargaLista()
        {
			dgvPedidosPendientes.DataSource = data;
			data.DataSource = AdmPedido.MuestraPedidosTodos(frmLogin.iCodUser, frmLogin.iCodAlmacen, dtpDesde.Value.Date, dtpHasta.Value.Date);
            dgvPedidosPendientes.ClearSelection();
            lblCantidadRegistros.Text = "Nº DE ÓRDENES ENCONTRADAS: " + dgvPedidosPendientes.Rows.Count;
            lblCantidadRegistros.Visible = true;
        }

        private void dgvPedidosPendientes_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvPedidosPendientes.Rows.Count >= 1 && e.RowIndex != -1)
            {
				/*
                 * Obtener el codigo del pedido
                 */
                frmOrdenVenta form = new frmOrdenVenta();
                //form.MdiParent = this.MdiParent;
                form.CodPedido = dgvPedidosPendientes.CurrentRow.Cells[codigo.Name].Value.ToString();
                form.Proceso = 3;//solo lectura
                form.Show();
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void btnBusqueda_Click(object sender, EventArgs e)
        {
            CargaLista();
        }

		private void txtFiltro_TextChanged(object sender, EventArgs e)
		{
			try
			{
				if (txtFiltro.Text.Length >= 2)
				{
					data.Filter = String.Format("[{0}] like '*{1}*'", "documento", txtFiltro.Text.Trim());
				}
				else
				{
					data.Filter = String.Empty;
				}
				KeyPressEventArgs ee = new KeyPressEventArgs((char)Keys.Return);
				if (ee.KeyChar != (char)Keys.Down)
				{
					dgvPedidosPendientes.ClearSelection();
				}
			}
			catch (Exception ex)
			{
				return;
			}
		}

		private void txtFiltro_Enter(object sender, EventArgs e)
		{
			txtFiltro.SelectionStart = txtFiltro.Text.Length;
			txtFiltro.SelectionLength = 0;
		}
	}
}
