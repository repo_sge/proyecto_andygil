﻿namespace SIGEFA.Formularios
{
    partial class frmOrdenesdeVenta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmOrdenesdeVenta));
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.lblCantidadRegistros = new System.Windows.Forms.Label();
			this.btnBusqueda = new System.Windows.Forms.Button();
			this.imageList2 = new System.Windows.Forms.ImageList(this.components);
			this.btnSalir = new System.Windows.Forms.Button();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.dtpDesde = new System.Windows.Forms.DateTimePicker();
			this.dtpHasta = new System.Windows.Forms.DateTimePicker();
			this.txtFiltro = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.dgvPedidosPendientes = new System.Windows.Forms.DataGridView();
			this.documento = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.codigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Numeracion = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Pedido_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.RazonSocial = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.cliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.importe = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.fecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.responsable = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvPedidosPendientes)).BeginInit();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.lblCantidadRegistros);
			this.groupBox1.Controls.Add(this.btnBusqueda);
			this.groupBox1.Controls.Add(this.btnSalir);
			this.groupBox1.Controls.Add(this.label6);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.dtpDesde);
			this.groupBox1.Controls.Add(this.dtpHasta);
			this.groupBox1.Controls.Add(this.txtFiltro);
			this.groupBox1.Controls.Add(this.label10);
			this.groupBox1.Controls.Add(this.dgvPedidosPendientes);
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox1.Location = new System.Drawing.Point(0, 0);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(959, 382);
			this.groupBox1.TabIndex = 1;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Pendientes";
			// 
			// lblCantidadRegistros
			// 
			this.lblCantidadRegistros.AutoSize = true;
			this.lblCantidadRegistros.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblCantidadRegistros.Location = new System.Drawing.Point(6, 64);
			this.lblCantidadRegistros.Name = "lblCantidadRegistros";
			this.lblCantidadRegistros.Size = new System.Drawing.Size(251, 16);
			this.lblCantidadRegistros.TabIndex = 34;
			this.lblCantidadRegistros.Text = "Nº DE ÓRDENES ENCONTRADAS:";
			this.lblCantidadRegistros.Visible = false;
			// 
			// btnBusqueda
			// 
			this.btnBusqueda.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnBusqueda.ImageIndex = 11;
			this.btnBusqueda.ImageList = this.imageList2;
			this.btnBusqueda.Location = new System.Drawing.Point(373, 21);
			this.btnBusqueda.Name = "btnBusqueda";
			this.btnBusqueda.Size = new System.Drawing.Size(96, 33);
			this.btnBusqueda.TabIndex = 33;
			this.btnBusqueda.Text = "BUSCAR";
			this.btnBusqueda.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.btnBusqueda.UseVisualStyleBackColor = true;
			this.btnBusqueda.Click += new System.EventHandler(this.btnBusqueda_Click);
			// 
			// imageList2
			// 
			this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
			this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
			this.imageList2.Images.SetKeyName(0, "Add Green Button.png");
			this.imageList2.Images.SetKeyName(1, "Add.png");
			this.imageList2.Images.SetKeyName(2, "Remove.png");
			this.imageList2.Images.SetKeyName(3, "Write Document.png");
			this.imageList2.Images.SetKeyName(4, "New Document.png");
			this.imageList2.Images.SetKeyName(5, "Remove Document.png");
			this.imageList2.Images.SetKeyName(6, "1328102023_Copy.png");
			this.imageList2.Images.SetKeyName(7, "document-print.png");
			this.imageList2.Images.SetKeyName(8, "g-icon-new-update.png");
			this.imageList2.Images.SetKeyName(9, "refresh_256.png");
			this.imageList2.Images.SetKeyName(10, "Refresh-icon.png");
			this.imageList2.Images.SetKeyName(11, "search (1).png");
			this.imageList2.Images.SetKeyName(12, "search (5).png");
			this.imageList2.Images.SetKeyName(13, "search (6).png");
			this.imageList2.Images.SetKeyName(14, "search (8).png");
			this.imageList2.Images.SetKeyName(15, "search_top.png");
			this.imageList2.Images.SetKeyName(16, "folder_open (1).png");
			this.imageList2.Images.SetKeyName(17, "folder-open-icon.png");
			this.imageList2.Images.SetKeyName(18, "Glossy-Open-icon.png");
			this.imageList2.Images.SetKeyName(19, "Ocean Blue Open.png");
			this.imageList2.Images.SetKeyName(20, "Open (1).png");
			this.imageList2.Images.SetKeyName(21, "open_folder_green.png");
			// 
			// btnSalir
			// 
			this.btnSalir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSalir.ImageIndex = 0;
			this.btnSalir.ImageList = this.imageList1;
			this.btnSalir.Location = new System.Drawing.Point(914, 20);
			this.btnSalir.Name = "btnSalir";
			this.btnSalir.Size = new System.Drawing.Size(33, 37);
			this.btnSalir.TabIndex = 32;
			this.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.btnSalir.UseVisualStyleBackColor = true;
			this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
			// 
			// imageList1
			// 
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
			this.imageList1.Images.SetKeyName(0, "exit.png");
			this.imageList1.Images.SetKeyName(1, "pedido.png");
			this.imageList1.Images.SetKeyName(2, "carrito.png");
			this.imageList1.Images.SetKeyName(3, "delete-file-icon.png");
			this.imageList1.Images.SetKeyName(4, "DeleteRed.png");
			this.imageList1.Images.SetKeyName(5, "document_delete.png");
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(188, 30);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(57, 16);
			this.label6.TabIndex = 17;
			this.label6.Text = "Hasta :";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(6, 30);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(62, 16);
			this.label5.TabIndex = 16;
			this.label5.Text = "Desde :";
			// 
			// dtpDesde
			// 
			this.dtpDesde.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dtpDesde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtpDesde.Location = new System.Drawing.Point(74, 27);
			this.dtpDesde.Name = "dtpDesde";
			this.dtpDesde.Size = new System.Drawing.Size(100, 22);
			this.dtpDesde.TabIndex = 15;
			// 
			// dtpHasta
			// 
			this.dtpHasta.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dtpHasta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtpHasta.Location = new System.Drawing.Point(251, 27);
			this.dtpHasta.Name = "dtpHasta";
			this.dtpHasta.Size = new System.Drawing.Size(100, 22);
			this.dtpHasta.TabIndex = 14;
			// 
			// txtFiltro
			// 
			this.txtFiltro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtFiltro.Location = new System.Drawing.Point(712, 28);
			this.txtFiltro.Name = "txtFiltro";
			this.txtFiltro.Size = new System.Drawing.Size(163, 20);
			this.txtFiltro.TabIndex = 8;
			this.txtFiltro.Text = "OV001-00000";
			this.txtFiltro.TextChanged += new System.EventHandler(this.txtFiltro_TextChanged);
			this.txtFiltro.Enter += new System.EventHandler(this.txtFiltro_Enter);
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label10.Location = new System.Drawing.Point(525, 30);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(181, 16);
			this.label10.TabIndex = 7;
			this.label10.Text = "Buscar Por Nº de Orden :";
			// 
			// dgvPedidosPendientes
			// 
			this.dgvPedidosPendientes.AllowUserToAddRows = false;
			this.dgvPedidosPendientes.AllowUserToDeleteRows = false;
			this.dgvPedidosPendientes.AllowUserToOrderColumns = true;
			this.dgvPedidosPendientes.AllowUserToResizeRows = false;
			this.dgvPedidosPendientes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.dgvPedidosPendientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.dgvPedidosPendientes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.documento,
            this.codigo,
            this.Numeracion,
            this.Pedido_,
            this.RazonSocial,
            this.cliente,
            this.importe,
            this.fecha,
            this.responsable});
			this.dgvPedidosPendientes.Location = new System.Drawing.Point(0, 83);
			this.dgvPedidosPendientes.MultiSelect = false;
			this.dgvPedidosPendientes.Name = "dgvPedidosPendientes";
			this.dgvPedidosPendientes.ReadOnly = true;
			this.dgvPedidosPendientes.RowHeadersVisible = false;
			this.dgvPedidosPendientes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvPedidosPendientes.Size = new System.Drawing.Size(959, 296);
			this.dgvPedidosPendientes.TabIndex = 0;
			this.dgvPedidosPendientes.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPedidosPendientes_CellDoubleClick);
			// 
			// documento
			// 
			this.documento.DataPropertyName = "documento";
			dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			this.documento.DefaultCellStyle = dataGridViewCellStyle5;
			this.documento.HeaderText = "Número de Orden";
			this.documento.Name = "documento";
			this.documento.ReadOnly = true;
			this.documento.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.documento.Width = 150;
			// 
			// codigo
			// 
			this.codigo.DataPropertyName = "codPedido";
			this.codigo.HeaderText = "Codigo";
			this.codigo.Name = "codigo";
			this.codigo.ReadOnly = true;
			this.codigo.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.codigo.Visible = false;
			this.codigo.Width = 80;
			// 
			// Numeracion
			// 
			this.Numeracion.DataPropertyName = "numeracion";
			this.Numeracion.HeaderText = "Numeracion";
			this.Numeracion.Name = "Numeracion";
			this.Numeracion.ReadOnly = true;
			this.Numeracion.Visible = false;
			// 
			// Pedido_
			// 
			this.Pedido_.DataPropertyName = "pedido";
			this.Pedido_.HeaderText = "Pedido";
			this.Pedido_.Name = "Pedido_";
			this.Pedido_.ReadOnly = true;
			this.Pedido_.Visible = false;
			// 
			// RazonSocial
			// 
			this.RazonSocial.DataPropertyName = "cliente";
			this.RazonSocial.HeaderText = "Cliente";
			this.RazonSocial.Name = "RazonSocial";
			this.RazonSocial.ReadOnly = true;
			this.RazonSocial.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.RazonSocial.Width = 400;
			// 
			// cliente
			// 
			this.cliente.DataPropertyName = "clientebole";
			this.cliente.HeaderText = "cliente";
			this.cliente.Name = "cliente";
			this.cliente.ReadOnly = true;
			this.cliente.Visible = false;
			// 
			// importe
			// 
			this.importe.DataPropertyName = "total";
			dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
			this.importe.DefaultCellStyle = dataGridViewCellStyle6;
			this.importe.HeaderText = "Importe";
			this.importe.Name = "importe";
			this.importe.ReadOnly = true;
			this.importe.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			// 
			// fecha
			// 
			this.fecha.DataPropertyName = "fecha";
			this.fecha.HeaderText = "Fecha";
			this.fecha.Name = "fecha";
			this.fecha.ReadOnly = true;
			this.fecha.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.fecha.Width = 120;
			// 
			// responsable
			// 
			this.responsable.DataPropertyName = "responsable";
			this.responsable.HeaderText = "Responsable";
			this.responsable.Name = "responsable";
			this.responsable.ReadOnly = true;
			this.responsable.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.responsable.Width = 250;
			// 
			// frmOrdenesdeVenta
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(959, 382);
			this.Controls.Add(this.groupBox1);
			this.DoubleBuffered = true;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "frmOrdenesdeVenta";
			this.Text = "Ordenes de Venta";
			this.Load += new System.EventHandler(this.frmOrdenesdeVenta_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvPedidosPendientes)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtpDesde;
        private System.Windows.Forms.DateTimePicker dtpHasta;
        private System.Windows.Forms.TextBox txtFiltro;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView dgvPedidosPendientes;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.Button btnBusqueda;
        private System.Windows.Forms.DataGridViewTextBoxColumn documento;
        private System.Windows.Forms.DataGridViewTextBoxColumn codigo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Numeracion;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pedido_;
        private System.Windows.Forms.DataGridViewTextBoxColumn RazonSocial;
        private System.Windows.Forms.DataGridViewTextBoxColumn cliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn importe;
        private System.Windows.Forms.DataGridViewTextBoxColumn fecha;
        private System.Windows.Forms.DataGridViewTextBoxColumn responsable;
        private System.Windows.Forms.Label lblCantidadRegistros;
    }
}