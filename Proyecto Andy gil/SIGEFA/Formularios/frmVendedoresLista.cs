﻿using SIGEFA.Administradores;
using SIGEFA.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SIGEFA.Formularios
{
	public partial class frmVendedoresLista : DevComponents.DotNetBar.Office2007Form
	{
		private clsAdmUsuario admUsuario = new clsAdmUsuario();
		public static BindingSource data = new BindingSource();
		String filtro = String.Empty;
		clsUsuario usuario;
        public int proc=0;


		public frmVendedoresLista()
		{
			InitializeComponent();
		}

		private void frmVendedoresLista_Load(object sender, EventArgs e)
		{
			this.dgvVendedores.DefaultCellStyle.Font = new Font("Verdana", 10);
			dgvVendedores.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.EnableResizing; //or even better .DisableResizing. Most time consumption enum is DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders
			dgvVendedores.AutoGenerateColumns = false;
			CargarUsuariosVendedores();
			ActiveControl = txtBusqueda;
		}

		private void CargarUsuariosVendedores()
		{
			dgvVendedores.DataSource = data;
			data.DataSource = admUsuario.CargaUsuarios();
			data.Filter = String.Empty;
			filtro = String.Empty;
			dgvVendedores.ClearSelection();
		}

		private void txtBusqueda_TextChanged(object sender, EventArgs e)
		{
			try
			{
				if (txtBusqueda.Text.Length >= 1)
				{
					data.Filter = String.Format("[{0}] like '*{1}*'", "vendedor", txtBusqueda.Text.Trim());
				}
				else
				{
					data.Filter = String.Empty;
				}
			}
			catch (Exception ex)
			{
				return;
			}
		}

		private void seleccionarCliente()
		{
			try
			{
                if (proc == 2)
                {
                    var frmVenta2019 = Application.OpenForms.OfType<frmVenta2019>().Single();
                    Int32 codigoVendedor = Convert.ToInt32(dgvVendedores.CurrentRow.Cells[codUsuario.Name].Value);
                    usuario = admUsuario.MuestraUsuarioSinAdmin(codigoVendedor);
                    if (usuario != null)
                    {
                        frmVenta2019.vendedor = usuario;
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("No se encontró el vendedor",
                                        "Vendedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    var frmOrdenVenta = Application.OpenForms.OfType<frmOrdenVenta>().Single();
                    Int32 codigoVendedor = Convert.ToInt32(dgvVendedores.CurrentRow.Cells[codUsuario.Name].Value);
                    usuario = admUsuario.MuestraUsuarioSinAdmin(codigoVendedor);
                    if (usuario != null)
                    {
                        frmOrdenVenta.vendedor = usuario;
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("No se encontró el vendedor",
                                        "Vendedores", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
				
				
			}
			catch (Exception ex)
			{
				this.Close();
			}
			
		}

		private void btnSeleccionar_Click(object sender, EventArgs e)
		{
			seleccionarCliente();
		}

		private void dgvVendedores_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
		{
			seleccionarCliente();
		}

		private void dgvVendedores_KeyDown(object sender, KeyEventArgs e)
		{
			try
			{
				if (e.KeyCode == Keys.Enter)
				{
					seleccionarCliente();
				}

			}
			catch (Exception ex)
			{
				MessageBox.Show("Ocurrió un error: " + ex.Message,
								"Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void txtBusqueda_KeyDown(object sender, KeyEventArgs e)
		{
			try
			{
				if (e.KeyCode == Keys.Enter)
				{
					dgvVendedores.Focus();
				}

			}
			catch (Exception ex)
			{
				MessageBox.Show("Ocurrió un error: " + ex.Message,
								"Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}
	}
}
