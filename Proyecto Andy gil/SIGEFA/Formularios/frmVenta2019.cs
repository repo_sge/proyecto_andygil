﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SIGEFA.Administradores;
using SIGEFA.Entidades;
using Tesseract;
using System.Text.RegularExpressions;
using AForge.Imaging.Filters;
using AForge;
using SIGEFA.Reportes;
using SIGEFA.Interfaces;
using SIGEFA.Reportes.clsReportes;
using ZXing;
using DevComponents.DotNetBar;
using DevComponents.DotNetBar.Controls;
using DevComponents.DotNetBar.Validator;
using System.Diagnostics;
using SIGEFA.SunatFacElec;
using System.Threading.Tasks;
using System.Globalization;

namespace SIGEFA.Formularios
{
    public partial class frmVenta2019 : Office2007Form
    {

        public static BindingSource data = new BindingSource();
        String filtro = String.Empty;
        string label6 = "";
        string label7 = "";
        String subtotalfila = "0.00";

        clsAdmProducto AdmPro = new clsAdmProducto();
        public Decimal puInicio = 0;
        clsCliente cli = new clsCliente();
        //Int32 ov_venta = 0;//usado para OV, 1 cuando se genera nueva venta, se cambia a 2 cuando se guarda la OV


        public Int32 CodCliente, CodigoCaja, Tipo = 0;
        public String NombreCliente, CodPedido;
        clsAdmCliente AdmCli = new clsAdmCliente();

        clsFormaPago fpago = new clsFormaPago();
        clsAdmFormaPago AdmPago = new clsAdmFormaPago();
        clsAdmUnidad AdmUnidad = new clsAdmUnidad();
        clsSerie ser = new clsSerie();
        clsAdmSerie AdmSerie = new clsAdmSerie();
        clsPedido pedido = new clsPedido();
        clsCaja Caja = new clsCaja();
        clsAdmAperturaCierre AdmCaja = new clsAdmAperturaCierre();
        clsAdmPago AdmPagos = new clsAdmPago();
        List<clsNotaCredito> ncredito = new List<clsNotaCredito>();
        clsValidar ok = new clsValidar();

        Sunat MyInfoSunat;
        Reniec MyInfoReniec;
        IntRange red = new IntRange(0, 255);
        IntRange green = new IntRange(0, 255);
        IntRange blue = new IntRange(0, 255);
        public Int32 codPedidoVenta = 0;
        public Boolean esventa = false;//esta variable entra en accion cuando se selecciona una orden de la ventana pedidos pendientes.

        public Int32 CodSerie, manual = 0;

        public clsUsuario vendedor;

        private clsAdmUsuario admUsuario = new clsAdmUsuario();
        clsAdmTipoDocumento Admdoc = new clsAdmTipoDocumento();
        clsTipoDocumento doc = new clsTipoDocumento();

        public Int32 CodDocumento /*Proceso = 0*/;
        public Boolean editar = false, nuevaOV = false;

        public Boolean cargaPedido { get; set; }


        clsAdmPedido AdmPedido = new clsAdmPedido();

        public Decimal montogratuitas, montogravadas, montoexoneradas = 0, montoinafectas = 0;

        public Boolean banderagrabada, banderaexonerada, banderainafecta, banderadelete = false, bandera = false;

        public List<clsDetallePedido> detalle = new List<clsDetallePedido>();

        public mdi_Menu menu;
        bool keyHold = false;

        public string timpuesto = "";

        clsAdmParametro admParametro = new clsAdmParametro();

        DataGridViewComboBoxEditingControl dgvCombo;

        //variables para venta
        List<Int32> ListaEmpresa = new List<int>();
        List<Int32> ListaCantDoc = new List<int>();
        clsFacturaVenta venta = new clsFacturaVenta();
        clsTransaccion tran = new clsTransaccion();
        private List<clsPedido> PedidosIngresados = new List<clsPedido>();
        clsAdmFacturaVenta AdmVenta = new clsAdmFacturaVenta();


        public List<clsDetalleFacturaVenta> detalle1 = new List<clsDetalleFacturaVenta>();
        public DataTable d = null;
        DataTable tablaFiltro = null;
        List<DataTable> listatablas = new List<DataTable>();

        Facturacion facturacion = new Facturacion();

        //documentos
        clsDocumentoIdentidad documentoIdentidadSeleccionado;
        clsPago Pag = new clsPago();

        //carga combo almacenes
        clsAdmAlmacen admalma = new clsAdmAlmacen();

        public Byte[] firmadigital { get; set; }

        public String CodVenta;

        public Int32 impresion;

        clsEmpresa empresa = new clsEmpresa();
        clsAdmEmpresa admEmpresa = new clsAdmEmpresa();

        clsReporteFactura ds1 = new clsReporteFactura();
        clsConsultasExternas ext = new clsConsultasExternas();

        clsAdmTransaccion AdmTran = new clsAdmTransaccion();
        ////para recibir la insercion de lafactura en la ventana cobro
        //public Boolean ventaRecibida { get; set; }
        //// cuando esta variable cambia a false, es porq se cancelo la ventana de cobro y se anula la operacion de generacion de comprobante
        //public Boolean ventana_cobro { get; set; }



        /***********Unidadades equivalentes**********************/
        public DataTable unidadesequi { get; set; }
        private clsAdmUnidadEquivalente clsuniequ = new clsAdmUnidadEquivalente();

        private clsAdmSucursal admsucu = new clsAdmSucursal();

        List<clsFacturaVenta> lista_facturas;

        public frmVenta2019()
        {
            InitializeComponent();
        }

        public frmVenta2019(mdi_Menu menu)
        {
            InitializeComponent();
            this.menu = menu;
        }

        public void inicializarForm()
        {
            toolStripGuardar.Text = "Guardar";

            editar = false;
            cargaPedido = false;
            esventa = false;
            nuevaOV = false;

            dgvproductos.DataSource = null;
            dgvproductos.Rows.Clear();

            dgvdetalle.DataSource = null;
            dgvdetalle.Rows.Clear();

            dgvStockAlmacenes.DataSource = null;
            dgvStockAlmacenes.Rows.Clear();

            txtCodCliente.Text = "";
            txtNombreCliente.Text = "";
            txtDireccion.Text = "";
            txtCodigoVendedor.Text = "";
            txtNombreVendedor.Text = "<--  SELECCIONE UN VENDEDOR";
            dtpFecha.Text = "";
            cmbFormaPago.DataSource = null;
            cmbFormaPago.Items.Clear();

            txtLineaCredito.Text = "";
            txtLineaCreditoDisponible.Text = "";
            txtLineaCreditoUso.Text = "";

            txtgravadas.Text = "";
            txtgratuitas.Text = "";
            txtinafectas.Text = "";
            txtIGV.Text = "";
            txtValorVenta.Text = "";
            txtPrecioVenta.Text = "";


            activaPaneles(false);
            toolStripEditaov.Enabled = false;
            //toolStripImprimir.Visible = false;
            dtpFecha.Value = DateTime.Now;
            txtDocRef.Text = "OV";
            txtCodCliente.Text = "C000001";
            KeyPressEventArgs ee = new KeyPressEventArgs((char)Keys.Return);
            txtDocRef_KeyPress(txtDocRef, ee);
            BuscaCliente();
            cargaVendedor();
            CargaFormaPagos();


            textBoxX2.Text = "000";
            textBoxX1.Text = "00000000";
            venta = new clsFacturaVenta();
        }

        private void frmVenta2019_Load(object sender, EventArgs e)
        {
            pictureBox1.Image = frmLogin.logo;
            //ventaRecibida = false;
            //ventana_cobro = true;
            this.Cursor = Cursors.WaitCursor;
            labelx.Text = "descripcion";
            label11.Text = "Descripción";

            toolStripButtonPendiente.Enabled = true;

            ponerestilo();
            //CargaProductos();
            cargaVendedor();


            //if (Proceso == 3)
            //{
            //    CargaPedido();
            //    sololectura(true);
            //    //label1.Visible = true;
            //}
            //if (Proceso == 2)
            //{
            //    CargaPedido();
            //    sololectura2(true);
            //    //label1.Visible = true;
            //}

            dtpFecha.Value = DateTime.Now;
            //dtpFechaPago.Value = DateTime.Now;

            this.Cursor = Cursors.Default;
        }

        private void cargaVendedor()
        {

            Int32 codigoUsuario = Convert.ToInt32(frmLogin.iCodUser);
            /*
             * consultar usuario sin considerar usuario
             * con nombre admin
             */
            vendedor = admUsuario.MuestraUsuarioSinAdmin(codigoUsuario);
            if (vendedor != null)
            {
                txtCodigoVendedor.Text = vendedor.CodUsuario.ToString();
                txtNombreVendedor.Text = vendedor.Nombre + " " + vendedor.Apellido;
            }
            else
            {
                txtCodigoVendedor.Text = "";
                txtNombreVendedor.Text = "";
                MessageBox.Show("No se encontró ningún vendedor con el código ingresado",
                                "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ponerestilo()
        {

            dgvproductos.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249); //color a las fila siguiente
            dgvproductos.EnableHeadersVisualStyles = false;
            dgvproductos.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;

            dgvdetalle.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249); //color a las fila siguiente
            dgvdetalle.EnableHeadersVisualStyles = false;
            dgvdetalle.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;

            /*dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249); //color a las fila siguiente
            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;*/

        }



        private void CargaProductos(int codAlmacen)
        {

            this.Cursor = Cursors.WaitCursor;

            unidadesequi = clsuniequ.listar_unidad_equivalente();

            dgvproductos.AutoGenerateColumns = false;
            dgvproductos.DataSource = null;
            dgvproductos.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.EnableResizing; //or even better .DisableResizing. Most time consumption enum is DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders


            // set it to false if not needed
            dgvproductos.RowHeadersVisible = false;

            dgvproductos.DataSource = data;
            data.DataSource = AdmPro.RelacionSalidaTodo(1, codAlmacen, 1);

            //dgvproductos.DataSource = data;
            //dgvproductos.DataSource = AdmPro.RelacionSalidaTodo(1, frmLogin.iCodAlmacen, 1);
            //data.DataSource = AdmPro.RelacionSalidaTodo(1, frmLogin.iCodAlmacen, 1);

            //setUnidades();

            data.Filter = String.Empty;
            filtro = String.Empty;

            if (dgvproductos.Rows.Count > 0)
            {
                dgvproductos.Rows[0].Cells[0].Selected = false;
                dgvproductos.Rows[0].Cells[9].Selected = true;
                dgvproductos.CurrentCell = dgvproductos.CurrentRow.Cells[9];
                dgvproductos.Rows[0].Cells[9].Selected = true;
            }

            dgvproductos.ClearSelection();

            dgvproductos.Focus();
            ActiveControl = dgvproductos;

            this.Cursor = Cursors.Default;

        }

        private void setUnidades()
        {
            d = null;

            //foreach (DataGridViewRow row in dgvproductos.Rows)
            //{

            if (dgvproductos.CurrentRow != null)
            {
                DataGridViewRow row = dgvproductos.CurrentRow;

                DataGridViewComboBoxCell a = (DataGridViewComboBoxCell)(row.Cells["unidad"]);

                var lista = unidadesequi.AsEnumerable().Where(

                    x => x.Field<int>("codProducto").ToString() == row.Cells[codigo.Name].Value.ToString()

                    ).Select(x => new
                    {

                        codUnidadEquivalente = x.Field<int>("codUnidadEquivalente"),
                        codUnidadMedida = x.Field<int>("codUnidadMedida"),
                        descripcion = x.Field<string>("descripcion"),
                        precio = x.Field<decimal>("Precio")

                    }

                    ).ToList();

                a.DataSource = lista;// AdmPro.MuestraUnidadesEquivalentesVenta(Convert.ToInt32(row.Cells[codigo.Name].Value.ToString()), frmLogin.iCodSucursal);
                                     // d = AdmPro.MuestraUnidadesEquivalentesVenta(Convert.ToInt32(row.Cells[codigo.Name].Value.ToString()), frmLogin.iCodSucursal);
                                     //var results = (from myRow in d.AsEnumerable()
                                     //               select myRow.Field<int>("codUnidadEquivalente")).First();
                a.DisplayMember = "descripcion";
                a.ValueMember = "codUnidadEquivalente";
                //a.Value = lista[0].descripcion;


                //a.Value = results;

                //var results2 = (from myRow in d.AsEnumerable()
                //                select myRow.Field<decimal>("Precio")).First();

                //var results3 = (from myRow in d.AsEnumerable()
                //                select myRow.Field<int>("codUnidadMedida")).First();

                //var results4 = (from myRow in d.AsEnumerable()
                //                select myRow.Field<string>("descripcion")).First();

                //decimal u = 0;

                //u = results2;

                //row.Cells["precio"].ValueType = typeof(decimal);
                row.Cells["precio"].Value = decimal.Parse(lista[0].precio.ToString());
                row.Cells["codUnidadMedida"].Value = lista[0].codUnidadMedida.ToString();
                row.Cells["unidadnombre"].Value = lista[0].precio.ToString();
            }
            //}
        }

        private void sololectura(Boolean estado)
        {
            dtpFecha.Enabled = !estado;

            txtCodCliente.ReadOnly = estado;
            txtDocRef.ReadOnly = estado;
            txtPedido.ReadOnly = estado;
            txtBruto.ReadOnly = estado;
            txtDscto.ReadOnly = estado;
            txtValorVenta.ReadOnly = estado;
            txtIGV.ReadOnly = estado;
            txtPrecioVenta.ReadOnly = estado;
            //btnInicioOV.Enabled = estado; //!estado

            toolStripGuardar.Enabled = estado;

            //btnEditaOV.Enabled = estado;
            toolStripEditaov.Enabled = estado;
            //btnAnulaOV.Enabled = estado;
            toolStripAnulaov.Enabled = estado;
            lbDocumento.Visible = estado;

            //btnImprimirTicket.Visible = estado;
            //groupBox4.Enabled = true;

        }

        private void lbl_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox4_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void frmVenta2019_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape)
            {
                //this.Close();
            }
        }

        private void textBoxX1_KeyUp(object sender, KeyEventArgs e)
        {
            //buscar();
        }

        public void buscar()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                dgvproductos.AutoGenerateColumns = false;

                if (txtFiltro.Text.Length >= 3)
                {
                    //data.Filter = String.Format("Convert([{0}], System.String) like '*{1}*'", labelx.Text.Trim(), txtFiltro.Text);
                    var queries = new List<string>();

                    if (txtFiltro.Text != "")
                    {
                        var filterCod = txtFiltro.Text;
                        String[] cad = filterCod.Split(' ');
                        int cont = 1;
                        if (cad.Count() > 1)
                        {

                            foreach (string c in cad)
                            {
                                if (cont == 1)
                                {
                                    queries.Add(String.Format("descripcion LIKE '%{0}%'", c));
                                    var queryFilter = String.Join(" ", queries);
                                    data.Filter = queryFilter;
                                }
                                else
                                {
                                    queries.Add(String.Format("descripcion LIKE '%{0}%'", c));
                                    var queryFilter = String.Join(" AND ", queries);
                                    data.Filter = queryFilter;
                                }
                                cont++;
                            }
                        }

                        if (cad.Count() == 1)
                        {

                            //foreach (string c in cad)
                            //{
                            //queries.Add();
                            data.Filter = String.Format("descripcion LIKE '%{0}%'", filterCod);
                            //dataGridView.DataSource = bSource;

                            //}
                        }

                    }

                }
                else
                {
                    data.Filter = String.Empty;
                }

                setUnidades();
                this.Cursor = Cursors.Default;

            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void dgvproductos_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            label11.Text = dgvproductos.Columns[e.ColumnIndex].HeaderText;
            labelx.Text = dgvproductos.Columns[e.ColumnIndex].DataPropertyName;
            txtFiltro.Focus();
        }

        private void txtFiltro_KeyUp(object sender, KeyEventArgs e)
        {
            buscar();
        }

        private void dgvproductos_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {

            dgvCombo = e.Control as DataGridViewComboBoxEditingControl;

            if (dgvCombo != null)
            {
                //
                // se remueve el handler previo que pudiera tener asociado, a causa ediciones previas de la celda
                // evitando asi que se ejecuten varias veces el evento
                //
                dgvCombo.SelectedIndexChanged -= new EventHandler(dvgCombo_SelectedIndexChanged);
                dgvCombo.SelectedIndexChanged += new EventHandler(dvgCombo_SelectedIndexChanged);



            }

            if (!(e.Control is ComboBox))
            {
                DataGridViewTextBoxEditingControl dText = (DataGridViewTextBoxEditingControl)e.Control;

                dText.KeyPress -= new KeyPressEventHandler(dText_KeyPress);
                dText.KeyPress += new KeyPressEventHandler(dText_KeyPress);
            }
        }

        private void dvgCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            //
            // se recupera el valor del combo
            // a modo de ejemplo se escribe en consola el valor seleccionado
            //
            ComboBox combo = sender as ComboBox;

            //Console.WriteLine(combo.SelectedValue);

            //
            // se accede a la fila actual, para trabajr con otor de sus campos
            // en este caso se marca el check si se cambia la seleccion
            //
            //DataGridViewRow row = dgvproductos.CurrentRow;
            try
            {


                d = unidadesequi;

                if (d != null)
                {

                    if (combo.SelectedValue != null)
                    {

                        if (combo.SelectedIndex != -1)
                        {
                            if (combo.SelectedValue.ToString() != "System.Data.DataRowView")
                            {
                                if (dgvproductos.CurrentCell != null)
                                {
                                    //AdmPro.MuestraUnidadesEquivalentesVenta(Convert.ToInt32(dgvproductos.Rows[dgvproductos.CurrentCell.RowIndex].Cells[codigo.Name].Value.ToString()), frmLogin.iCodSucursal);

                                    var a = d.AsEnumerable().Where(x => x.Field<int>("codUnidadEquivalente").ToString() == combo.SelectedValue.ToString()).Select(x => x.Field<decimal>("Precio"));
                                    var b = d.AsEnumerable().Where(x => x.Field<int>("codUnidadEquivalente").ToString() == combo.SelectedValue.ToString()).Select(x => x.Field<string>("descripcion"));
                                    var c = d.AsEnumerable().Where(x => x.Field<int>("codUnidadEquivalente").ToString() == combo.SelectedValue.ToString()).Select(x => x.Field<int>("codunidadmedida"));



                                    if (a.Any())
                                    {

                                        dgvproductos.CurrentRow.Cells["precio"].Value = (a.ToList())[0].ToString();
                                        dgvproductos.CurrentRow.Cells["unidadnombre"].Value = (b.ToList())[0].ToString();
                                        dgvproductos.CurrentRow.Cells["codunidadmedida"].Value = (c.ToList())[0].ToString();

                                    }
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        private void LastColumnComboSelectionChanged(object sender, EventArgs e)
        {
            var currentcell = dgvproductos.CurrentCellAddress;
            var sendingCB = sender as DataGridViewComboBoxEditingControl;
            DataGridViewTextBoxCell cel = (DataGridViewTextBoxCell)dgvproductos.Rows[currentcell.Y].Cells[5];
            cel.Value = sendingCB.EditingControlFormattedValue.ToString();
        }


        public void dText_KeyPress(object sender, KeyPressEventArgs e)
        {


            if (!Char.IsDigit(e.KeyChar) && !Char.IsNumber(e.KeyChar) && e.KeyChar != (Char)Keys.Back && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
            if (e.KeyChar == '-')
            {
                e.Handled = true;
            }
            if (e.KeyChar == ' ')
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.' && (sender as TextBox).Text.Length == 0)
            {
                e.Handled = true;
            }

        }

        private void dgvproductos_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;


            if (dgvCombo != null)
                dgvCombo.SelectedIndexChanged -= new EventHandler(dvgCombo_SelectedIndexChanged);

            string total = "";
            try
            {
                if (dgvproductos.RowCount > 0)
                {
                    toolStripGuardar.Enabled = true;

                    if (dgvproductos.Rows[dgvproductos.CurrentCell.RowIndex].Cells["unidad"].Value == null)
                    {
                        return;
                    }

                    if (dgvproductos.CurrentRow.Cells["precio"].Value != null)
                    {
                        if (dgvproductos.CurrentRow.Cells["precio"].Value.ToString() == "")
                        {
                            //CargaProductos(cboFamilia.SelectedValue.ToString());
                            this.Cursor = Cursors.Default;
                            return;
                        }
                        else
                        {
                            if (Convert.ToDecimal(dgvproductos.CurrentRow.Cells["precio"].Value) <= 0)
                            {
                                //CargaProductos(cboFamilia.SelectedValue.ToString());
                                this.Cursor = Cursors.Default;
                                return;
                            }
                        }
                    }


                    switch (e.ColumnIndex)
                    {
                        case 12: // celda total

                            // total = CalcularCantidad();

                            if (total != "")
                            {
                                if (Convert.ToDecimal(total) <= 0)
                                {
                                    this.Cursor = Cursors.Default;
                                    return;
                                }
                            }
                            if (total == "")
                            {
                                this.Cursor = Cursors.Default;
                                return;
                            }

                            AgregaDetalleaGrilla(e.RowIndex);





                            break;

                        case 9: // celda cantidad



                            total = CalcularTotal();


                            if (total != "")
                            {
                                if (Convert.ToDecimal(total) <= 0)
                                {

                                    this.Cursor = Cursors.Default;
                                    return;
                                }
                            }
                            if (total == "")
                            {

                                this.Cursor = Cursors.Default;
                                return;
                            }

                            AgregaDetalleaGrilla(e.RowIndex);



                            break;
                    }


                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show(ex.Message, "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this.Cursor = Cursors.Default;
        }


        public String CalcularTotal()
        {

            String cantidad = "0.00";
            subtotalfila = "0.00";

            try
            {

                if (dgvproductos.CurrentRow.Cells["stockdisponible"].Value != null)
                {
                    if (dgvproductos.CurrentRow.Cells["stockdisponible"].Value.ToString() != "")
                    {
                        if (Convert.ToDecimal(dgvproductos.CurrentRow.Cells["stockdisponible"].Value) > 0)
                        {

                            if (dgvproductos.CurrentRow.Cells["cant"].Value != null)
                            {

                                if (dgvproductos.CurrentRow.Cells["cant"].Value.ToString() != "")
                                {
                                    if (Convert.ToDecimal(dgvproductos.CurrentRow.Cells["cant"].Value) > 0)
                                    {

                                        subtotalfila = String.Format("{0:##,##0.00}",
                                        (Convert.ToDecimal(dgvproductos.CurrentRow.Cells["cant"].Value) * Convert.ToDecimal(dgvproductos.CurrentRow.Cells["precio"].Value)));

                                        cantidad = decimal.Round(decimal.Parse(subtotalfila), 2).ToString();

                                        dgvproductos.CurrentRow.Cells["total"].Value = subtotalfila;
                                        dgvproductos.CurrentRow.DefaultCellStyle.BackColor = Color.LemonChiffon;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show(ex.Message, "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            this.Cursor = Cursors.Default;
            return subtotalfila;

        }

        private void txtFiltro_Leave(object sender, EventArgs e)
        {
            dgvproductos.Focus();
            ActiveControl = dgvproductos;
        }

        private void txtFiltro_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Down)
            {
                dgvproductos.Focus();
                ActiveControl = dgvproductos;

                if (dgvproductos.Rows.Count > 0)
                {
                    dgvproductos.Rows[0].Cells[0].Selected = false;
                    dgvproductos.Rows[0].Cells[9].Selected = true;
                    dgvproductos.CurrentCell = dgvproductos.CurrentRow.Cells[9];

                }
            }

            if (e.KeyData == Keys.Enter)
            {
                string f = txtFiltro.Text.Trim();


            }


        }


        private void AgregaDetalleaGrilla(int index)
        {

            if (!dgvdetalle.Rows.Cast<DataGridViewRow>().
                                                    Where(x => int.Parse(x.Cells["codproducto"].Value.ToString()) ==
                                                          int.Parse(dgvproductos.Rows[index].Cells["codigo"].Value.ToString())).Any())
            {


                Decimal bruto, montodescuento, valorventa, igv, precioventa, precioreal, valorreal, factorigv, maxPorcDescto;
                Decimal cantidad = 0;
                Decimal preciou = 0;
                Decimal total = 0;
                int tipoImpuesto = 0;

                cantidad = Convert.ToDecimal(dgvproductos.CurrentRow.Cells["Cant"].Value);
                preciou = Convert.ToDecimal(dgvproductos.CurrentRow.Cells["precio"].Value);
                total = Convert.ToDecimal(dgvproductos.CurrentRow.Cells["Total"].Value);

                tipoImpuesto = Convert.ToInt32(dgvproductos.CurrentRow.Cells[codtimpuesto.Name].Value);

                puInicio = preciou;
                bruto = cantidad * puInicio;

                montodescuento = 0;


                if (tipoImpuesto == 1)
                {
                    //DEBE TOMAR EL DATO DE IGV DE LA CONFIGURACION DEL SISTEMA   
                    precioventa = total;
                    factorigv = Convert.ToDecimal(frmLogin.Configuracion.IGV / 100 + 1);
                    valorventa = precioventa / factorigv;

                }
                else
                {

                    valorventa = total;
                    precioventa = valorventa;



                }



                precioreal = precioventa / cantidad;
                valorreal = valorventa / cantidad;
                igv = precioventa - valorventa;


                maxPorcDescto = 0;

                //codtipoarticulo = Convert.ToInt32(dgvproductos.CurrentRow.Cells[codTipoArticulo_lista.Name].Value.ToString());



                dgvdetalle.Rows.Add("0", dgvproductos.CurrentRow.Cells[codigo.Name].Value.ToString(), dgvproductos.CurrentRow.Cells[referencia.Name].Value.ToString(), dgvproductos.CurrentRow.Cells[descripcion.Name].Value.ToString(),
                                            dgvproductos.CurrentRow.Cells[codunidadmedida.Name].Value.ToString(), dgvproductos.CurrentRow.Cells[unidadnombre.Name].Value.ToString(), cantidad,
                                            puInicio, bruto, 0, 0, 0, montodescuento,
                                            valorventa, igv, precioventa, valorreal, precioreal, precioventa, 0,
                                            0, dgvproductos.CurrentRow.Cells[codsunatimpuesto.Name].Value.ToString(), cmbAlmacenes.SelectedValue.ToString(), "", "", dgvproductos.CurrentRow.Cells[codEmp.Name].Value.ToString());
                //dgvdetalle.CurrentCell = dgvdetalle.Rows[dgvdetalle.Rows.Count - 1].Cells[0];

                calculatotales();
                montosventa();

                //CargaProductos();
            }
            else
            {

                MessageBox.Show("El producto ya se encuentra agregado, primero elimine y vuelva a agregarlo");

            }
        }




        public void calculatotales()
        {
            Decimal bruto = 0;
            Decimal descuen = 0;
            Decimal valor = 0;
            Decimal preciovent = 0;
            Decimal igvt = 0;

            montogravadas = 0;
            montoexoneradas = 0;
            montogratuitas = 0;
            montoinafectas = 0;

            foreach (DataGridViewRow row in dgvdetalle.Rows)
            {
                bruto = bruto + Convert.ToDecimal(row.Cells[importe.Name].Value);
                descuen = descuen + Convert.ToDecimal(row.Cells[montodscto.Name].Value);
                valor = valor + Convert.ToDecimal(row.Cells[valorventa.Name].Value);
                preciovent = preciovent + Convert.ToDecimal(row.Cells[precioventa.Name].Value);
                igvt = igvt + Convert.ToDecimal(row.Cells[igv.Name].Value);


                timpuesto = row.Cells[Tipoimpuesto.Name].Value.ToString();

                if (timpuesto == "21") // gratuitas
                {
                    montogratuitas = montogratuitas + (Convert.ToDecimal(row.Cells[preciounit.Name].Value) * Convert.ToDecimal(row.Cells[cantidad.Name].Value));
                }

                if (timpuesto == "10" || timpuesto == "11" ||
                timpuesto == "12" || timpuesto == "13" ||
                timpuesto == "14" || timpuesto == "15" ||
                timpuesto == "16" || timpuesto == "17")   // gravadas
                {
                    montogravadas = montogravadas + (Convert.ToDecimal(row.Cells[valorventa.Name].Value));
                }

                if (timpuesto == "20") // exoneradas
                {
                    montoexoneradas = montoexoneradas + (Convert.ToDecimal(row.Cells[preciounit.Name].Value) * Convert.ToDecimal(row.Cells[cantidad.Name].Value));
                }

                if (timpuesto == "30" || timpuesto == "31" ||
                timpuesto == "32" || timpuesto == "33" ||
                timpuesto == "34" || timpuesto == "35" ||
                timpuesto == "36") // inafectas
                {
                    montoinafectas = montoinafectas + (Convert.ToDecimal(row.Cells[preciounit.Name].Value) * Convert.ToDecimal(row.Cells[cantidad.Name].Value));
                }
            }

            txtBruto.Text = String.Format("{0:#,##0.00}", bruto);
            txtDscto.Text = String.Format("{0:#,##0.00}", descuen);
            txtValorVenta.Text = String.Format("{0:#,##0.00}", valor);
            txtIGV.Text = String.Format("{0:#,##0.00}", igvt);
            txtPrecioVenta.Text = String.Format("{0:#,##0.00}", preciovent);

            txtgravadas.Text = String.Format("{0:#,##0.00}", montogravadas);
            txtinafectas.Text = String.Format("{0:#,##0.00}", montoinafectas);
            txtgratuitas.Text = String.Format("{0:#,##0.00}", montogratuitas);
            txtexoneradas.Text = String.Format("{0:#,##0.00}", montoexoneradas);

        }


        private void CargaCliente()
        {
            try
            {
                cli = AdmCli.MuestraCliente(CodCliente);

                if (cli.RucDni.Length == 8)
                {
                    chkBoleta.Checked = true;
                    cli.DocumentoIdentidad = new clsDocumentoIdentidad();
                    cli.DocumentoIdentidad.CodDocumentoIdentidad = 1;
                    cli.DocumentoIdentidad.CodigoSunat = 1;
                }
                else if (cli.RucDni.Length == 11)
                {
                    chkFactura.Checked = true;

                    cli.DocumentoIdentidad = new clsDocumentoIdentidad();
                    cli.DocumentoIdentidad.CodDocumentoIdentidad = 3;
                    cli.DocumentoIdentidad.CodigoSunat = 6;
                }
                txtCodCliente.Text = cli.RucDni;
                txtNombreCliente.Text = cli.RazonSocial;
                txtDireccion.Text = cli.DireccionLegal;

                CargaCreditoCliente(cli);

            }
            catch (Exception a) { MessageBox.Show(a.Message); }

        }

        private void groupBox5_Enter(object sender, EventArgs e)
        {

        }

        private void cmbFormaPago_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cmbFormaPago.SelectedIndex != -1)
            {
                fpago = AdmPago.CargaFormaPago(Convert.ToInt32(cmbFormaPago.SelectedValue));
                dtpFechaPago.Value = dtpFecha.Value.AddDays(fpago.Dias);
            }
        }

        private void txtCodCliente_KeyPress(object sender, KeyPressEventArgs e)
        {

            this.Cursor = Cursors.WaitCursor;

            ok.enteros(e);
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                try
                {
                    //Cursor = Cursors.WaitCursor;
                    switch (this.txtCodCliente.Text.Length)
                    {
                        case 1:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo un digito Ingresado",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            txtCodCliente.SelectAll();
                            txtCodCliente.Focus();
                            break;

                        case 2:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo dos digitos Ingresados",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            txtCodCliente.SelectAll();
                            txtCodCliente.Focus();
                            break;

                        case 3:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo tres digitos Ingresados",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            txtCodCliente.SelectAll();
                            txtCodCliente.Focus();
                            break;

                        case 4:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo cuatro digitos Ingresados",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            txtCodCliente.SelectAll();
                            txtCodCliente.Focus();
                            break;

                        case 5:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo cinco digitos Ingresados",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            txtCodCliente.SelectAll();
                            txtCodCliente.Focus();
                            break;

                        case 6:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo seis digitos Ingresados",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            txtCodCliente.SelectAll();
                            txtCodCliente.Focus();
                            break;

                        case 7:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo siete digitos Ingresados",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            txtCodCliente.SelectAll();
                            txtCodCliente.Focus();
                            break;

                        case 8:
                            cli = AdmCli.ConsultaCliente(txtCodCliente.Text);

                            if (cli != null)
                            {
                                CodCliente = cli.CodCliente;
                                txtNombreCliente.Text = cli.RazonSocial;
                                txtDireccion.Text = cli.DireccionLegal;

                                cli.DocumentoIdentidad = new clsDocumentoIdentidad();
                                cli.DocumentoIdentidad.CodDocumentoIdentidad = 1;
                                cli.DocumentoIdentidad.CodigoSunat = 1;


                                CargaCreditoCliente(cli);
                            }
                            else
                            {
                                MessageBox.Show("El DNI ingresado no se encuentra registrado");
                                CodCliente = 0;
                            }

                            chkBoleta.Checked = true;
                            cmbFormaPago.Enabled = true;
                            break;

                        case 9:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Ingreso nueve digitos ",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            break;

                        case 10:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Ingreso diez digitos ",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            txtCodCliente.SelectAll();
                            txtCodCliente.Focus();
                            break;

                        case 11:
                            cli = AdmCli.ConsultaCliente(txtCodCliente.Text);
                            if (cli != null)
                            {
                                cli = AdmCli.MuestraCliente(cli.CodCliente);
                                if (cli != null)
                                {
                                    CodCliente = cli.CodCliente;
                                    txtNombreCliente.Text = cli.RazonSocial;
                                    txtDireccion.Text = cli.DireccionLegal;


                                    cli.DocumentoIdentidad = new clsDocumentoIdentidad();
                                    cli.DocumentoIdentidad.CodDocumentoIdentidad = 3;
                                    cli.DocumentoIdentidad.CodigoSunat = 6;

                                    //CargaCreditoCliente(cli);
                                }
                                else
                                {
                                    CargarImagenSunat();
                                    CargaRUC();
                                    CodCliente = 0;
                                }
                            }
                            else
                            {
                                CargarImagenSunat();
                                CargaRUC();
                                CodCliente = 0;
                            }

                            CargaCreditoCliente(cli);

                            chkFactura.Checked = true;
                            cmbFormaPago.Enabled = true;
                            break;

                        default:
                            ValidaLongitud();
                            break;
                    }


                    txtCodigoVendedor.Focus();
                    //cbFamilia.Select();       
                    this.Cursor = Cursors.Default;

                }
                catch (Exception ex)
                {
                    this.Cursor = Cursors.Default;
                    MessageBox.Show(ex.Message);
                    CargarImagenSunat();
                }
            }

            this.Cursor = Cursors.Default;
        }

        private void CargaRUC()
        {
            if (this.txtCodCliente.Text.Length == 11)
            {
                LeerDatos();
            }
        }


        public void CargaCreditoCliente(clsCliente cli)
        {

            if (cli != null)
            {
                if (cli.Moneda == 1)
                {
                    txtLineaCredito.Text = String.Format("{0:#,##0.00}", cli.LineaCredito);
                    txtLineaCreditoDisponible.Text = String.Format("{0:#,##0.00}", cli.LineaCreditoDisponible);
                    txtLineaCreditoUso.Text = String.Format("{0:#,##0.00}", cli.LineaCreditoUsado);
                    txttasa.Text = String.Format("{0:#,##0.00}", cli.Tasa);
                    lbLineaCredito.Text = "Línea de Crédito (S/.):";
                    label23.Text = "Línea Disponible (S/.):";
                    label25.Text = "Línea C. en Uso (S/.):";
                    if (cli.LineaCredito > 0) { cmbFormaPago.Enabled = true; } else { cmbFormaPago.Enabled = false; }
                }
                else
                {
                    txtLineaCredito.Text = String.Format("{0:#,##0.00}", (cli.LineaCredito / Convert.ToDecimal(mdi_Menu.tc_hoy)));
                    txtLineaCreditoDisponible.Text = String.Format("{0:#,##0.00}", (cli.LineaCreditoDisponible / Convert.ToDecimal(mdi_Menu.tc_hoy)));
                    txtLineaCreditoUso.Text = Text = String.Format("{0:#,##0.00}", (cli.LineaCreditoUsado / Convert.ToDecimal(mdi_Menu.tc_hoy)));
                    lbLineaCredito.Text = "Línea de Crédito ($.):";
                    label23.Text = "Línea Disponible ($.):";
                    label25.Text = "Línea C. en Uso ($.):";
                    if (cli.LineaCredito > 0) { cmbFormaPago.Enabled = true; } else { cmbFormaPago.Enabled = false; }
                }
                if (cli.FormaPago != 0)
                {
                    cmbFormaPago.SelectedValue = cli.FormaPago; //cli.FormaPago  --   6 Contado
                    EventArgs ee = new EventArgs();
                    cmbFormaPago_SelectionChangeCommitted(cmbFormaPago, ee);
                }
                else
                {
                    dtpFechaPago.Value = DateTime.Today;
                }
            }

            else
            {

                cli = AdmCli.ConsultaCliente(txtCodCliente.Text);
                if (cli != null)
                {


                    if (cli.Moneda == 1)
                    {
                        txtLineaCredito.Text = String.Format("{0:#,##0.00}", cli.LineaCredito);
                        txtLineaCreditoDisponible.Text = String.Format("{0:#,##0.00}", cli.LineaCreditoDisponible);
                        txtLineaCreditoUso.Text = String.Format("{0:#,##0.00}", cli.LineaCreditoUsado);
                        txttasa.Text = String.Format("{0:#,##0.00}", cli.Tasa);
                        lbLineaCredito.Text = "Línea de Crédito (S/.):";
                        label23.Text = "Línea Disponible (S/.):";
                        label25.Text = "Línea C. en Uso (S/.):";
                        if (cli.LineaCredito > 0) { cmbFormaPago.Enabled = true; } else { cmbFormaPago.Enabled = false; }
                    }
                    else
                    {
                        txtLineaCredito.Text = String.Format("{0:#,##0.00}", (cli.LineaCredito / Convert.ToDecimal(mdi_Menu.tc_hoy)));
                        txtLineaCreditoDisponible.Text = String.Format("{0:#,##0.00}", (cli.LineaCreditoDisponible / Convert.ToDecimal(mdi_Menu.tc_hoy)));
                        txtLineaCreditoUso.Text = Text = String.Format("{0:#,##0.00}", (cli.LineaCreditoUsado / Convert.ToDecimal(mdi_Menu.tc_hoy)));
                        lbLineaCredito.Text = "Línea de Crédito ($.):";
                        label23.Text = "Línea Disponible ($.):";
                        label25.Text = "Línea C. en Uso ($.):";
                        if (cli.LineaCredito > 0) { cmbFormaPago.Enabled = true; } else { cmbFormaPago.Enabled = false; }
                    }
                    if (cli.FormaPago != 0)
                    {
                        cmbFormaPago.SelectedValue = cli.FormaPago; //cli.FormaPago  --   6 Contado
                        EventArgs ee = new EventArgs();
                        cmbFormaPago_SelectionChangeCommitted(cmbFormaPago, ee);
                    }
                    else
                    {
                        dtpFechaPago.Value = DateTime.Today;
                    }
                }
            }
        }

        private void LeerDatos()
        {

            string cadena = "";

            //llamamos a los metodos de la libreria ConsultaReniec...
            MyInfoSunat.GetInfo(this.txtCodCliente.Text, this.txtSunat_Capchat.Text);
            switch (MyInfoSunat.GetResul)
            {
                case Sunat.Resul.Ok:
                    limpiarSunat();
                    txtCodCliente.Text = MyInfoSunat.Ruc.Trim();
                    txtDireccion.Text = MyInfoSunat.Direcion.Trim();
                    txtNombreCliente.Text = MyInfoSunat.RazonSocial.Trim();

                    if (!txtCodCliente.Text.StartsWith("1"))
                    {
                        cadena = txtDireccion.Text;
                        int indice = cadena.IndexOf('<');
                        cadena = cadena.Substring(0, cadena.Length - (cadena.Length - indice));
                        //cadena = cadena.Replace("</td>\r\n </tr>\r\n\r\n <tr>\r\n", " ");
                        txtDireccion.Text = cadena;
                    }
                    else
                    {
                        txtDireccion.Text = "-";
                    }

                    string textoOriginal = txtNombreCliente.Text;//transformación UNICODE
                    string textoNormalizado = textoOriginal.Normalize(NormalizationForm.FormD);
                    //coincide todo lo que no sean letras y números ascii o espacio
                    //y lo reemplazamos por una cadena vacía.Regex reg = new Regex("[^a-zA-Z0-9 ]");
                    Regex reg = new Regex("[^a-zA-Z0-9 ]");
                    string textoSinAcentos = reg.Replace(textoNormalizado, "");

                    txtNombreCliente.Text = textoSinAcentos;

                    //Ciudad(MyInfoSunat.Direcion);
                    BloqueaDatos();
                    break;
                case Sunat.Resul.NoResul:
                    limpiarSunat();
                    MessageBox.Show("No Existe RUC");
                    break;
                case Sunat.Resul.ErrorCapcha:
                    limpiarSunat();
                    MessageBox.Show("Ingrese imagen correctamente");
                    break;
                default:
                    MessageBox.Show("Error Desconocido");
                    break;
            }
            //CargarImagenSunat();
        }


        private void Ciudad(string Direccion)
        {
            String[] array = Direccion.Split('-');
            if (array.Length > 1)
            {
                int a = array.Length;
                String DirTemp = array[a - 3].Trim();
                DirTemp = DirTemp.TrimEnd(' ');
                String[] ArrayDir = DirTemp.Split(' ');
                int i = ArrayDir.Length;
                //cbDepartamento.Text = ArrayDir[i - 1].Trim();
                //cbProvincia.Text = array[a - 2].Trim();
                //cbDistrito.Text = array[a - 1].Trim();
            }
        }

        private void BloqueaDatos()
        {
            /*txtRUC.ReadOnly = true; */
            //txtDireccion.ReadOnly = true;
            txtNombreCliente.ReadOnly = false;
        }


        private void ValidaLongitud()
        {
            if (txtCodCliente.Text.Length == 0)
            {
                MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Ningun digito Ingresado",
                               "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
            }
            else if (txtCodCliente.Text.Length > 11)
            {
                MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Ha Ingresado " + txtCodCliente.Text.Length + " Digitos",
                               "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                txtCodCliente.SelectAll();
                txtCodCliente.Focus();
            }
        }


        private void LeerCaptchaSunat()
        {
            //string ruta = Directory.GetCurrentDirectory()+"\\tessdata";
            //string RutaArchivo = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "tessdata\\");
            try
            {
                using (var engine = new TesseractEngine(@"./tessdata", "eng", EngineMode.Default))
                {
                    using (var image = new System.Drawing.Bitmap(pbCapchatS.Image))
                    {
                        using (var pix = PixConverter.ToPix(image))
                        {
                            using (var page = engine.Process(pix))
                            {
                                var Porcentaje = String.Format("{0:P}", page.GetMeanConfidence());
                                string CaptchaTexto = page.GetText();
                                char[] eliminarChars = { '\n', ' ' };
                                CaptchaTexto = CaptchaTexto.TrimEnd(eliminarChars);
                                CaptchaTexto = CaptchaTexto.Replace(" ", string.Empty);
                                CaptchaTexto = Regex.Replace(CaptchaTexto, "[^a-zA-Z]+", string.Empty);
                                if (CaptchaTexto != string.Empty & CaptchaTexto.Length == 4)
                                    txtSunat_Capchat.Text = CaptchaTexto.ToUpper();
                                else
                                    CargarImagenSunat();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void CargarImagenSunat()
        {
            try
            {
                if (MyInfoSunat == null)
                    MyInfoSunat = new Sunat();
                this.pbCapchatS.Image = MyInfoSunat.GetCapcha;
                LeerCaptchaSunat();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CargarImagenReniec()
        {
            try
            {
                if (MyInfoReniec == null)
                    MyInfoReniec = new Reniec();
                this.pbCapchatS.Image = MyInfoReniec.GetCapcha;
                AplicacionFiltros();
                LeerCaptchaReniec();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AplicacionFiltros()
        {
            Bitmap bmp = new Bitmap(pbCapchatS.Image);
            FiltroInvertir(bmp);
            ColorFiltros();
            Bitmap bmp1 = new Bitmap(pbCapchatS.Image);
            FiltroInvertir(bmp1);
            Bitmap bmp2 = new Bitmap(pbCapchatS.Image);
            FiltroSharpen(bmp2);
        }

        private void FiltroInvertir(Bitmap bmp)
        {
            IFilter Filtro = new Invert();
            Bitmap XImage = Filtro.Apply(bmp);
            pbCapchatS.Image = XImage;
        }


        private void FiltroSharpen(Bitmap bmp)
        {
            IFilter Filtro = new Sharpen();
            Bitmap XImage = Filtro.Apply(bmp);
            pbCapchatS.Image = XImage;

        }
        private void ColorFiltros()
        {
            //Red Min - MAX
            red.Min = Math.Min(red.Max, byte.Parse("229"));
            red.Max = Math.Max(red.Min, byte.Parse("255"));
            //Verde Min - MAX
            green.Min = Math.Min(green.Max, byte.Parse("0"));
            green.Max = Math.Max(green.Min, byte.Parse("255"));
            //Azul Min - MAX
            blue.Min = Math.Min(blue.Max, byte.Parse("0"));
            blue.Max = Math.Max(blue.Min, byte.Parse("130"));
            ActualizarFiltro();
        }


        private void ActualizarFiltro()
        {
            ColorFiltering FiltroColor = new ColorFiltering();
            FiltroColor.Red = red;
            FiltroColor.Green = green;
            FiltroColor.Blue = blue;
            IFilter Filtro = FiltroColor;
            Bitmap bmp = new Bitmap(pbCapchatS.Image);
            Bitmap XImage = Filtro.Apply(bmp);
            pbCapchatS.Image = XImage;
        }


        private void LeerCaptchaReniec()
        {
            using (var engine = new TesseractEngine(@"./tessdata", "eng", EngineMode.Default))
            {
                using (var image = new System.Drawing.Bitmap(pbCapchatS.Image))
                {
                    using (var pix = PixConverter.ToPix(image))
                    {
                        using (var page = engine.Process(pix))
                        {
                            var Porcentaje = String.Format("{0:P}", page.GetMeanConfidence());
                            string CaptchaTexto = page.GetText();
                            char[] eliminarChars = { '\n', ' ' };
                            CaptchaTexto = CaptchaTexto.TrimEnd(eliminarChars);
                            CaptchaTexto = CaptchaTexto.Replace(" ", string.Empty);
                            CaptchaTexto = Regex.Replace(CaptchaTexto, "[^a-zA-Z0-9]+", string.Empty);
                            if (CaptchaTexto != string.Empty & CaptchaTexto.Length == 4)
                                txtSunat_Capchat.Text = CaptchaTexto.ToUpper();
                            //else
                            //    CargarImagenReniec();
                        }
                    }
                }
            }
        }


        private void CargaDNI()
        {
            MyInfoReniec.GetInfo(this.txtCodCliente.Text, this.txtSunat_Capchat.Text);
            switch (MyInfoReniec.GetResul)
            {
                case Reniec.Resul.Ok:
                    limpiarSunat();
                    txtCodCliente.Text = MyInfoReniec.Dni;
                    String apellidos = MyInfoReniec.ApePaterno + " " + MyInfoReniec.ApeMaterno;
                    txtNombreCliente.Text = MyInfoReniec.Nombres + " " + apellidos;
                    break;
                case Reniec.Resul.NoResul:
                    limpiarSunat();
                    MessageBox.Show("No Existe DNI");
                    break;
                case Reniec.Resul.ErrorCapcha:
                    limpiarSunat();
                    MessageBox.Show("Ingrese imagen correctamente");
                    break;
                default:
                    MessageBox.Show("Error Desconocido");
                    break;
            }
            //Comentar esta linea para consultar multiples DNI usando un solo captcha.
            CargarImagenReniec();
        }

        private void limpiarSunat()
        {
            txtNombreCliente.Text = "";
            txtSunat_Capchat.Text = string.Empty;
        }

        private void btnInicioOV_Click(object sender, EventArgs e)
        {

            limpiarVentana();

        }


        public void limpiarVentana()
        {
            editar = false;
            cargaPedido = false;
            esventa = false;
            nuevaOV = true;
            //ov_venta = 0;

            this.Cursor = Cursors.WaitCursor;
            toolStripImprimir.Enabled = false;
            this.activaPaneles(true);
            toolStripGuardar.Text = "Guardar";
            toolStripGuardar.Enabled = false;
            dtpFecha.Value = DateTime.Now;
            cmbAlmacenes.Enabled = true;
            cargaAlmacenes();
            cargaVendedor();
            CargaFormaPagos();
            //CargaNumeracionOV();

            CargaProductos(frmLogin.iCodAlmacen);

            txtgravadas.Enabled = true;
            txtexoneradas.Enabled = true;
            txtgratuitas.Enabled = true;
            txtinafectas.Enabled = true;
            txtinafectas.Enabled = true;
            txtPrecioVenta.Enabled = true;
            txtValorVenta.Enabled = true;
            txtIGV.Enabled = true;



            txtDocRef.Text = "OV";
            txtCodCliente.Text = "C000001";
            KeyPressEventArgs ee = new KeyPressEventArgs((char)Keys.Return);
            txtDocRef_KeyPress(txtDocRef, ee);
            BuscaCliente();


            dgvdetalle.Rows.Clear();
            dgvStockAlmacenes.DataSource = null;
            dgvStockAlmacenes.Rows.Clear();
            calculatotales();

            textBoxX2.Text = "000";
            textBoxX1.Text = "00000000";


            /*
            txtBruto.Text = String.Format("{0:#,##0.00}", 0);
            txtDscto.Text = String.Format("{0:#,##0.00}", 0);
            txtValorVenta.Text = String.Format("{0:#,##0.00}", 0);
            txtIGV.Text = String.Format("{0:#,##0.00}", 0);
            txtPrecioVenta.Text = String.Format("{0:#,##0.00}", 0);

            txtgratuitas.Text = String.Format("{0:#,##0.00}", 0);
            txtgratuitas.Text = String.Format("{0:#,##0.00}", 0);
            txtexoneradas.Text = String.Format("{0:#,##0.00}", 0);
            txtinafectas.Text = String.Format("{0:#,##0.00}", 0);*/

            this.Cursor = Cursors.Default;
            txtCodCliente.Enabled = true;
            txtCodCliente.ReadOnly = false;
            chkBoleta.Checked = true;
            txtNombreCliente.ReadOnly = false;
            txtNombreCliente.Enabled = true;
        }


        private void activaPaneles(Boolean estado)
        {

            //btnEditaOV.Enabled = !estado;
            toolStripEditaov.Enabled = !estado;
            toolStripGuardar.Enabled = estado;
            //btnInicioOV.Enabled = !estado;

            bandera = estado;// para saber si se activaron los controles de la venta
            groupBox2.Enabled = estado;
            groupBox4.Enabled = estado;

        }

        private void btnEditaOV_Click(object sender, EventArgs e)
        {
            toolStripImprimir.Enabled = false;
            sololectura2(false);
            cmbAlmacenes.Enabled = true;
            cargaAlmacenes();
            /*
             * Bandera a true para dejar agregar detalles al editar 
             * una orden de venta
             */
            toolStripButtonPendiente.Enabled = false;
            toolStripGuardar.Enabled = true;
            CargaProductos(frmLogin.iCodAlmacen);
            groupBox2.Enabled = true;
            groupBox4.Enabled = true;
            bandera = true;
            calculatotales();
            //Proceso = 2;
            editar = true;
            nuevaOV = false;
            toolStripGuardar.Text = "Guardar";
            txtNombreCliente.ReadOnly = false;
            txtNombreCliente.Enabled = true;
        }


        private void sololectura2(Boolean estado)//Creado para modificar OV
        {
            dtpFecha.Enabled = !estado;

            txtCodCliente.ReadOnly = estado;
            txtDocRef.ReadOnly = estado;
            txtPedido.ReadOnly = estado;

            txtValorVenta.ReadOnly = estado;
            txtIGV.ReadOnly = estado;
            txtPrecioVenta.ReadOnly = estado;
            //btnInicioOV.Enabled = false; //estado;


            //btnEditaOV.Enabled = estado;
            toolStripEditaov.Enabled = estado;
            //btnAnulaOV.Enabled = estado;
            toolStripAnulaov.Enabled = estado;
            lbDocumento.Visible = estado;

            //groupBox4.Enabled = true;

        }

        private void txtNombreCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if ((int)e.KeyChar == (int)Keys.Enter)
                {
                    if (Application.OpenForms["frmClientesLista"] != null)
                    {
                        Application.OpenForms["frmClientesLista"].Activate();
                    }
                    else
                    {
                        frmClientesLista form = new frmClientesLista();
                        form.Proceso = 7;
                        form.filtrocliente = txtNombreCliente.Text;
                        if (form.ShowDialog() == DialogResult.OK)
                        {
                            CodCliente = form.GetCodigoCliente();
                            CargaCliente();

                            if (cli != null)
                            {
                                CodCliente = cli.CodCliente;
                                txtNombreCliente.Text = cli.RazonSocial;
                                txtDireccion.Text = cli.DireccionLegal;
                                txtCodCliente.Text = cli.RucDni;
                                cli = AdmCli.ConsultaCliente(cli.RucDni);
                                if (cli.Moneda == 1)
                                {
                                    txtLineaCredito.Text = String.Format("{0:#,##0.00}", cli.LineaCredito);
                                    txtLineaCreditoDisponible.Text = String.Format("{0:#,##0.00}", cli.LineaCreditoDisponible);
                                    txtLineaCreditoUso.Text = String.Format("{0:#,##0.00}", cli.LineaCreditoUsado);
                                    txttasa.Text = String.Format("{0:#,##0.00}", cli.Tasa);
                                    lbLineaCredito.Text = "Línea de Crédito (S/.):";
                                    label23.Text = "Línea Disponible (S/.):";
                                    label25.Text = "Línea C. en Uso (S/.):";
                                    if (cli.LineaCredito > 0) { cmbFormaPago.Enabled = true; } else { cmbFormaPago.Enabled = false; }
                                }
                                else
                                {
                                    txtLineaCredito.Text = String.Format("{0:#,##0.00}", (cli.LineaCredito / Convert.ToDecimal(mdi_Menu.tc_hoy)));
                                    txtLineaCreditoDisponible.Text = String.Format("{0:#,##0.00}", (cli.LineaCreditoDisponible / Convert.ToDecimal(mdi_Menu.tc_hoy)));
                                    txtLineaCreditoUso.Text = Text = String.Format("{0:#,##0.00}", (cli.LineaCreditoUsado / Convert.ToDecimal(mdi_Menu.tc_hoy)));
                                    lbLineaCredito.Text = "Línea de Crédito ($.):";
                                    label23.Text = "Línea Disponible ($.):";
                                    label25.Text = "Línea C. en Uso ($.):";
                                    if (cli.LineaCredito > 0) { cmbFormaPago.Enabled = true; } else { cmbFormaPago.Enabled = false; }
                                }
                                if (cli.FormaPago != 0)
                                {
                                    cmbFormaPago.SelectedValue = 6; //cli.FormaPago  --   6 Contado
                                    EventArgs ee = new EventArgs();
                                    cmbFormaPago_SelectionChangeCommitted(cmbFormaPago, ee);
                                }
                                else
                                {
                                    dtpFechaPago.Value = DateTime.Today;
                                }

                            }
                        }
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void txtCodigoVendedor_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F1)
                {
                    frmVendedoresLista frmDialog = new frmVendedoresLista();
                    frmDialog.proc = 2;
                    frmDialog.ShowDialog();
                    if (vendedor != null)
                    {
                        txtCodigoVendedor.Text = vendedor.CodUsuario.ToString();
                        txtNombreVendedor.Text = vendedor.Nombre + " " + vendedor.Apellido;
                    }
                    else
                    {
                        txtCodigoVendedor.Text = "";
                        txtNombreVendedor.Text = "";
                        MessageBox.Show("No se encontró ningún vendedor con el código ingresado",
                                        "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error..." + ex.Message);
            }
        }

        private void txtCodigoVendedor_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (txtCodigoVendedor.Text.Trim() != "")
                    {
                        Int32 codigoUsuario = Convert.ToInt32(txtCodigoVendedor.Text);
                        /*
                         * consultar usuario sin considerar usuario
                         * con nombre admin
                         */
                        vendedor = admUsuario.MuestraUsuarioSinAdmin(codigoUsuario);
                        if (vendedor != null)
                        {
                            txtNombreVendedor.Text = vendedor.Nombre + " " + vendedor.Apellido;
                        }
                        else
                        {
                            txtCodigoVendedor.Text = "";
                            txtNombreVendedor.Text = "";
                            MessageBox.Show("No se encontró ningún vendedor con el código ingresado",
                                            "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió un error: " + ex.Message,
                                "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void chkBoleta_Click(object sender, EventArgs e)
        {
            chkBoleta.Checked = true;
            chkFactura.Checked = false;
            chkTicket.Enabled = false;
        }

        private void chkFactura_Click(object sender, EventArgs e)
        {
            chkBoleta.Checked = false;
            chkFactura.Checked = true;
            chkTicket.Enabled = false;
        }

        private void toolStripButtonSalir_Click(object sender, EventArgs e)
        {
            CerrarOV();
        }


        private void CerrarOV()
        {
            if (dgvdetalle.Rows.Count > 0 && txtPedido.Text == "")
            {
                if (MessageBox.Show("Existen productos pendientes por vender..! \n Desea cancelar el pedido?", "Pedido Venta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    this.Close();
                    this.Dispose();
                }
            }
            else
            {
                this.Close();
                this.Dispose();
            }
        }

        private void txtSerie_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (Application.OpenForms["frmSerie"] != null)
                {
                    Application.OpenForms["frmSerie"].Activate();
                }
                else
                {
                    frmSerie form = new frmSerie();
                    form.Proceso = 3;
                    form.DocSeleccionado = CodDocumento;
                    form.ShowDialog();
                    ser = form.ser;
                    CodSerie = ser.CodSerie;
                    manual = Convert.ToInt32(ser.PreImpreso);
                    if (CodSerie != 0)
                    {
                        txtSerie.Text = ser.Serie;
                    }
                    if (CodSerie != 0) { }
                }
            }
        }

        private void txtDocRef_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                if (txtDocRef.Text != "")
                {
                    if (BuscaTipoDocumento())
                    {
                        //ProcessTabKey(true);
                    }
                    else
                    {
                        MessageBox.Show("Codigo de Documento no existe, Presione F1 para consultar la tabla de ayuda", "NOTA DE SALIDA", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        private Boolean BuscaTipoDocumento()
        {
            doc = Admdoc.BuscaTipoDocumento(txtDocRef.Text);
            if (doc != null)
            {
                CodDocumento = doc.CodTipoDocumento;
                lbDocumento.Text = doc.Descripcion;
                lbDocumento.Visible = true;
                //label1.Visible = true;
                return true;
            }
            else
            {
                CodDocumento = 0;
                return false;
            }

        }


        private void CargaNumeracionOV()
        {
            try
            {
                ser = AdmSerie.CargaSerieOV(frmLogin.iCodEmpresa, frmLogin.iCodAlmacen);
                if (ser != null)
                {
                    CodSerie = ser.CodSerie;
                    manual = Convert.ToInt32(ser.PreImpreso);
                    if (CodSerie != 0)
                    {
                        txtSerie.Text = ser.Serie;
                    }
                    if (CodSerie != 0) { }
                }
                else
                {
                    MessageBox.Show("No existe numeración para la orden de venta", "Orden de venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception a)
            {
                MessageBox.Show(a.Message);
                this.Cursor = Cursors.Default;

            }
        }

        private void frmVenta2019_Shown(object sender, EventArgs e)
        {
            CargaFormaPagos();
            if (nuevaOV)
            {
                txtDocRef.Text = "OV";
                txtCodCliente.Text = "C000001";
                KeyPressEventArgs ee = new KeyPressEventArgs((char)Keys.Return);
                txtDocRef_KeyPress(txtDocRef, ee);
                BuscaCliente();

            }
            //else if (Proceso == 3)
            //{
            //    //toolStripButtonSalir.Visible = true;
            //    //toolStripButtonSalir.Enabled = true;
            //    // btnImprimirTicket.Enabled = true;
            //}
        }


        private Boolean BuscaCliente()
        {
            if (txtCodCliente.Text != "")
            {
                cli = AdmCli.BuscaCliente(txtCodCliente.Text, Tipo);
                if (cli != null)
                {
                    txtNombreCliente.Text = cli.RazonSocial;
                    CodCliente = cli.CodCliente;
                    txtDireccion.Text = cli.DireccionLegal;
                    txtCodCliente.Text = cli.RucDni;
                    if (cli.RucDni == "00000000")
                    {
                        txtNombreCliente.Enabled = true;
                        cmbFormaPago.Enabled = false;
                    }

                    txtDocRef.Visible = false;
                    label12.Visible = false;


                    CargaCreditoCliente(cli);




                    return true;
                }
                else
                {
                    txtNombreCliente.Text = "";
                    CodCliente = 0;
                    txtDireccion.Text = "";
                    return false;
                }
            }
            else
            {

                return false;
            }
        }


        public Boolean verificarPrecioVenta()
        {
            Boolean valor = false;
            if (mdi_Menu.MontoTopeBoleta > 0)
            {
                if (Convert.ToDecimal(txtPrecioVenta.Text) >= mdi_Menu.MontoTopeBoleta && CodCliente == 1)
                {
                    MessageBox.Show("Precio venta > S/. 700, registrar(DNI, datos completos del cliente) o seleccionar cliente para guardar pedido", "Advertencia",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    if (Application.OpenForms["frmClientesLista"] != null)
                    {
                        Application.OpenForms["frmClientesLista"].Activate();
                    }
                    else
                    {
                        frmClientesLista form = new frmClientesLista();
                        form.Proceso = 3;
                        form.ShowDialog();
                        txtCodCliente.Text = "";
                        txtDireccion.Text = "";
                        txtNombreCliente.Text = "";
                        if (form.exit)
                        {
                            cli = form.cli;
                            CodCliente = cli.CodCliente;
                            if (CodCliente != 0)
                            {
                                NombreCliente = cli.Nombre;
                                CargaCliente();
                                valor = true;
                                ProcessTabKey(true);
                            }
                        }
                        else
                        {
                            txtCodCliente.Focus();
                            valor = false;
                        }
                    }
                }
                else
                {
                    valor = true;
                }
            }
            else
            {
                valor = true;
            }
            return valor;
        }


        private void frmVenta2019_KeyDown(object sender, KeyEventArgs e)
        {

            this.Cursor = Cursors.WaitCursor;

            switch (e.KeyCode)
            {
                case Keys.F6:


                    btnInicioOV_Click(null, null);
                    //Proceso = 1;

                    //sololectura(false);
                    //LimpiaForm();

                    break;

                case Keys.F3:
                    // if (bandera) { LlamarAddDetalle(); dgvdetalle.Focus(); }
                    break;

                case Keys.F2:
                    //if (bandera) { btnDeleteItem_Click(sender, new EventArgs()); }
                    break;

                case Keys.F1:
                    if (bandera)
                    {
                        /*
                         * 21-06-2018: se tomo instancia de elementos del
                         * formulario que hacen uso de la tecla F1
                         */
                        if (ActiveControl is TextBox)
                        {
                            TextBox campoTexto = ActiveControl as TextBox;
                            if (campoTexto.Name == "txtCodigoVendedor")
                            {
                                txtCodigoVendedor_KeyUp(sender, e);
                            }

                            if (campoTexto.Name == "txtCodCliente")
                            {
                                txtCodCliente_KeyUp(sender, e);
                            }

                        }

                    }
                    break;


                case Keys.F4: // Editar orden de venta

                    if (toolStripEditaov.Enabled == true)
                    {

                        btnEditaOV.PerformClick();
                    }
                    break;

            }

            this.Cursor = Cursors.Default;

        }


        private void LimpiaForm()
        {
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    c.Text = "";
                }

            }
            dgvdetalle.Rows.Clear();
            calculatotales();
        }


        private void Recalcular()
        {
            Double bruto = 0;
            Double descuen = 0;
            Double valor = 0;
            Double figv = 0;
            Double pven = 0;
            foreach (DataGridViewRow row in dgvdetalle.Rows)
            {
                bruto = bruto + Convert.ToDouble(row.Cells[importe.Name].Value);
                descuen = descuen + Convert.ToDouble(row.Cells[montodscto.Name].Value);
                valor = valor + Convert.ToDouble(row.Cells[valorventa.Name].Value);
                figv = figv + Convert.ToDouble(row.Cells[igv.Name].Value);
                pven = pven + Convert.ToDouble(row.Cells[precioventa.Name].Value);
            }
            txtBruto.Text = String.Format("{0:#,##0.00}", bruto);
            txtDscto.Text = String.Format("{0:#,##0.00}", descuen);
            txtValorVenta.Text = String.Format("{0:#,##0.00}", valor);
            txtIGV.Text = String.Format("{0:#,##0.00}", figv);
            txtPrecioVenta.Text = String.Format("{0:#,##0.00}", pven);

            montosventa();

        }


        private void dgvdetalle_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            //if (e.RowIndex != -1)
            //{
            //    if (nuevaOV)
            //    {
            //        calculatotales();
            //        montosventa();
            //    }
            //    else if (editar)
            //    {
            //        int coddetallep = Convert.ToInt32(dgvdetalle.Rows[e.RowIndex].Cells[0].Value);

            //        if (AdmPedido.deletedetalle(coddetallep))
            //        {
            //            calculatotales();
            //            montosventa();
            //        }
            //    }
            //}
        }

        private void dgvproductos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {

                if (dgvproductos.CurrentCell.ColumnIndex.Equals(5))
                {
                    if (dgvproductos.CurrentCell != null)
                    {
                        setUnidades();
                    }
                }
                //dgvStockAlmacenes.AutoGenerateColumns = false;
                //dataGridView1.DataSource = AdmPro.MuestraUnidadesEquivalentesVenta1(int.Parse(dgvproductos.CurrentRow.Cells[codigo.Name].Value.ToString()), frmLogin.iCodAlmacen);
                //dataGridView1.ClearSelection();

                //dgvStockAlmacenes.AutoGenerateColumns = false;
                //dgvStockAlmacenes.DataSource = AdmPro.muestraStockProducto_almacenes(int.Parse(dgvproductos.CurrentRow.Cells[codigo.Name].Value.ToString()));
                //dgvStockAlmacenes.ClearSelection();

                /*
                if (e.ColumnIndex==5)
                {

                }*/

            }
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            /* if (dataGridView1.CurrentRow.Index != -1)
             {
                 dataGridView1.DataSource = AdmPro.MuestraUnidadesEquivalentesVenta1(int.Parse(dgvproductos.CurrentRow.Cells[codigo.Name].Value.ToString()), frmLogin.iCodAlmacen);
                 dataGridView1.ClearSelection();
             }*/
        }

        private void txtCodCliente_DoubleClick(object sender, EventArgs e)
        {

        }

        private void txtDireccion_DoubleClick(object sender, EventArgs e)
        {

        }

        private void ValidaCliente(String rucdni)
        {
            cli = AdmCli.ConsultaCliente(rucdni);
            if (cli == null)
            {
                cli = new clsCliente();
                Int32 id = AdmCli.GetUltimoId() + 1;
                if (rucdni.Length == 8) { cli.Dni = rucdni; cli.DireccionEntrega = "-"; cli.DireccionLegal = "-"; }
                else if (rucdni.Length == 11) { cli.Ruc = rucdni; cli.DireccionEntrega = txtDireccion.Text; cli.DireccionLegal = txtDireccion.Text; }
                cli.Nombre = txtNombreCliente.Text;
                cli.RazonSocial = txtNombreCliente.Text;
                cli.CodigoPersonalizado = "C" + id.ToString().PadLeft(6, '0').Trim();
                cli.FormaPago = 6; //pago ala contado
                cli.Moneda = 1;
                cli.LineaCredito = 0;
                cli.LineaCreditoDisponible = 0;
                cli.Habilitado = true;
                cli.CodUser = frmLogin.iCodUser;

                if (AdmCli.insert(cli)) { CodCliente = cli.CodClienteNuevo; }
            }
        }

        private void txtIGV_TextChanged(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void btnAnulaOV_Click(object sender, EventArgs e)
        {

        }

        private void groupBox7_Enter(object sender, EventArgs e)
        {

        }

        private void txtPrecioVenta_TextChanged(object sender, EventArgs e)
        {

        }

        private void chkTicket_CheckedChanged(object sender, EventArgs e)
        {
            chkBoleta.Checked = false;
            chkFactura.Checked = false;
            chkTicket.Checked = true;
        }

        private void toolStripGuardar_Click(object sender, EventArgs e)
        {

            this.Cursor = Cursors.WaitCursor;
            try
            {

                if (dgvdetalle.RowCount == 0)
                {
                    MessageBox.Show("No se puede Guardar, no existen productos en el pedido!",
                                    "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                }
                else
                {
                    if (verificarPrecioVenta())
                    {

                        if (superValidator1.Validate())
                        {
                            realizaProcesos();

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message.ToString());
                this.Cursor = Cursors.Default;
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                this.Cursor = Cursors.Default;
            }

            this.Cursor = Cursors.Default;
        }

        public void realizaProcesos()
        {

            //if (toolStripGuardar.Text == "Guardar Venta")
            //{
            //    toolStripGuardar.Enabled = true;
            //    guardaVenta();

            //}

            //if(!nuevaOV && editar && !cargaPedido)
            //{
            //    setPedido();
            //    toolStripGuardar.Enabled = true;
            //    if (AdmPedido.update(pedido))
            //    {
            //        //RecorreDetalle();
            //        foreach (clsDetallePedido det in detalle)
            //        {
            //            foreach (clsDetallePedido det1 in detalle)
            //            {
            //                if (det.CodDetallePedido == det1.CodDetallePedido && det1.CodDetallePedido != 0)
            //                {
            //                    AdmPedido.updatedetalle(det1);
            //                }
            //            }
            //        }
            //        foreach (clsDetallePedido deta in detalle)
            //        {
            //            if (deta.CodDetallePedido == 0)
            //            {
            //                AdmPedido.insertdetalle(deta);
            //            }
            //        }
            //        CodPedido = pedido.CodPedido;
            //        //CargaPedido();
            //        MessageBox.Show(
            //            "Los datos se actualizaron correctamente!\nPedido: " +
            //            Convert.ToInt32(pedido.Numeracion) + "\nTotal: " + pedido.Total + " Soles.",
            //            "Pedido Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //        sololectura(true);
            //        //frmPedidosPendientes form =
            //        //(frmPedidosPendientes)Application.OpenForms["frmPedidosPendientes"];
            //        //if (form != null) form.CargaLista();
            //        editar = false;
            //        //ov_venta = 1;


            //    }
            //}

            //proceso general
            if (!cargaPedido && !editar && toolStripGuardar.Text != "Guardar Venta" && nuevaOV)
            {
                CargaNumeracionOV();
                setPedido();
                if (AdmPedido.insertarOrdenVenta(pedido))
                {
                    CodPedido = pedido.CodPedido;

                    if (CodPedido != "")
                    {
                        frmVenta2019_Load(new object(), new EventArgs());
                    }
                    //MessageBox.Show("Los datos se guardaron correctamente!\nPedido: " + Convert.ToInt32(pedido.Numeracion) + "\nTotal: " + pedido.Total + " Soles.", "Pedido Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    toolStripGuardar.Enabled = false;

                    toolStripEditaov.Enabled = false;

                    txtBruto.Enabled = false;
                    txtDscto.Enabled = false;
                    sololectura(true);

                    //toolStripGuardar.Text = "Guardar Venta";
                    textBoxX2.Text = pedido.SerieDoc;
                    textBoxX1.Text = Convert.ToString(pedido.Numeracion);
                    nuevaOV = false;
                    toolStripGuardar.Enabled = false;
                    cmbAlmacenes.DataSource = null;
                    cmbAlmacenes.Enabled = false;
                    MessageBox.Show("Los datos se guardaron correctamente!\nPedido: " + Convert.ToInt32(pedido.Numeracion) + "\nTotal: " + pedido.Total + " Soles.", "Pedido Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    inicializarForm();
                }
                else
                {
                    MessageBox.Show("Ocurrió un error al registrar la operación",
                                    "Orden de Venta", MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                    return;
                }
            }

            if (cargaPedido && !editar && toolStripGuardar.Text == "Guardar Venta")
            {
                guardaVenta(); //guardarventa

            }

            if (cargaPedido && editar)
            {
                setPedido();
                toolStripGuardar.Enabled = true;
                if (AdmPedido.update(pedido))
                {
                    RecorreDetalle();
                    //foreach (clsDetallePedido det in detalle)
                    //{
                    //    foreach (clsDetallePedido det1 in detalle)
                    //    {
                    //        if (det.CodDetallePedido == det1.CodDetallePedido && det1.CodDetallePedido != 0)
                    //        {
                    //            AdmPedido.updatedetalle(det1);
                    //        }
                    //    }
                    //}
                    foreach (clsDetallePedido deta in detalle)
                    {
                        if (deta.CodDetallePedido == 0)
                        {
                            AdmPedido.insertdetalle(deta);
                        }
                    }
                    CodPedido = pedido.CodPedido;
                    MessageBox.Show(
                        "Los datos se actualizaron correctamente!\nPedido: " +
                        Convert.ToInt32(pedido.Numeracion) + "\nTotal: " + pedido.Total + " Soles.",
                        "Pedido Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    sololectura(true);

                    toolStripButtonPendiente.Enabled = true;
                    cmbAlmacenes.DataSource = null;
                    cmbAlmacenes.Enabled = false;
                    inicializarForm();

                }
            }
        }


        private void CargaTransaccion()
        {
            string codTransacion = "";

            tran = new clsTransaccion();


            codTransacion = "FT";


            tran = AdmTran.MuestraTransaccionS(codTransacion, 1);
            tran.Configuracion = AdmTran.MuestraConfiguracion(tran.CodTransaccion);

        }



        public void ArmaCabecera(int codalma)
        {

            Decimal bruto = 0; Decimal Dscto = 0; Decimal igv = 0; Decimal valor = 0;
            String montoBruto, montodescuento, montoigv, montovv, montotal;
            montogratuitas = 0; montoexoneradas = 0; montogravadas = 0; montoinafectas = 0;
            banderagrabada = false; banderaexonerada = false; banderainafecta = false;

            if (dgvdetalle.Rows.Count > 0)
            {

                foreach (DataGridViewRow row in dgvdetalle.Rows)
                {

                    if (Convert.ToInt32(row.Cells[codalmacen.Name].Value) == codalma)
                    {
                        //foreach( var item in x)
                        //{
                        //    int alma = Convert.ToInt32(item);

                        //    //if(alma==)

                        //}


                        bruto = bruto + Convert.ToDecimal(row.Cells[importe.Name].Value);
                        Dscto = Dscto + Convert.ToDecimal(row.Cells[montodscto.Name].Value);
                        valor = valor + Convert.ToDecimal(row.Cells[valorventa.Name].Value);


                        if (Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "21") // gratuitas
                        {
                            montogratuitas = montogratuitas + (Convert.ToDecimal(row.Cells[preciounit.Name].Value) * Convert.ToDecimal(row.Cells[cantidad.Name].Value));
                        }

                        if (Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "10" || Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "11" ||
                            Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "12" || Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "13" ||
                            Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "14" || Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "15" ||
                            Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "16" || Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "17")   // gravadas
                        {
                            montogravadas = montogravadas + (Convert.ToDecimal(row.Cells[valorventa.Name].Value)); banderagrabada = true;
                        }

                        if (Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "20") // exoneradas
                        {
                            montoexoneradas = montoexoneradas + (Convert.ToDecimal(row.Cells[preciounit.Name].Value) * Convert.ToDecimal(row.Cells[cantidad.Name].Value) - Convert.ToDecimal(row.Cells[montodscto.Name].Value));
                            banderaexonerada = true;
                        }

                        if (Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "30" || Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "31" ||
                            Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "32" || Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "33" ||
                            Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "34" || Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "35" ||
                            Convert.ToString(row.Cells[Tipoimpuesto.Name].Value) == "36") // inafectas
                        {
                            montoinafectas = montoinafectas + (Convert.ToDecimal(row.Cells[preciounit.Name].Value) * Convert.ToDecimal(row.Cells[cantidad.Name].Value) - Convert.ToDecimal(row.Cells[montodscto.Name].Value));
                            banderainafecta = true;
                        }
                    }
                }

                venta.Gratuitas = montogratuitas;
                venta.Exoneradas = montoexoneradas;
                venta.Gravadas = montogravadas;
                venta.Inafectas = montoinafectas;
                if (chkVentaGratuita.Checked) { venta.Tipoventa = 4; }  // venta gratuita
                else if (chkVentaDsctoGlobal.Checked)
                {
                    venta.Tipoventa = 5; // venta con descuento Global
                }
                else if (banderagrabada == true && banderaexonerada == false && banderainafecta == false)
                {
                    venta.Tipoventa = 1;  // venta grabada
                }
                else if (banderagrabada == false && banderaexonerada == true && banderainafecta == false)
                {
                    venta.Tipoventa = 2;  // venta exonerada
                }
                else if (banderagrabada == false && banderaexonerada == false && banderainafecta == true)
                {
                    venta.Tipoventa = 3;  // venta inafecta
                }
                else if (banderagrabada == true && banderaexonerada == true && banderainafecta == false)
                {
                    venta.Tipoventa = 6;  // venta grabada + exonerada
                }
                else if (banderagrabada == true && banderaexonerada == false && banderainafecta == true)
                {
                    venta.Tipoventa = 7;  // venta grabada + inafecta
                }
                montodescuento = String.Format("{0:#,##0.00}", Dscto);
                montoBruto = String.Format("{0:#,##0.00}", bruto);
                montovv = String.Format("{0:#,##0.00}", valor);
                montoigv = String.Format("{0:#,##0.00}", bruto - Dscto - valor);
                montotal = String.Format("{0:#,##0.00}", bruto - Dscto);

                venta.MontoBruto = Convert.ToDouble(montoBruto);
                venta.MontoDscto = Convert.ToDouble(montodescuento);
                venta.Igv = Convert.ToDouble(montoigv);
                venta.Total = Convert.ToDouble(montotal);
            }
        }



        private async void guardaVenta()
        {
            try
            {

                //clsDocumentoIdentidad documentoIdentidadSeleccionado = AdmDocumentoIdentidad.MuestraDocumentoIdentidad(Convert.ToInt32(cmbDocumentoIdentidad.SelectedValue));

                //if (documentoIdentidadSeleccionado == null)
                //{
                //    MessageBox.Show("Por favor seleccione un tipo de documento", "Genera Venta", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //    btnGuardaVenta.Enabled = true;
                //    return;
                //}

                Double totalsoles = 0;

                /**
                 *  Cargar nuevamente para ver el detalle actualizado
                 *  nota: verificar que el datagridview se actualice en todo caso buscar una alternativa
                 */
                //CodPedido = Convert.ToInt32(pedido.CodPedido);
                //CargaPedido();


                if (superValidator1.Validate())
                {
                    //using (var Scope = new TransactionScope())
                    //{
                    if (Convert.ToInt32(cli.Moneda) == 1)
                    {
                        totalsoles = Convert.ToDouble(txtPrecioVenta.Text);
                    }
                    else
                    {
                        totalsoles = (Convert.ToDouble(txtPrecioVenta.Text) * mdi_Menu.tc_hoy);
                    }

                    if ((totalsoles > Convert.ToDouble(txtLineaCreditoDisponible.Text)) && Convert.ToInt32(cmbFormaPago.SelectedValue) != 6)
                    {
                        MessageBox.Show("El Monto Excede a la Línea de Crédito");
                    }
                    else
                    {
                        var empre = dgvdetalle.Rows.Cast<DataGridViewRow>().Select(z => z.Cells[empres.Name].Value).Distinct().ToList();
                        var alma = dgvdetalle.Rows.Cast<DataGridViewRow>().Select(y => y.Cells[codalmacen.Name].Value).Distinct().ToList();

                        //var prueba = from row in dgvdetalle.Rows.Cast<DataGridViewRow>() select new { empres = Convert.ToInt32(row.Cells[empres.Name].Value), almac =Convert.ToInt32(row.Cells[codalmacen.Name].Value) };

                        lista_facturas = new List<clsFacturaVenta>();

                        foreach (var e in alma)
                        {
                            int codsucu = admsucu.sucursalxalmacen(Convert.ToInt32(e));
                            int codemp = admEmpresa.empresaxalmacen(Convert.ToInt32(e));

                            venta = new clsFacturaVenta();

                            venta.CodSucursal = codsucu;
                            venta.CodAlmacen = Convert.ToInt32(e);
                            cli = AdmCli.MuestraCliente(pedido.CodCliente);

                            if (cli.RucDni.Length == 8)
                            {
                                chkBoleta.Checked = true;
                                cli.DocumentoIdentidad = new clsDocumentoIdentidad();
                                cli.DocumentoIdentidad.CodDocumentoIdentidad = 1;
                                cli.DocumentoIdentidad.CodigoSunat = 1;
                            }
                            else if (cli.RucDni.Length == 11)
                            {
                                chkFactura.Checked = true;

                                cli.DocumentoIdentidad = new clsDocumentoIdentidad();
                                cli.DocumentoIdentidad.CodDocumentoIdentidad = 3;
                                cli.DocumentoIdentidad.CodigoSunat = 6;
                            }
                            txtCodCliente.Text = cli.RucDni;
                            txtNombreCliente.Text = cli.RazonSocial;
                            txtDireccion.Text = cli.DireccionLegal;

                            venta.DocumentoIdentidad = new clsDocumentoIdentidad();
                            venta.DocumentoIdentidad = cli.DocumentoIdentidad;


                            venta.CodCliente = pedido.CodCliente;
                            venta.NumeroDocumentoCliente = txtCodCliente.Text;


                            venta.Detallecomentario = "";
                            venta.CodCotizacion = 0;
                            if (chkBoleta.Checked)
                            {
                                venta.Boletafactura = 1;
                                doc.CodTipoDocumento = 1;
                                venta.CodTipoDocumento = doc.CodTipoDocumento;
                            }
                            else if (chkFactura.Checked)
                            {
                                venta.Boletafactura = 2;
                                doc.CodTipoDocumento = 2;
                                venta.CodTipoDocumento = doc.CodTipoDocumento;
                            }

                            CargaTransaccion();
                            venta.CodTipoTransaccion = tran.CodTransaccion;

                            //venta.CodSerie = CodSerie;
                            //venta.Serie = txtSerie.Text;
                            venta.NumDoc = txtPedido.Text;

                            venta.Estado = 1;
                            venta.Consultorext = false;
                            venta.Codsalidaconsulext = 0;

                            /*
                                * Comentado para agregar el codigo del pedido
                                * 
                                if (PedidosIngresados.Count > 1)
                                venta.CodPedido = 0;
                                else
                                venta.CodPedido = Convert.ToInt32(pedido.CodPedido);
                            */
                            venta.CodPedido = Convert.ToInt32(pedido.CodPedido);

                            venta.Moneda = 1;
                            venta.TipoCambio = mdi_Menu.tc_hoy;
                            venta.FechaSalida = dtpFecha.Value;
                            venta.FechaPago = dtpFechaPago.Value;
                            venta.FormaPago = Convert.ToInt32(cmbFormaPago.SelectedValue);
                            venta.CodListaPrecio = 0;
                            venta.CodVendedor = Convert.ToInt32(txtCodigoVendedor.Text);
                            //venta.Comentario = txtComentario.Text;
                            venta.Comentario = "";
                            venta.CodUser = frmLogin.iCodUser;
                            //venta.Entregado = Convert.ToInt32(rbtnPendiente.Checked);
                            venta.Entregado = 1;

                            venta.CodigoBarras = DateTime.Today.Year.ToString().Substring(2, 2) + DateTime.Today.Month.ToString().PadLeft(2, '0') +
                                    DateTime.Today.Day.ToString().PadLeft(2, '0') + DateTime.Now.ToShortTimeString().Substring(0, 2) + DateTime.Now.ToShortTimeString().Substring(3, 2) +
                                    venta.CodSerie.ToString().PadLeft(3, '0') + CodCliente;
                            venta.CodigoBarrasCifrado = "";//ok.Encode(venta.CodigoBarras);
                                                            //venta.ventasinstock = facturarsinmoverstock;
                            venta.ventasinstock = 0;

                            foreach (clsPedido ped in PedidosIngresados)
                            {
                                ped.CodigoBarras = venta.CodigoBarras;
                                ped.CodigoBarrasCifrado = venta.CodigoBarrasCifrado;
                            }
                            if (txtCodCliente.Text.ToString() == "00000000") { venta.Nombre = txtNombreCliente.Text; } else { venta.Nombre = txtNombreCliente.Text; }

                            ArmaCabecera(Convert.ToInt32(e));

                            ser = AdmSerie.CargaSerieEmpresa(Convert.ToInt32(e), doc.CodTipoDocumento);

                            if (ser != null)
                                venta.CodSerie = ser.CodSerie;
                            venta.Serie = ser.Serie;
                            venta.NumDoc = ser.Numeracion.ToString().PadLeft(8, '0');

                            clsFacturaVenta factura = new clsFacturaVenta();
                            factura = AdmVenta.FechaCorrelativoAnterior(venta.CodSerie);


                            venta.CodEmpresa = codemp; //venta.CodEmpresa = frmLogin.iCodEmpresa;
                                                                    //if (Proceso == 4 || Proceso == 1)
                                                                    //{
                            if (factura.FechaSalida.Date > venta.FechaSalida.Date)
                            {
                                MessageBox.Show("Error No se puede Registrar los Datos. Verifique Fecha");
                            }
                            else
                            {

                                RecorreDetalleVenta(Convert.ToInt32(e));
                                venta.Detalle = detalle1;
                                // venta.DocumentoIdentidad = documentoIdentidadSeleccionado;
                                if (detalle1.Count > 0)
                                {
                                    /*
                                    * realizar la inserción en una sola
                                    * función enviando el objeto venta
                                    * con la lista de detalles
                                    */
                                    venta.Pendiente = Convert.ToDouble(venta.Total);

                                    fpago = AdmPago.CargaFormaPago(Convert.ToInt32(cmbFormaPago.SelectedValue));
                                    dtpFechaPago.Value = dtpFecha.Value.AddDays(fpago.Dias);
                                    //{
                                    if (fpago.Dias > 0 && venta.CodTipoTransaccion == 7)
                                    //se comprueba que el pago sea al contado y que la trnasaccion sea ingreso por compra
                                    {
                                        //ingresarpago();

                                        AdmVenta.insertComprobante(venta);

                                        //venta = AdmVenta.CargaFacturaVenta(Convert.ToInt32(venta.CodFacturaVenta));

                                        CodVenta = venta.CodFacturaVenta;

                                        MessageBox.Show("Los datos se guardaron correctamente",
                                        "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);


                                        if (venta.FormaPago != 6)
                                        {
                                            toolStripImprimir.Visible = true;
                                        }

                                        //FACTURACION ELECTRONICA

                                        if (!chkTicket.Checked)
                                        {
                                            await facturacion.GeneraDocumento(cli, venta, detalle1);
                                            // contingencia xml - SUNAT

                                            venta.Qr = facturacion.LogoEmp;
                                        }

                                        //fnImprimir(venta);
                                        //textBoxX2.Text = venta.Serie;
                                        //textBoxX1.Text = venta.NumDoc;
                                        //toolStripImprimir.Enabled = true;
                                        //toolStripGuardar.Enabled = false;
                                        //toolStripEditaov.Enabled = false;
                                    }
                                    else
                                    {
                                        frmCancelarPago form = new frmCancelarPago();
                                        //form.CodNota = venta.CodFacturaVenta;
                                        form.VentComp = 1;
                                        form.tipo = 3;
                                        form.CodCliente = cli.CodCliente;
                                        form.venta = venta;

                                        form.ShowDialog();

                                        if (form.ventana_cobro)
                                        {

                                            if (form.ventaRecibida)
                                            {
                                                //foreach (clsDetalleFacturaVenta det in venta.Detalle)
                                                //{
                                                //    Int32 unidadbase = AdmPro.UnidadBase(det.CodProducto, venta.CodAlmacen);
                                                //    Int32 productofacturado = AdmPro.GetProductoFacturado(det.CodProducto);
                                                //    Decimal factor = AdmPro.FactorProducto(det.CodProducto, det.UnidadIngresada, unidadbase, 2); // 2 es equivalencia
                                                //    Decimal valorpromediosoles = AdmPro.GetValorPromedioSoles(det.CodProducto, venta.CodAlmacen);
                                                //    Decimal stock = 0; Decimal soles = 0;

                                                //    if (venta.ventasinstock == 0 && productofacturado == 0 && venta.CodTipoDocumento == 7)
                                                //    {
                                                //        if (AdmPro.ExisteProductoSinFacturar(det.CodProducto) > 0)
                                                //        {
                                                //            AdmVenta.ActualizaStockSinFacturar(det, unidadbase, factor, 1);// 1 es aumentar stock
                                                //        }
                                                //        else
                                                //        {
                                                //            stock = Convert.ToDecimal(det.Cantidad) * factor;
                                                //            soles = stock * valorpromediosoles;
                                                //            AdmVenta.InsertaProductoSinFacturar(det, valorpromediosoles, stock, soles, unidadbase);
                                                //        }
                                                //    }
                                                //    else if (venta.ventasinstock == 1 && productofacturado == 0)
                                                //    {
                                                //        if (AdmPro.ExisteProductoSinFacturar(det.CodProducto) > 0)
                                                //        {
                                                //            AdmVenta.ActualizaStockSinFacturar(det, unidadbase, factor, 2);// 2 es disminuir stock
                                                //        }
                                                //    }
                                                //}
                                                //MessageBox.Show("Los datos se guardaron correctamente",
                                                //    "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                                //toolStripAnulaov.Enabled = false;


                                                //txtNumDoc.Text = venta.CodFacturaVenta.PadLeft(11, '0');


                                                //if (ncredito.Count > 0)
                                                //{

                                                //}
                                                //else
                                                //{
                                                //if (fpago.Dias == 0 && venta.CodTipoTransaccion == 7)
                                                //se comprueba que el pago sea al contado y que la trnasaccion sea ingreso por compra
                                                //{
                                                    //ingresarpago();
                                                //}
                                                CodVenta = venta.CodFacturaVenta;
                                                //Proceso = 0;
                                                if (venta.FormaPago != 6)
                                                {
                                                    toolStripImprimir.Visible = true;
                                                }
                                                //}
                                                //FACTURACION ELECTRONICA

                                                if (!chkTicket.Checked)
                                                {
                                                    await facturacion.GeneraDocumento(cli, venta, detalle1);
                                                    // contingencia xml - SUNAT

                                                    venta.Qr = facturacion.LogoEmp;
                                                }

                                                //fnImprimir(venta);
                                                //textBoxX2.Text = venta.Serie;
                                                //textBoxX1.Text = venta.NumDoc;
                                                //toolStripImprimir.Enabled = true;
                                                //toolStripGuardar.Enabled = false;
                                                //toolStripEditaov.Enabled = false;
                                                //cmbAlmacenes.DataSource = null;
                                                //cmbAlmacenes.Enabled = false;

                                            }
                                            else
                                            {
                                                MessageBox.Show("Ocurrió un problema al registrar la venta.", "Registro de Venta",
                                                                MessageBoxButtons.OK, MessageBoxIcon.Error);

                                                if (!AdmVenta.ValidaAnulacionVenta(Convert.ToInt32(venta.CodFacturaVenta)))
                                                {
                                                    if (AdmVenta.anular(Convert.ToInt32(venta.CodFacturaVenta)))
                                                    {

                                                        DataTable dtPagos = AdmPagos.GetPagosVenta(Convert.ToInt32(e), Convert.ToInt32(venta.CodFacturaVenta));

                                                        foreach (DataRow fila in dtPagos.Rows)
                                                        {
                                                            AdmPagos.AnularPago(Convert.ToInt32(fila[0]));
                                                        }

                                                    }
                                                }
                                                venta = new clsFacturaVenta();
                                                factura = new clsFacturaVenta();
                                            }
                                        }
                                        else
                                        {
                                            MessageBox.Show("Cancelo la venta", "Error");

                                            if (!AdmVenta.ValidaAnulacionVenta(Convert.ToInt32(venta.CodFacturaVenta)))
                                            {
                                                if (AdmVenta.anular(Convert.ToInt32(venta.CodFacturaVenta)))
                                                {
                                                    DataTable dtPagos = AdmPagos.GetPagosVenta(Convert.ToInt32(e), Convert.ToInt32(venta.CodFacturaVenta));

                                                    foreach (DataRow fila in dtPagos.Rows)
                                                    {
                                                        AdmPagos.AnularPago(Convert.ToInt32(fila[0]));
                                                    }

                                                }
                                            }

                                            venta = new clsFacturaVenta();
                                            factura = new clsFacturaVenta();
                                        }

                                    }
                                }
                                else
                                {
                                    MessageBox.Show("No hay items en el comprobante.", "Registro de Venta", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    venta = new clsFacturaVenta();
                                    factura = new clsFacturaVenta();
                                }

                            }

                            foreach (clsPedido ped in PedidosIngresados)
                            {
                                AdmPedido.GuardaCodigoBarras(ped);
                            }

                            this.PedidosIngresados = new List<clsPedido>();
                            /**
                                * llamada a funcion comentada debido a que no se imprime orden de despacho
                                * printaOV();
                                */
                            //this.Close();

                            lista_facturas.Add(venta);
                        }

                        foreach(clsFacturaVenta fv in lista_facturas)
                        {
                            fnImprimir(fv);
                        }

                        textBoxX2.Text = venta.Serie;
                        textBoxX1.Text = venta.NumDoc;
                        toolStripImprimir.Enabled = true;
                        toolStripGuardar.Enabled = false;
                        toolStripEditaov.Enabled = false;
                        cmbAlmacenes.DataSource = null;
                        cmbAlmacenes.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message.ToString());

                if (venta.CodFacturaVenta != null)
                {

                    if (!AdmVenta.ValidaAnulacionVenta(Convert.ToInt32(venta.CodFacturaVenta)))
                    {
                        if (AdmVenta.anular(Convert.ToInt32(venta.CodFacturaVenta)))
                        {
                            DataTable dtPagos = AdmPagos.GetPagosVenta(venta.CodAlmacen, Convert.ToInt32(venta.CodFacturaVenta));

                            foreach (DataRow fila in dtPagos.Rows)
                            {
                                AdmPagos.AnularPago(Convert.ToInt32(fila[0]));
                            }

                        }
                    }
                    venta = new clsFacturaVenta();
                    //factura = new clsFacturaVenta();
                }
            }

        }

        public void fnImprimir(clsFacturaVenta fv)
        {
            try
            {
                impresion = AdmVenta.chekeaImpresion(Convert.ToInt32(fv.CodFacturaVenta));
                empresa = admEmpresa.CargaEmpresa(fv.CodEmpresa);

                if (impresion == 0)
                {

                    PrintaDocumento(fv);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se encontro el siguiente problema" + ex.Message, "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void PrintaDocumento(clsFacturaVenta fv)
        {
            try
            {
                if (frmLogin.iCodAlmacen != 0)
                {
                    DataSet jes = new DataSet();
                    frmRptFactura frm = new frmRptFactura();
                    //CRFacturaInsumos rpt = new CRFacturaInsumos();
                    CRFacturaFomatoContinuo rpt = new CRFacturaFomatoContinuo();

                    //rpt.Load("CRNotaCreditoVenta.rpt");
                    jes = ds1.ReporteFactura2(Convert.ToInt32(fv.CodFacturaVenta));

                    foreach (DataTable mel in jes.Tables)
                    {
                        foreach (DataRow changesRow in mel.Rows)
                        {
                            changesRow["firma"] = fv.Qr;
                        }

                        if (mel.HasErrors)
                        {
                            foreach (DataRow changesRow in mel.Rows)
                            {
                                if ((int)changesRow["Item", DataRowVersion.Current] > 100)
                                {
                                    changesRow.RejectChanges();
                                    changesRow.ClearErrors();
                                }
                            }
                        }
                    }

                    rpt.SetDataSource(jes);
                    CrystalDecisions.CrystalReports.Engine.PrintOptions rptoption = rpt.PrintOptions;
                    rptoption.PrinterName = ser.NombreImpresora;
                    rptoption.PaperSize = (CrystalDecisions.Shared.PaperSize)ext.GetIDPaperSize(ser.NombreImpresora, ser.PaperSize);

                    rptoption.ApplyPageMargins(new CrystalDecisions.Shared.PageMargins(40, 5, 0, 10));

                    rpt.PrintToPrinter(1, false, 1, 1);
                    //rpt.PrintToPrinter(1, false, 1, 1);

                    rpt.Close();
                    rpt.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se encontro el siguiente problema" + ex.Message, "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void VerificaSaldoCaja()
        {
            try
            {
                Caja = AdmCaja.ValidarAperturaDia(frmLogin.iCodSucursal, DateTime.Now.Date, 1, frmLogin.iCodAlmacen, frmLogin.iCodUser);//1 caja ventas

                if (Caja == null)
                {
                    MessageBox.Show("Aperture caja, el pago no se a registrado", "SGE SYSTEM'S", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                }
                else
                {
                    CodigoCaja = Caja.Codcaja;
                }
            }
            catch (Exception a) { MessageBox.Show(a.Message); }
        }

        private void ingresarpago()
        {


            VerificaSaldoCaja();

            if (AdmVenta.insertComprobante(venta))
            {
                venta = AdmVenta.CargaFacturaVenta(Convert.ToInt32(venta.CodFacturaVenta));

                if (CodigoCaja != 0)
                {
                    Pag.CodNota = venta.CodFacturaVenta.ToString();
                    Pag.CodLetra = 0;
                    Pag.CodTipoPago = 5; //metodo de pago
                    Pag.CodMoneda = venta.Moneda;
                    Pag.CodCobrador = Convert.ToInt32(frmLogin.iCodUser);
                    Pag.Tipo = true;// total o parcial
                    Pag.IngresoEgreso = true;//ingreso
                    Pag.TipoCambio = Convert.ToDecimal(venta.TipoCambio);
                    Pag.MontoPagado = Convert.ToDecimal(venta.Total);
                    Pag.MontoCobrado = Convert.ToDecimal(venta.Total);
                    Pag.Vuelto = 0;
                    Pag.codCtaCte = 0;
                    Pag.CtaCte = "";
                    Pag.NOperacion = "";
                    Pag.NCheque = "";
                    Pag.FechaPago = venta.FechaPago.Date;
                    Pag.Observacion = "";
                    Pag.CodUser = frmLogin.iCodUser;
                    Pag.CodAlmacen = frmLogin.iCodAlmacen;
                    Pag.CodSucursal = frmLogin.iCodSucursal;
                    Pag.CodDoc = venta.CodTipoDocumento;
                    Pag.Serie = "";
                    Pag.NumDoc = "";
                    Pag.Referencia = "";
                    Pag.Codcaja = CodigoCaja;
                    Pag.CodBanco = 0;
                    Pag.CodTarjeta = 0;
                    Pag.Aprobado = 4;
                    //montoPag = 1;
                    if (AdmPagos.insert(Pag))
                    {
                        //this.Close();
                    }
                }
                else
                {
                    MessageBox.Show("Hubo un error al insertar venta");
                }



            }

        }

        private void VerificaDocumentosPorEmpresa(int codigoE)
        {/*
            try
            {
                Boolean bandera = false;
                ListaCantDoc.Clear();
                foreach (DataGridViewRow row in dgvDetalle.Rows)
                {
                    bandera = false;
                    if (ListaCantDoc.Count == 0)
                    {
                        ListaCantDoc.Add(Convert.ToInt32(row.Cells[venta_tickeck.Name].Value));
                    }
                    else
                    {
                        foreach (Int32 lista in ListaCantDoc)
                        {
                            if (lista == Convert.ToInt32(row.Cells[venta_tickeck.Name].Value))
                            {
                                bandera = true;
                            }
                        }

                        if (!bandera)
                        {
                            ListaCantDoc.Add(Convert.ToInt32(row.Cells[venta_tickeck.Name].Value));
                        }
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void GeneraListaEnpresas()
        {

            /*
            Boolean bandera = false;
            ListaEmpresa.Clear();
            foreach (DataGridViewRow row in dgvDetalle.Rows)
            {
                bandera = false;
                if (ListaEmpresa.Count == 0)
                {
                    ListaEmpresa.Add(Convert.ToInt32(row.Cells[CodigoEmpresa.Name].Value));
                }
                else
                {
                    foreach (Int32 lista in ListaEmpresa)
                    {
                        if (lista == Convert.ToInt32(row.Cells[CodigoEmpresa.Name].Value))
                        {
                            bandera = true;
                        }
                    }

                    if (!bandera)
                    {
                        ListaEmpresa.Add(Convert.ToInt32(row.Cells[CodigoEmpresa.Name].Value));
                    }
                }
            }*/
        }

        public void setPedido()
        {

                pedido.CodAlmacen = frmLogin.iCodAlmacen;
                if (CodCliente == 0)
                {
                    ValidaCliente(txtCodCliente.Text);
                }
                pedido.CodCliente = CodCliente;
                pedido.CodTipoDocumento = doc.CodTipoDocumento;

                //if (txtCodCliente.Text.ToString() == "00000000") { pedido.Nombrecliente = txtNombreCliente.Text; } else { pedido.Nombrecliente = txtNombreCliente.Text; }
                pedido.Nombrecliente = txtNombreCliente.Text;
                pedido.Moneda = 1;//Convert.ToInt32(cmbMoneda.SelectedValue);
                if (mdi_Menu.tc_hoy > 0)
                {
                    pedido.TipoCambio = mdi_Menu.tc_hoy;
                }
                pedido.FechaPedido = dtpFecha.Value.Date;
                pedido.FechaEntrega = dtpFecha.Value.Date;
                pedido.FormaPago = Convert.ToInt32(cmbFormaPago.SelectedValue);
                pedido.FechaPago = dtpFecha.Value.AddDays(fpago.Dias);
                pedido.CodSerie = CodSerie;
                pedido.Comentario = "";
                if (editar)
                {
                    pedido.SerieDoc = textBoxX2.Text;
                    pedido.Numeracion = Convert.ToInt32(textBoxX1.Text);
                }
                else
                {
                    pedido.SerieDoc = ser.Serie;
                    pedido.Numeracion = ser.Numeracion;
                }

                pedido.MontoBruto = Convert.ToDouble(txtBruto.Text);
                pedido.MontoDscto = Convert.ToDouble(txtDscto.Text);
                pedido.Igv = Convert.ToDouble(txtIGV.Text);
                pedido.Total = Convert.ToDouble(txtPrecioVenta.Text);
                /*
                 * cambiar el uso de este parámetro
                 * en lugar de usar el codigo del usuario autenticado
                 * tomar de una variable local
                 */
                pedido.CodVendedor = Convert.ToInt32(txtCodigoVendedor.Text);//frmLogin.iCodUser;
                pedido.CodUser = Convert.ToInt32(txtCodigoVendedor.Text);//vendedor.CodUsuario;
                pedido.Estado = 1;

                pedido.Gratuitas = montogratuitas;
                pedido.Exoneradas = montoexoneradas;
                pedido.Gravadas = montogravadas;
                pedido.Inafectas = montoinafectas;
                if (chkBoleta.Checked)
                {
                    pedido.Boletafactura = 1;
                }
                else if (chkFactura.Checked)
                {
                    pedido.Boletafactura = 2;
                }
                pedido.CodEmpresa = frmLogin.iCodEmpresa;
                pedido.CodigoBarras = "";
                pedido.CodigoBarrasCifrado = "";
                pedido.ventasinstock = Convert.ToInt32(chkVentaSinStock.Checked);
                // 1 venta grabada   2 venta exonerada    3 venta inafecta   4 venta gratuita    5 venta con descuento 
                // 6 venta grabada + exonerada   7 venta grabada + inafecta
                banderagrabada = false; banderaexonerada = false; banderainafecta = false;
                foreach (DataGridViewRow row in dgvdetalle.Rows)
                {
                    switch (Convert.ToInt32(row.Cells[Tipoimpuesto.Name].Value))
                    {
                        case 10: banderagrabada = true; break;

                        case 11: banderagrabada = true; break;

                        case 12: banderagrabada = true; break;

                        case 13: banderagrabada = true; break;

                        case 14: banderagrabada = true; break;

                        case 15: banderagrabada = true; break;

                        case 16: banderagrabada = true; break;

                        case 17: banderagrabada = true; break;

                        case 20: banderaexonerada = true; break;

                        case 30: banderainafecta = true; break;

                        case 31: banderainafecta = true; break;

                        case 32: banderainafecta = true; break;

                        case 33: banderainafecta = true; break;

                        case 34: banderainafecta = true; break;

                        case 35: banderainafecta = true; break;

                        case 36: banderainafecta = true; break;

                    }
                }

                if (chkVentaGratuita.Checked)
                {
                    pedido.Tipoventa = 4; // venta gratuita
                }
                else if (chkVentaDsctoGlobal.Checked)
                {
                    pedido.Tipoventa = 5; // venta con descuento Global
                }
                else if (banderagrabada == true && banderaexonerada == false && banderainafecta == false)
                {
                    pedido.Tipoventa = 1; // venta gravada
                }
                else if (banderagrabada == false && banderaexonerada == true && banderainafecta == false)
                {
                    pedido.Tipoventa = 2; // venta exonerada
                }
                else if (banderagrabada == false && banderaexonerada == false && banderainafecta == true)
                {
                    pedido.Tipoventa = 3; // venta inafecta
                }
                else if (banderagrabada == true && banderaexonerada == true && banderainafecta == false)
                {
                    pedido.Tipoventa = 6; // 
                }
                else if (banderagrabada == false && banderaexonerada == true && banderainafecta == true)
                {
                    pedido.Tipoventa = 7; // 
                }

                // Para saber si la nota esta activa o anulada. El estado se podra cambiar en una ventana especifica para anular notas
                //if (Proceso == 1)
                //{
                /*
                 * Insercion de cabecera y detalles en un solo 
                 * metodo
                 */
                RecorreDetalle();
                pedido.Detalle = detalle;
        }

        private void CargaFormaPagos()
        {
            cmbFormaPago.DataSource = AdmPago.CargaFormaPagos(1);
            cmbFormaPago.DisplayMember = "descripcion";
            cmbFormaPago.ValueMember = "codFormaPago";
            cmbFormaPago.SelectedIndex = 0;

            cmbFormaPago_SelectionChangeCommitted(new object(), new EventArgs());
            cmbFormaPago.Visible = true;
            dtpFechaPago.Visible = true;
        }

        private void toolStripButtonPendiente_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["frmPedidosPendientes"] != null)
            {
                Application.OpenForms["frmPedidosPendientes"].Activate();
            }
            else
            {
                //limpiarVentana();
                cargaPedido = false;
                frmPedidosPendientes form = new frmPedidosPendientes();
                form.venta2019 = this;
                form.WindowState = FormWindowState.Normal;
                form.ShowDialog();

                venta = new clsFacturaVenta();

                if (cargaPedido)
                {
                    dgvproductos.DataSource = null;
                    dgvproductos.Rows.Clear();

                    dgvStockAlmacenes.DataSource = null;
                    dgvStockAlmacenes.Rows.Clear();

                    CargaPedidoVenta();
                    if (frmLogin.iNivelUser == 3 || frmLogin.iNivelUser == 1 || frmLogin.iNivelUser == 1)
                    {
                        toolStripGuardar.Text = "Guardar Venta";
                        toolStripGuardar.Enabled = true;
                        this.Text = "VENTA";
                        lbDocumento.Text = "VENTA";
                    }
                    else
                    {
                        toolStripGuardar.Text = "Guardar";
                        toolStripGuardar.Enabled = false;
                        this.Text = "ORDEN VENTA";
                        lbDocumento.Text = "ORDEN VENTA";
                    }

                    toolStripEditaov.Enabled = true;
                    //toolStripAnulaov.Enabled = true;

                    //CargaCreditoCliente(cli);

                }
            }
        }



        private void txtCodCliente_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F1)
                {
                    if (Application.OpenForms["frmClientesLista"] != null)
                    {
                        Application.OpenForms["frmClientesLista"].Activate();
                    }
                    else
                    {
                        frmClientesLista form = new frmClientesLista();
                        form.Proceso = 3;
                        form.ShowDialog();
                        cli = form.cli;
                        CodCliente = cli.CodCliente;
                        if (CodCliente != 0)
                        {
                            txtCodCliente.Text = "";
                            txtDireccion.Text = "";
                            txtNombreCliente.Text = "";
                            NombreCliente = cli.Nombre;
                            CargaCliente();


                        }
                    }
                }
            }
            catch (Exception a) { MessageBox.Show(a.Message); }
        }

        private void CargaPedido()
        {
            try
            {
                pedido = AdmPedido.CargaPedido(Convert.ToInt32(CodPedido));
                if (pedido != null)
                {
                    //txtPedido.Text = pedido.Numeracion.ToString("0000");
                    doc.CodTipoDocumento = pedido.CodTipoDocumento;
                    pedido.CodCotizacion = 0;
                    txtSerie.Text = pedido.SerieDoc;
                    CodSerie = pedido.CodSerie;
                    txtPedido.Text = pedido.Numeracion.ToString().PadLeft(8, '0');
                    txtCodigoBarras.Text = pedido.CodigoBarrasCifrado;
                    cmbFormaPago.SelectedIndex = pedido.FormaPago;

                    if (pedido.Boletafactura == 1) // 1 dni
                    {
                        CodCliente = pedido.CodCliente;
                        cli.CodCliente = CodCliente;
                        txtCodCliente.Text = pedido.DNI;
                        if (pedido.Nombrecliente != "") { txtNombreCliente.Text = pedido.Nombrecliente; } else { txtNombreCliente.Text = pedido.Nombre; }
                        // txtNombreCliente.Enabled = false;
                        txtDireccion.Text = pedido.Direccion;

                    }
                    else
                    {
                        CodCliente = pedido.CodCliente;
                        cli.CodCliente = CodCliente;
                        txtCodCliente.Text = pedido.RUCCliente;
                        if (pedido.RazonSocialCliente != "") txtNombreCliente.Text = pedido.RazonSocialCliente;
                        else txtNombreCliente.Text = pedido.Nombre;
                        txtDireccion.Text = pedido.Direccion;
                    }

                    dtpFecha.Value = pedido.FechaPedido;

                    if (txtDocRef.Enabled)
                    {
                        CodDocumento = pedido.CodTipoDocumento;
                        txtDocRef.Text = pedido.SiglaDocumento;
                        lbDocumento.Text = pedido.DescripcionDocumento;
                    }

                    txtBruto.Text = String.Format("{0:#,##0.00}", pedido.MontoBruto);
                    txtDscto.Text = String.Format("{0:#,##0.00}", pedido.MontoDscto);
                    txtValorVenta.Text = String.Format("{0:#,##0.00}", pedido.Total - pedido.Igv);
                    txtIGV.Text = String.Format("{0:#,##0.00}", pedido.Igv);
                    txtPrecioVenta.Text = String.Format("{0:#,##0.00}", pedido.Total);
                    montogravadas = pedido.Gravadas;
                    montoexoneradas = pedido.Exoneradas;
                    montoinafectas = pedido.Inafectas;
                    montogratuitas = pedido.Gratuitas;
                    txtPedido.Text = pedido.Numeracion.ToString().PadLeft(8, '0');

                    montosventa();
                    CargaDetalle();
                    if (editar)
                    {
                        pedido.Detalle = new List<clsDetallePedido>();
                        RecorreDetallePedido();

                    }

                    /*
                     * Cargar datos del vendedor
                     */
                    txtCodigoVendedor.Text = pedido.CodUser.ToString();
                    txtCodigoVendedor.Focus();
                    KeyEventArgs arg = new KeyEventArgs(Keys.Enter);
                    txtCodigoVendedor_KeyDown(new object(), arg);

                    /*
                     * Cargar cantidad de datos
                     */
                    // lblCantidadProductos.Text = "Productos Agregados: " + dgvDetalle.RowCount;
                }
                else
                {
                    MessageBox.Show("El documento solicitado no existe", "Nota de Ingreso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txtCodCliente_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void toolStripImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (clsFacturaVenta fv in lista_facturas)
                {
                    if (cargaPedido && !editar)
                    {
                        impresion = AdmVenta.chekeaImpresion(Convert.ToInt32(fv.CodFacturaVenta));
                        empresa = admEmpresa.CargaEmpresa(fv.CodEmpresa);

                        if (impresion == 0)
                        {

                            PrintaDocumento(fv);

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Se encontro el siguiente problema" + ex.Message, "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void labelX1_Click(object sender, EventArgs e)
        {

        }

        private void labelX5_Click(object sender, EventArgs e)
        {

        }

        private void toolStripAnulaov_Click(object sender, EventArgs e)
        {

        }

        private void dgvdetalle_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == (Keys.Delete))
            {

                if (dgvdetalle.Rows.Count > 0)
                {
                    if (dgvdetalle.CurrentCell != null)
                    {
                        if (dgvdetalle.CurrentCell.RowIndex != -1)
                        {

                            if (editar && !nuevaOV)
                            {
                                int coddetallep = Convert.ToInt32(dgvdetalle.Rows[dgvdetalle.CurrentCell.RowIndex].Cells["coddetalle"].Value);
                                AdmPedido.deletedetalle(coddetallep);

                            }

                            dgvdetalle.Rows.RemoveAt(dgvdetalle.CurrentCell.RowIndex);
                            //dgvdetalle.Refresh();

                            calculatotales();
                            montosventa();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("No hay elementos para eliminar");
                }
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    dgvproductos.DataSource = null;
            //    dgvproductos.Rows.Clear();
            //    tablaFiltro = AdmPro.RelacionSalidaTodo(1,frmLogin.iCodAlmacen,1);
            //    if (txtFiltro.Text.Length >= 1)
            //    {
            //        listatablas.Clear(); //listatablas01.Clear();
            //        String cad = txtFiltro.Text;
            //        String[] AUnidad = cad.Split(' ');
            //        DataTable datamel = null;
            //        for (int i = 0; i < AUnidad.Length; i++)
            //        {
            //            datamel = null;
            //            if (i == 0)
            //            {
            //                datamel = tablaFiltro.AsEnumerable()
            //                         .Where(r => r.Field<string>("descripcion").Contains(AUnidad[i].Trim()))
            //                         .CopyToDataTable();
            //                listatablas.Add(datamel);

            //            }
            //            else
            //            {
            //                for (int j = 0; j < listatablas.Count; j++)
            //                {
            //                    if (listatablas.Count <= i)
            //                    {
            //                        if (listatablas[j].AsEnumerable()
            //                                .Where(r => r.Field<string>("descripcion").Contains(AUnidad[i].Trim())).Any())
            //                        {
            //                            datamel = listatablas[j].AsEnumerable()
            //                                    .Where(r => r.Field<string>("descripcion").Contains(AUnidad[i].Trim()))
            //                                    .CopyToDataTable();
            //                            listatablas.Add(datamel);
            //                            break;
            //                        }
            //                    }
            //                }
            //            }

            //            if (datamel != null)
            //            {
            //                dgvproductos.DataSource = null;
            //                dgvproductos.Rows.Clear();
            //                dgvproductos.DataSource = datamel;
            //                //foreach (DataRow row in datamel.Rows)
            //                //{
            //                //    dgvproductos.Rows.Add(row);
            //                //}
            //            }

            //        }
            //    }
            //    else
            //    {
            //        dgvproductos.DataSource = null;
            //        dgvproductos.Rows.Clear();
            //        dgvproductos.DataSource = tablaFiltro;
            //        //foreach (DataRow row in tablaFiltro.Rows)
            //        //{
            //        //    dgvproductos.Rows.Add(row);
            //        //}
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message);
            //}
        }

        private void dgvproductos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void cargaAlmacenes()
        {

            cmbAlmacenes.DataSource = admalma.ListaAlmacen2();
            cmbAlmacenes.ValueMember = "codAlmacen";
            cmbAlmacenes.DisplayMember = "nombre";
            cmbAlmacenes.SelectedValue = frmLogin.iCodAlmacen;
        }

        private void cmbAlmacenes_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (nuevaOV || editar)
            {
                //if (Convert.ToInt32(cmbAlmacenes.SelectedValue) != frmLogin.iCodAlmacen)
                //{
                CargaProductos(Convert.ToInt32(cmbAlmacenes.SelectedValue));
                //}

            }
        }

        private void CargaPedidoVenta()
        {
            try
            {
                clsCliente cli2 = null;
                pedido = AdmPedido.CargaPedido(Convert.ToInt32(codPedidoVenta));
                if (pedido != null)
                {
                    textBoxX2.Text = pedido.SerieDoc;
                    textBoxX1.Text = pedido.Numeracion.ToString();
                    //txtPedido.Text = pedido.Numeracion.ToString("0000");
                    doc.CodTipoDocumento = pedido.CodTipoDocumento;
                    pedido.CodCotizacion = 0;
                    txtSerie.Text = pedido.SerieDoc;
                    CodSerie = pedido.CodSerie;
                    //txtPedido.Text = pedido.Numeracion.ToString().PadLeft(8, '0');
                    txtCodigoBarras.Text = pedido.CodigoBarrasCifrado;


                    if (pedido.Boletafactura == 1) // 1 dni
                    {
                        cli2 = AdmCli.MuestraCliente(pedido.CodCliente);
                        //CodCliente = pedido.CodCliente;
                        //cli.CodCliente = CodCliente;

                        if (cli2.Dni != "")
                        {
                            txtCodCliente.Text = cli2.Dni;
                        }
                        else
                        {
                            txtCodCliente.Text = cli2.RucDni;
                        }

                        if (cli2.Nombre != "") { txtNombreCliente.Text = pedido.Nombre; } else { txtNombreCliente.Text = pedido.RazonSocialCliente; }
                        // txtNombreCliente.Enabled = false;
                        txtDireccion.Text = pedido.Direccion;

                        cli2 = AdmCli.ConsultaCliente(pedido.DNI);
                        CargaCreditoCliente(cli2);
                        chkBoleta.Checked = true;
                        CodCliente = cli2.CodCliente;
                    }
                    else
                    {
                        cli2 = AdmCli.MuestraCliente(pedido.CodCliente);
                        //cli.CodCliente = CodCliente;
                        txtCodCliente.Text = cli2.RucDni;
                        if (cli2.RazonSocial != "") txtNombreCliente.Text = cli2.RazonSocial;
                        else txtNombreCliente.Text = cli2.RucDni;
                        txtDireccion.Text = cli2.DireccionLegal;
                        CargaCreditoCliente(cli2);
                        chkFactura.Checked = true;
                        CodCliente = cli2.CodCliente;
                    }

                    //BuscaCliente();

                    dtpFecha.Value = pedido.FechaPedido;

                    if (txtDocRef.Enabled)
                    {
                        CodDocumento = pedido.CodTipoDocumento;
                        txtDocRef.Text = pedido.SiglaDocumento;
                        lbDocumento.Text = pedido.DescripcionDocumento;
                    }

                    txtBruto.Text = String.Format("{0:#,##0.00}", pedido.MontoBruto);
                    txtDscto.Text = String.Format("{0:#,##0.00}", pedido.MontoDscto);
                    txtValorVenta.Text = String.Format("{0:#,##0.00}", pedido.Total - pedido.Igv);
                    txtIGV.Text = String.Format("{0:#,##0.00}", pedido.Igv);
                    txtPrecioVenta.Text = String.Format("{0:#,##0.00}", pedido.Total);
                    montogravadas = pedido.Gravadas;
                    montoexoneradas = pedido.Exoneradas;
                    montoinafectas = pedido.Inafectas;
                    montogratuitas = pedido.Gratuitas;
                    txtPedido.Text = pedido.Numeracion.ToString().PadLeft(8, '0');

                    CargaDetalle();
                    calculatotales();
                    montosventa();

                    /*
                     * Cargar datos del vendedor
                     */
                    txtCodigoVendedor.Text = pedido.CodUser.ToString();
                    txtCodigoVendedor.Focus();
                    KeyEventArgs arg = new KeyEventArgs(Keys.Enter);
                    txtCodigoVendedor_KeyDown(new object(), arg);


                    cmbFormaPago.SelectedValue = pedido.FormaPago;
                    /*
                     * Cargar cantidad de datos
                     */
                    // lblCantidadProductos.Text = "Productos Agregados: " + dgvDetalle.RowCount;
                }
                else
                {
                    MessageBox.Show("El documento solicitado no existe", "Nota de Ingreso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void RecorreDetallePedido()
        {
            pedido.Detalle.Clear();
            if (dgvdetalle.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in dgvdetalle.Rows)
                {
                    añadedetallePedido(row);
                }
            }
        }

        private void añadedetallePedido(DataGridViewRow fila)
        {
            clsDetallePedido deta = new clsDetallePedido();
            deta.CodDetallePedido = Convert.ToInt32(fila.Cells[coddetalle.Name].Value);
            deta.CodProducto = Convert.ToInt32(fila.Cells[codproducto.Name].Value);
            deta.CodPedido = Convert.ToInt32(pedido.CodPedido);
            deta.CodAlmacen = frmLogin.iCodAlmacen;
            deta.UnidadIngresada = Convert.ToInt32(fila.Cells[codunidad.Name].Value);

            deta.Cantidad = Convert.ToDouble(fila.Cells[cantidad.Name].Value);

            deta.PrecioUnitario = Convert.ToDouble(fila.Cells[preciounit.Name].Value);
            deta.Subtotal = Convert.ToDouble(fila.Cells[importe.Name].Value);
            deta.Descuento1 = Convert.ToDouble(fila.Cells[dscto1.Name].Value);
            deta.Descuento2 = Convert.ToDouble(fila.Cells[dscto2.Name].Value);
            deta.Descuento3 = Convert.ToDouble(fila.Cells[dscto3.Name].Value);
            deta.Descuento3 = Convert.ToDouble(fila.Cells[dscto3.Name].Value);
            deta.Igv = Convert.ToDouble(fila.Cells[igv.Name].Value);
            deta.Importe = Convert.ToDouble(fila.Cells[precioventa.Name].Value);
            deta.PrecioReal = Convert.ToDouble(fila.Cells[precioreal.Name].Value);
            deta.ValoReal = Convert.ToDouble(fila.Cells[valoreal.Name].Value);
            deta.Precioigv = Convert.ToBoolean(Convert.ToInt32(fila.Cells[precioconigv.Name].Value));
            deta.Valorpromedio = Convert.ToDecimal(fila.Cells[valorpromedio.Name].Value);
            deta.CodUser = frmLogin.iCodUser;

            pedido.Detalle.Add(deta);
        }

        private void CargaDetalle()
        {
            DataTable newData = new DataTable();
            dgvdetalle.Rows.Clear();
            try
            {
                newData = AdmPedido.CargaDetalle(Convert.ToInt32(pedido.CodPedido));
                foreach (DataRow row in newData.Rows)
                {
                        dgvdetalle.Rows.Add(row[0].ToString(), row[1].ToString(), row[2].ToString(), row[3].ToString(), row[4].ToString(),
                        row[5].ToString(), row[6].ToString(), row[7].ToString(), row[8].ToString(), row[9].ToString(),
                        row[10].ToString(), row[11].ToString(), row[12].ToString(), row[13].ToString(), row[14].ToString(),
                        row[15].ToString(), row[16].ToString(), row[17].ToString(), row[18].ToString(),
                        row[19].ToString(), row[20].ToString(), row[21].ToString(), row[22].ToString(), row[23].ToString(), row[24].ToString(), row[25].ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void montosventa()
        {
            if (montogravadas > 0)
            {
                txtgravadas.Clear();
                txtgravadas.Text = String.Format("{0:#,##0.00}", montogravadas);
            }
            else { txtgravadas.Text = String.Format("{0:#,##0.00}", 0); }

            if (montogratuitas > 0)
            {
                txtgratuitas.Clear();
                txtgratuitas.Text = String.Format("{0:#,##0.00}", montogratuitas);
            }
            else { txtgratuitas.Text = String.Format("{0:#,##0.00}", 0); }

            if (montoexoneradas > 0)
            {
                txtexoneradas.Clear();
                txtexoneradas.Text = String.Format("{0:#,##0.00}", montoexoneradas);
            }
            else { txtexoneradas.Text = String.Format("{0:#,##0.00}", 0); }

            if (montoinafectas > 0)
            {
                txtinafectas.Clear();
                txtinafectas.Text = String.Format("{0:#,##0.00}", montoinafectas);
            }
            else { txtinafectas.Text = String.Format("{0:#,##0.00}", 0); }
        }

        private void RecorreDetalle()
        {
            detalle.Clear();
            if (dgvdetalle.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in dgvdetalle.Rows)
                {
                        añadedetalle(row);
                }
            }
        }

        private void añadedetalle(DataGridViewRow fila)
        {
            clsDetallePedido deta = new clsDetallePedido();
            deta.CodDetallePedido = Convert.ToInt32(fila.Cells[coddetalle.Name].Value);
            deta.CodProducto = Convert.ToInt32(fila.Cells[codproducto.Name].Value);
            deta.CodPedido = Convert.ToInt32(pedido.CodPedido);
            deta.CodAlmacen = Convert.ToInt32(fila.Cells[codalmacen.Name].Value);
            deta.UnidadIngresada = Convert.ToInt32(fila.Cells[codunidad.Name].Value);
            deta.Cantidad = Convert.ToDouble(fila.Cells[cantidad.Name].Value);
            deta.PrecioUnitario = Convert.ToDouble(fila.Cells[preciounit.Name].Value);
            deta.Subtotal = Convert.ToDouble(fila.Cells[importe.Name].Value);
            deta.Descuento1 = Convert.ToDouble(fila.Cells[dscto1.Name].Value);
            deta.Descuento2 = 0;
            deta.Descuento3 = 0;
            deta.MontoDescuento = Convert.ToDouble(fila.Cells[montodscto.Name].Value);
            deta.Igv = Convert.ToDouble(fila.Cells[igv.Name].Value);
            deta.Importe = Convert.ToDouble(fila.Cells[precioventa.Name].Value);
            deta.PrecioReal = Convert.ToDouble(fila.Cells[precioreal.Name].Value);
            deta.ValoReal = Convert.ToDouble(fila.Cells[valoreal.Name].Value);
            deta.Precioigv = Convert.ToBoolean(Convert.ToInt32(fila.Cells[precioconigv.Name].Value));
            deta.Valorpromedio = Convert.ToDecimal(fila.Cells[valorpromedio.Name].Value);
            deta.CodUser = frmLogin.iCodUser;
            deta.Tipoimpuesto = fila.Cells[Tipoimpuesto.Name].Value.ToString();
            deta.CodEmpresa = Convert.ToInt32(fila.Cells[empres.Name].Value.ToString());
            /*deta.CodEmpresa = Convert.ToInt32(fila.Cells[CodEmpresa.Name].Value);//desde esto toma valor nulo
            deta.Tipoimpuesto = fila.Cells[Tipoimpuesto.Name].Value.ToString();
            deta.TipoUnidad = Convert.ToInt32(fila.Cells[TipoUnidad.Name].Value);*/
            //deta.
            detalle.Add(deta);
        }


        private void RecorreDetalleVenta(int codalma)
        {
            detalle.Clear();
            detalle1.Clear();

            if (dgvdetalle.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in dgvdetalle.Rows)
                {
                    if (Convert.ToInt32(row.Cells[codalmacen.Name].Value) == codalma)
                    {
                        añadedetalleVenta(row);
                    }
                }
            }
        }
        private void añadedetalleVenta(DataGridViewRow fila)
        {

            clsDetalleFacturaVenta deta = new clsDetalleFacturaVenta();
            deta.CodProducto = Convert.ToInt32(fila.Cells[codproducto.Name].Value);
            deta.CodVenta = Convert.ToInt32(venta.CodFacturaVenta);
            deta.CodAlmacen = /*frmLogin.iCodAlmacen;*/Convert.ToInt32(fila.Cells[codalmacen.Name].Value);
            deta.UnidadIngresada = Convert.ToInt32(fila.Cells[codunidad.Name].Value);
            deta.SerieLote = "";
            deta.Cantidad = Convert.ToDouble(fila.Cells[cantidad.Name].Value);
            deta.PrecioUnitario = Convert.ToDouble(fila.Cells[preciounit.Name].Value);
            deta.Subtotal = Convert.ToDouble(fila.Cells[importe.Name].Value);
            deta.Descuento1 = Convert.ToDouble(fila.Cells[dscto1.Name].Value);
            deta.MontoDescuento = Convert.ToDouble(fila.Cells[montodscto.Name].Value);
            deta.Igv = Convert.ToDouble(fila.Cells[igv.Name].Value);
            deta.Importe = Convert.ToDouble(fila.Cells[precioventa.Name].Value);
            deta.PrecioReal = Convert.ToDouble(fila.Cells[precioreal.Name].Value);
            deta.ValoReal = Convert.ToDouble(fila.Cells[valoreal.Name].Value);
            deta.CodUser = frmLogin.iCodUser;
            deta.CantidadPendiente = Convert.ToDouble(fila.Cells[cantidad.Name].Value);
            deta.Moneda = 1;
            deta.Descripcion = fila.Cells[product.Name].Value.ToString();

            deta.Tipoimpuesto = fila.Cells[Tipoimpuesto.Name].Value.ToString();
            deta.Entregado = true;
            deta.TipoUnidad = Convert.ToInt32(fila.Cells[TipoUnidad.Name].Value);
            deta.CodDetalleCotizacion = 0;
            //deta.ProductoVentaTicket = Convert.ToInt32(fila.Cells[venta_tickeck.Name].Value);
            //deta.CodigoProductoSunat = fila.Cells[codigoproductosunat.Name].Value.ToString();

            deta.ProductoVentaTicket = 0;
            //deta.CodigoProductoSunat = fila.Cells[codigoproductosunat.Name].Value.ToString();
            deta.CodigoProductoSunat = "00000 ";

            deta.CodDetallePedido = Convert.ToInt32(fila.Cells[coddetalle.Name].Value);

            detalle1.Add(deta);

        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Alt | Keys.Q))
            {
                //cancelarventa
                cancelaVenta();
            }

            if (keyData == (Keys.Alt | Keys.R))
            {
                //codcliente
                txtCodCliente.Focus();
            }

            if (keyData == (Keys.Alt | Keys.C))
            {
                //cliente
                txtNombreCliente.Focus();
            }


            if (keyData == (Keys.Alt | Keys.D))
            {
                //direccion

                txtDireccion.Focus();
                ActiveControl = txtDireccion;
            }

            if (keyData == (Keys.Alt | Keys.G) && toolStripGuardar.Enabled == true)
            {
                //boton guardar
                toolStripGuardar_Click(null, null);
            }

            if (keyData == (Keys.Alt | Keys.B))
            {
                //busqueda
                txtFiltro.Focus();
            }

            if (keyData == (Keys.Alt | Keys.P))
            {

                //grilla productos
                dgvproductos.Focus();

                if (dgvproductos.Rows.Count > 0)
                {

                    dgvproductos.Rows[0].Cells[9].Selected = true;
                    dgvproductos.CurrentCell = dgvproductos.CurrentRow.Cells[9];
                }
                ActiveControl = dgvproductos;
            }


            if (keyData == (Keys.Alt | Keys.D))
            {
                //grilla detalle
                dgvdetalle.Focus();

                if (dgvdetalle.Rows.Count > 0)
                {
                    dgvdetalle.Rows[0].Cells[7].Selected = true;
                    dgvdetalle.CurrentCell = dgvproductos.CurrentRow.Cells[7];
                }

                ActiveControl = dgvdetalle;
            }

            if (keyData == (Keys.Alt | Keys.V))
            {
                //vendedor

                txtCodigoVendedor.Focus();
                //if (txtCodigoVendedor.Text.Trim() != "")
                //{
                //    Int32 codigoUsuario = Convert.ToInt32(txtCodigoVendedor.Text);
                //    /*
                //     * consultar usuario sin considerar usuario
                //     * con nombre admin
                //     */
                //    vendedor = admUsuario.MuestraUsuarioSinAdmin(codigoUsuario);
                //    if (vendedor != null)
                //    {
                //        txtNombreVendedor.Text = vendedor.Nombre + " " + vendedor.Apellido;
                //    }
                //    else
                //    {
                //        txtCodigoVendedor.Text = "";
                //        txtNombreVendedor.Text = "";
                //        MessageBox.Show("No se encontró ningún vendedor con el código ingresado",
                //                        "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //    }

                //}
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void limpiarTotales()
        {
            foreach (Control c in groupBox5.Controls)
            {
                if (c is TextBox)
                {
                    c.Text = "";
                }
            }
        }

        private void cancelaVenta()
        {
            sololectura(true);
            activaPaneles(false);
            dgvdetalle.DataSource = null;
            dgvdetalle.Rows.Clear();
            dgvStockAlmacenes.DataSource = null;
            dgvStockAlmacenes.Rows.Clear();
            limpiarTotales();
            toolStripAnulaov.Enabled = false;
            editar = false;
            cargaPedido = false;
            nuevaOV = false;
        }
    }
}
