namespace SIGEFA.Formularios
{
    partial class frmVerCompras
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVerCompras));
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.dgvFacturas = new System.Windows.Forms.DataGridView();
			this.btnConsultar = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.dtpDesde = new System.Windows.Forms.DateTimePicker();
			this.dtpHasta = new System.Windows.Forms.DateTimePicker();
			this.btnSalir = new System.Windows.Forms.Button();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.btnAnular = new System.Windows.Forms.Button();
			this.Documento = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.codfactura = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.RUC = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.proveedor = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.codProveedor = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.fecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.fechapago = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.responsable = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Comentario = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Estado = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.cancelado = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.codNotaI = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvFacturas)).BeginInit();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.dgvFacturas);
			this.groupBox1.Location = new System.Drawing.Point(1, 4);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(846, 368);
			this.groupBox1.TabIndex = 1;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Vigentes";
			// 
			// dgvFacturas
			// 
			this.dgvFacturas.AllowUserToAddRows = false;
			this.dgvFacturas.AllowUserToDeleteRows = false;
			this.dgvFacturas.AllowUserToOrderColumns = true;
			this.dgvFacturas.AllowUserToResizeRows = false;
			this.dgvFacturas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.dgvFacturas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Documento,
            this.codfactura,
            this.RUC,
            this.proveedor,
            this.codProveedor,
            this.fecha,
            this.fechapago,
            this.responsable,
            this.Comentario,
            this.Estado,
            this.cancelado,
            this.codNotaI});
			this.dgvFacturas.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgvFacturas.Location = new System.Drawing.Point(3, 16);
			this.dgvFacturas.MultiSelect = false;
			this.dgvFacturas.Name = "dgvFacturas";
			this.dgvFacturas.ReadOnly = true;
			this.dgvFacturas.RowHeadersVisible = false;
			this.dgvFacturas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvFacturas.Size = new System.Drawing.Size(840, 349);
			this.dgvFacturas.TabIndex = 0;
			this.dgvFacturas.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFacturas_CellDoubleClick);
			this.dgvFacturas.RowStateChanged += new System.Windows.Forms.DataGridViewRowStateChangedEventHandler(this.dgvFacturas_RowStateChanged);
			// 
			// btnConsultar
			// 
			this.btnConsultar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnConsultar.BackColor = System.Drawing.Color.CornflowerBlue;
			this.btnConsultar.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue;
			this.btnConsultar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnConsultar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnConsultar.ForeColor = System.Drawing.SystemColors.ControlLightLight;
			this.btnConsultar.Image = ((System.Drawing.Image)(resources.GetObject("btnConsultar.Image")));
			this.btnConsultar.Location = new System.Drawing.Point(343, 401);
			this.btnConsultar.Name = "btnConsultar";
			this.btnConsultar.Size = new System.Drawing.Size(148, 32);
			this.btnConsultar.TabIndex = 29;
			this.btnConsultar.Text = "BUSCAR";
			this.btnConsultar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.btnConsultar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.btnConsultar.UseVisualStyleBackColor = false;
			this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
			// 
			// label6
			// 
			this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(176, 392);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(57, 16);
			this.label6.TabIndex = 28;
			this.label6.Text = "Hasta :";
			// 
			// label5
			// 
			this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(12, 392);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(62, 16);
			this.label5.TabIndex = 27;
			this.label5.Text = "Desde :";
			// 
			// dtpDesde
			// 
			this.dtpDesde.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.dtpDesde.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dtpDesde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtpDesde.Location = new System.Drawing.Point(15, 412);
			this.dtpDesde.Name = "dtpDesde";
			this.dtpDesde.Size = new System.Drawing.Size(158, 22);
			this.dtpDesde.TabIndex = 26;
			// 
			// dtpHasta
			// 
			this.dtpHasta.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.dtpHasta.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dtpHasta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtpHasta.Location = new System.Drawing.Point(179, 411);
			this.dtpHasta.Name = "dtpHasta";
			this.dtpHasta.Size = new System.Drawing.Size(158, 22);
			this.dtpHasta.TabIndex = 25;
			// 
			// btnSalir
			// 
			this.btnSalir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSalir.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
			this.btnSalir.Location = new System.Drawing.Point(763, 402);
			this.btnSalir.Name = "btnSalir";
			this.btnSalir.Size = new System.Drawing.Size(75, 32);
			this.btnSalir.TabIndex = 30;
			this.btnSalir.Text = "SALIR";
			this.btnSalir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.btnSalir.UseVisualStyleBackColor = true;
			this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
			// 
			// imageList1
			// 
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
			this.imageList1.Images.SetKeyName(0, "exit.png");
			this.imageList1.Images.SetKeyName(1, "pedido.png");
			this.imageList1.Images.SetKeyName(2, "carrito.png");
			this.imageList1.Images.SetKeyName(3, "delete-file-icon.png");
			this.imageList1.Images.SetKeyName(4, "DeleteRed.png");
			this.imageList1.Images.SetKeyName(5, "document_delete.png");
			// 
			// btnAnular
			// 
			this.btnAnular.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnAnular.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnAnular.Image = ((System.Drawing.Image)(resources.GetObject("btnAnular.Image")));
			this.btnAnular.Location = new System.Drawing.Point(600, 402);
			this.btnAnular.Name = "btnAnular";
			this.btnAnular.Size = new System.Drawing.Size(157, 32);
			this.btnAnular.TabIndex = 31;
			this.btnAnular.Text = "ANULAR FACTURA";
			this.btnAnular.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.btnAnular.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.btnAnular.UseVisualStyleBackColor = true;
			this.btnAnular.Click += new System.EventHandler(this.btnAnular_Click);
			// 
			// Documento
			// 
			this.Documento.DataPropertyName = "documento";
			this.Documento.HeaderText = "Documento";
			this.Documento.Name = "Documento";
			this.Documento.ReadOnly = true;
			// 
			// codfactura
			// 
			this.codfactura.DataPropertyName = "codfactura";
			this.codfactura.HeaderText = "codfactura";
			this.codfactura.Name = "codfactura";
			this.codfactura.ReadOnly = true;
			this.codfactura.Visible = false;
			// 
			// RUC
			// 
			this.RUC.DataPropertyName = "ruc";
			this.RUC.HeaderText = "Ruc";
			this.RUC.Name = "RUC";
			this.RUC.ReadOnly = true;
			// 
			// proveedor
			// 
			this.proveedor.DataPropertyName = "razonsocial";
			this.proveedor.HeaderText = "Proveedor";
			this.proveedor.Name = "proveedor";
			this.proveedor.ReadOnly = true;
			this.proveedor.Width = 270;
			// 
			// codProveedor
			// 
			this.codProveedor.DataPropertyName = "codProveedor";
			this.codProveedor.HeaderText = "codProveedor";
			this.codProveedor.Name = "codProveedor";
			this.codProveedor.ReadOnly = true;
			this.codProveedor.Visible = false;
			// 
			// fecha
			// 
			this.fecha.DataPropertyName = "fecha";
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
			this.fecha.DefaultCellStyle = dataGridViewCellStyle1;
			this.fecha.HeaderText = "Fecha";
			this.fecha.Name = "fecha";
			this.fecha.ReadOnly = true;
			this.fecha.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.fecha.Width = 90;
			// 
			// fechapago
			// 
			this.fechapago.DataPropertyName = "fechapago";
			this.fechapago.HeaderText = "Vence";
			this.fechapago.Name = "fechapago";
			this.fechapago.ReadOnly = true;
			// 
			// responsable
			// 
			this.responsable.DataPropertyName = "responsable";
			this.responsable.HeaderText = "Responsable";
			this.responsable.Name = "responsable";
			this.responsable.ReadOnly = true;
			this.responsable.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.responsable.Width = 180;
			// 
			// Comentario
			// 
			this.Comentario.DataPropertyName = "comentario";
			this.Comentario.HeaderText = "Comentario";
			this.Comentario.Name = "Comentario";
			this.Comentario.ReadOnly = true;
			this.Comentario.Visible = false;
			// 
			// Estado
			// 
			this.Estado.DataPropertyName = "Estado";
			this.Estado.HeaderText = "Estado";
			this.Estado.Name = "Estado";
			this.Estado.ReadOnly = true;
			// 
			// cancelado
			// 
			this.cancelado.DataPropertyName = "cancelado";
			this.cancelado.HeaderText = "Cancelado";
			this.cancelado.Name = "cancelado";
			this.cancelado.ReadOnly = true;
			this.cancelado.Visible = false;
			// 
			// codNotaI
			// 
			this.codNotaI.DataPropertyName = "codNotaI";
			this.codNotaI.HeaderText = "CodigoNotaIngreso";
			this.codNotaI.Name = "codNotaI";
			this.codNotaI.ReadOnly = true;
			this.codNotaI.Visible = false;
			// 
			// frmVerCompras
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(850, 445);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.btnSalir);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.btnAnular);
			this.Controls.Add(this.btnConsultar);
			this.Controls.Add(this.dtpDesde);
			this.Controls.Add(this.dtpHasta);
			this.Controls.Add(this.groupBox1);
			this.DoubleBuffered = true;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "frmVerCompras";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Facturas";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.Load += new System.EventHandler(this.frmVerCompras_Load);
			this.groupBox1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvFacturas)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgvFacturas;
        private System.Windows.Forms.Button btnConsultar;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtpDesde;
        private System.Windows.Forms.DateTimePicker dtpHasta;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button btnAnular;
		private System.Windows.Forms.DataGridViewTextBoxColumn Documento;
		private System.Windows.Forms.DataGridViewTextBoxColumn codfactura;
		private System.Windows.Forms.DataGridViewTextBoxColumn RUC;
		private System.Windows.Forms.DataGridViewTextBoxColumn proveedor;
		private System.Windows.Forms.DataGridViewTextBoxColumn codProveedor;
		private System.Windows.Forms.DataGridViewTextBoxColumn fecha;
		private System.Windows.Forms.DataGridViewTextBoxColumn fechapago;
		private System.Windows.Forms.DataGridViewTextBoxColumn responsable;
		private System.Windows.Forms.DataGridViewTextBoxColumn Comentario;
		private System.Windows.Forms.DataGridViewTextBoxColumn Estado;
		private System.Windows.Forms.DataGridViewTextBoxColumn cancelado;
		private System.Windows.Forms.DataGridViewTextBoxColumn codNotaI;
	}
}