﻿using MySql.Data.MySqlClient;
using SIGEFA.Conexion;
using SIGEFA.Interfaces;
using System;
using System.Data;

namespace SIGEFA.InterMySql
{
	class MysqlParametro : IParametro
	{
		clsConexionMysql con = new clsConexionMysql();
		MySqlCommand cmd = null;
		MySqlDataReader dr = null;
		MySqlDataAdapter adap = null;

		public bool ActualizarParametroVenta(string valorParametro)
		{
			try
			{
				con.conectarBD();

				cmd = new MySqlCommand("ActualizaParametroVenta", con.conector);
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.Parameters.AddWithValue("valor_parametro", valorParametro);
				int x = cmd.ExecuteNonQuery();

				return (x != 0) ? true : false;
			}
			catch (MySqlException ex)
			{
				throw ex;
			}
			finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
		}

		public Boolean ConsultarParametroVenta(Int32 codigo)
		{
			Boolean todasLasVentas = false;
			try
			{
				con.conectarBD();
				cmd = new MySqlCommand("ConsultarParametroVenta", con.conector);
				cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("codParametro_ex", codigo);
                dr = cmd.ExecuteReader();
				if (dr.HasRows)
				{
					while (dr.Read())
					{
						todasLasVentas = dr.GetBoolean(0);
					}
				}
				return todasLasVentas;
			}
			catch (MySqlException ex)
			{
				throw ex;

			}
			finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
		}

        public bool actualizaDocumentoVenta(string valorParametro)
        {
            try
            {
                con.conectarBD();

                cmd = new MySqlCommand("actualizaDocumentoVenta", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("valor_parametro", valorParametro);
                int x = cmd.ExecuteNonQuery();

                return (x != 0) ? true : false;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    }
}
