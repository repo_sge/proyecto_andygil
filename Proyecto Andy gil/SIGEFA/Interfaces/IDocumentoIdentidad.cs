﻿using SIGEFA.Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace SIGEFA.Interfaces
{
	interface IDocumentoIdentidad
	{
		/**
		 * codigoTipoDocumento: codigo de Documento de Facturacion (BOLETA O FACTURA)
		 */
		DataTable ListaDocumentoIdentidad(Int32 codigoTipoDocumento);
		clsDocumentoIdentidad MuestraDocumentoIdentidad(Int32 codigoDocumentoIdentidad);
		clsDocumentoIdentidad ObtenerDocumentoIdentidadDeVenta(Int32 codigoFacturaVenta);
	}
}
