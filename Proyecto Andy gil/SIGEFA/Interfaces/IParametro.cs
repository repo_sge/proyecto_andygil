﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIGEFA.Interfaces
{
	interface IParametro
	{
		Boolean ConsultarParametroVenta(Int32 codigo);
		Boolean ActualizarParametroVenta(String valorParametro);
        Boolean actualizaDocumentoVenta(String valorParametro);
    }
}
