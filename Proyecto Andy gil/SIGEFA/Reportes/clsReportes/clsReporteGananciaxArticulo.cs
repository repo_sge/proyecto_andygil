﻿using MySql.Data.MySqlClient;
using SIGEFA.Conexion;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
namespace SIGEFA.Reportes.clsReportes
{
    public class clsReporteGananciaxArticulo
    {

        clsConexionMysql con = new clsConexionMysql();
        MySqlCommand cmd = null;
        MySqlDataReader dr = null;
        MySqlDataAdapter adap = null;
        DataSet set = null;

        public DataSet ReportGananciaxArticulo(Int32 codigoProducto, DateTime fecha_inicio, DateTime fecha_fin)
        {
            try
            {
                set = new DataSet();
                con.conectarBD();
                cmd = new MySqlCommand("ReportGananciaPorArticulo", con.conector);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 15;
                cmd.Parameters.AddWithValue("codigo_producto", codigoProducto);
                cmd.Parameters.AddWithValue("fecha_inicio", fecha_inicio);
                cmd.Parameters.AddWithValue("fecha_fin", fecha_fin);
                adap = new MySqlDataAdapter(cmd);
                adap.Fill(set, "dt_ReporteGananciaPorArticulo");
                set.WriteXml("C:\\XML\\GananciaPorArticuloRPT.xml", XmlWriteMode.WriteSchema);
                return set;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally { con.conector.Dispose(); cmd.Dispose(); con.desconectarBD(); }
        }
    }
}
