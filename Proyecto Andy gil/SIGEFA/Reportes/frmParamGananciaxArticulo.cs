﻿using SIGEFA.Administradores;
using SIGEFA.Entidades;
using SIGEFA.Formularios;
using SIGEFA.Reportes.clsReportes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SIGEFA.Reportes
{
    public partial class frmParamGananciaxArticulo : Form
    {

        private clsReporteGananciaxArticulo ds = new clsReporteGananciaxArticulo();
        private clsAdmProducto AdmPro = new clsAdmProducto();
        private clsProducto productoSeleccionado = new clsProducto();

        public frmParamGananciaxArticulo()
        {
            InitializeComponent();
        }


        private void CargaProducto(Int32 Codigo)
        {
            productoSeleccionado = AdmPro.CargaProducto(Codigo, frmLogin.iCodAlmacen);
            txtBusquedaProducto.Text = productoSeleccionado.Referencia;
            txtNombreProducto.Text = productoSeleccionado.Descripcion;
        }

        private void btnReporte_Click(object sender, EventArgs e)
        {
            Int32 codigoProducto = (productoSeleccionado == null) ? 0 : productoSeleccionado.CodProducto;
            CRGananciaxArticulo rpt = new CRGananciaxArticulo();
            frmRptGananciaxArticulo frm = new frmRptGananciaxArticulo();
            DataTable dt = ds.ReportGananciaxArticulo(codigoProducto, dtpDesde.Value, dtpHasta.Value).Tables[0];
            if (dt.Rows.Count > 0)
            {
                rpt.SetDataSource(dt);
                frm.crvRptGananciaxArticulo.ReportSource = rpt;
                frm.Show();
            }
            else
            {
                MessageBox.Show("No se encontraron resultados con los parámetros seleccionados",
                                "Reporte de Ganancia por Articulo", MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
        }

        private void frmParamGananciaxArticulo_Load(object sender, EventArgs e)
        {

        }

        private void txtBusquedaProducto_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                frmProductosLista frm = new frmProductosLista();
                frm.Procede = 15; //(15) Procede desde el formulario frmParamVentxVendedor
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    if (frm.pro.CodProducto != 0)
                    {
                        CargaProducto(frm.pro.CodProducto);
                    }
                }
            }
        }

        private void rbProductoEspecifico_CheckedChanged(object sender, EventArgs e)
        {
            if (rbProductoEspecifico.Checked)
            {
                txtBusquedaProducto.Enabled = true;
                txtBusquedaProducto.Focus();
            }
        }

        private void rbTodos_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTodos.Checked)
            {
                txtBusquedaProducto.Enabled = false;
                txtBusquedaProducto.Text = "";
                txtNombreProducto.Text = "";
                productoSeleccionado = null;
            }
        }
    }
}
