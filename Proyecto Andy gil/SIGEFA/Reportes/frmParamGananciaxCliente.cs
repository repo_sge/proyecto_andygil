﻿using SIGEFA.Administradores;
using SIGEFA.Entidades;
using SIGEFA.Formularios;
using SIGEFA.Reportes.clsReportes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SIGEFA.Reportes
{
    public partial class frmParamGananciaxCliente : Form
    {

        private clsReporteGananciaxCliente ds = new clsReporteGananciaxCliente();
        private clsAdmCliente AdmCliente = new clsAdmCliente();
        private clsCliente clienteSeleccionado = new clsCliente();


        public frmParamGananciaxCliente()
        {
            InitializeComponent();
        }

        private void frmParamGananciaxCliente_Load(object sender, EventArgs e)
        {

        }

        private void btnReporte_Click(object sender, EventArgs e)
        {
            Int32 codigoCliente = (clienteSeleccionado == null) ? 0 : clienteSeleccionado.CodCliente;
            CRGananciaxCliente rpt = new CRGananciaxCliente();
            frmRptGananciaxCliente frm = new frmRptGananciaxCliente();
            DataTable dt = ds.ReportGananciaxCliente(codigoCliente, dtpDesde.Value, dtpHasta.Value).Tables[0];
            if (dt.Rows.Count > 0)
            {
                rpt.SetDataSource(dt);
                frm.crvRptGananciaxCliente.ReportSource = rpt;
                frm.Show();
            }
            else
            {
                MessageBox.Show("No se encontraron resultados con los parámetros seleccionados",
                                "Reporte de Ganancia por Cliente", MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
        }


        private void CargaCliente(Int32 Codigo)
        {
            clienteSeleccionado = AdmCliente.MuestraCliente(Codigo);
            txtBusquedaCliente.Text = clienteSeleccionado.RucDni;
            txtNombreCliente.Text = clienteSeleccionado.RazonSocial;
        }



        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtBusquedaCliente_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.F1)
            {
                frmClientesLista frm = new frmClientesLista();
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    CargaCliente(frm.GetCodigoCliente());
                }
            }
        }

        private void rbClienteEspecifico_CheckedChanged(object sender, EventArgs e)
        {
            if (rbClienteEspecifico.Checked)
            {
                txtBusquedaCliente.Enabled = true;
                txtBusquedaCliente.Focus();
            }
        }

        private void rbTodos_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTodos.Checked)
            {
                txtBusquedaCliente.Enabled = false;
                txtBusquedaCliente.Text = "";
                txtNombreCliente.Text = "";
                clienteSeleccionado = null;
            }
        }
    }
}
