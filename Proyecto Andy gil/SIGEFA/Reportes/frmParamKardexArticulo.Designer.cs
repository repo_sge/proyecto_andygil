﻿namespace SIGEFA.Reportes
{
    partial class frmParamKardexArticulo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.txtArticulo = new System.Windows.Forms.TextBox();
			this.txtUnArt = new System.Windows.Forms.TextBox();
			this.rbArt = new System.Windows.Forms.RadioButton();
			this.rbTodosArt = new System.Windows.Forms.RadioButton();
			this.btnCancelar = new System.Windows.Forms.Button();
			this.btnReporte = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.dtpFecha2 = new System.Windows.Forms.DateTimePicker();
			this.dtpFecha1 = new System.Windows.Forms.DateTimePicker();
			this.label8 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox4.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.txtArticulo);
			this.groupBox4.Controls.Add(this.txtUnArt);
			this.groupBox4.Controls.Add(this.rbArt);
			this.groupBox4.Controls.Add(this.rbTodosArt);
			this.groupBox4.Location = new System.Drawing.Point(16, 112);
			this.groupBox4.Margin = new System.Windows.Forms.Padding(4);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Padding = new System.Windows.Forms.Padding(4);
			this.groupBox4.Size = new System.Drawing.Size(603, 96);
			this.groupBox4.TabIndex = 71;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "PRODUCTO";
			// 
			// txtArticulo
			// 
			this.txtArticulo.Enabled = false;
			this.txtArticulo.Location = new System.Drawing.Point(259, 25);
			this.txtArticulo.Margin = new System.Windows.Forms.Padding(4);
			this.txtArticulo.Name = "txtArticulo";
			this.txtArticulo.Size = new System.Drawing.Size(321, 22);
			this.txtArticulo.TabIndex = 63;
			// 
			// txtUnArt
			// 
			this.txtUnArt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtUnArt.Location = new System.Drawing.Point(139, 25);
			this.txtUnArt.Margin = new System.Windows.Forms.Padding(4);
			this.txtUnArt.Name = "txtUnArt";
			this.txtUnArt.Size = new System.Drawing.Size(111, 22);
			this.txtUnArt.TabIndex = 61;
			this.txtUnArt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtUnArt_KeyDown);
			// 
			// rbArt
			// 
			this.rbArt.AutoSize = true;
			this.rbArt.BackColor = System.Drawing.Color.Transparent;
			this.rbArt.Checked = true;
			this.rbArt.Location = new System.Drawing.Point(25, 28);
			this.rbArt.Margin = new System.Windows.Forms.Padding(4);
			this.rbArt.Name = "rbArt";
			this.rbArt.Size = new System.Drawing.Size(90, 20);
			this.rbArt.TabIndex = 57;
			this.rbArt.TabStop = true;
			this.rbArt.Text = "Un Artículo";
			this.rbArt.UseVisualStyleBackColor = false;
			this.rbArt.CheckedChanged += new System.EventHandler(this.rbArt_CheckedChanged);
			// 
			// rbTodosArt
			// 
			this.rbTodosArt.AutoSize = true;
			this.rbTodosArt.BackColor = System.Drawing.Color.Transparent;
			this.rbTodosArt.Location = new System.Drawing.Point(25, 59);
			this.rbTodosArt.Margin = new System.Windows.Forms.Padding(4);
			this.rbTodosArt.Name = "rbTodosArt";
			this.rbTodosArt.Size = new System.Drawing.Size(140, 20);
			this.rbTodosArt.TabIndex = 54;
			this.rbTodosArt.Text = "Todos los artículos";
			this.rbTodosArt.UseVisualStyleBackColor = false;
			// 
			// btnCancelar
			// 
			this.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnCancelar.ImageIndex = 0;
			this.btnCancelar.Location = new System.Drawing.Point(519, 215);
			this.btnCancelar.Margin = new System.Windows.Forms.Padding(4);
			this.btnCancelar.Name = "btnCancelar";
			this.btnCancelar.Size = new System.Drawing.Size(100, 28);
			this.btnCancelar.TabIndex = 70;
			this.btnCancelar.Text = "Cancelar";
			this.btnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.btnCancelar.UseVisualStyleBackColor = true;
			// 
			// btnReporte
			// 
			this.btnReporte.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnReporte.ImageIndex = 1;
			this.btnReporte.Location = new System.Drawing.Point(411, 215);
			this.btnReporte.Margin = new System.Windows.Forms.Padding(4);
			this.btnReporte.Name = "btnReporte";
			this.btnReporte.Size = new System.Drawing.Size(100, 28);
			this.btnReporte.TabIndex = 69;
			this.btnReporte.Text = "Reporte";
			this.btnReporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.btnReporte.UseVisualStyleBackColor = true;
			this.btnReporte.Click += new System.EventHandler(this.btnReporte_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.dtpFecha2);
			this.groupBox1.Controls.Add(this.dtpFecha1);
			this.groupBox1.Controls.Add(this.label8);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new System.Drawing.Point(16, 4);
			this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
			this.groupBox1.Size = new System.Drawing.Size(603, 101);
			this.groupBox1.TabIndex = 68;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "FILTRO DE FECHAS";
			// 
			// dtpFecha2
			// 
			this.dtpFecha2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtpFecha2.Location = new System.Drawing.Point(259, 50);
			this.dtpFecha2.Margin = new System.Windows.Forms.Padding(4);
			this.dtpFecha2.Name = "dtpFecha2";
			this.dtpFecha2.Size = new System.Drawing.Size(131, 22);
			this.dtpFecha2.TabIndex = 51;
			// 
			// dtpFecha1
			// 
			this.dtpFecha1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtpFecha1.Location = new System.Drawing.Point(25, 50);
			this.dtpFecha1.Margin = new System.Windows.Forms.Padding(4);
			this.dtpFecha1.Name = "dtpFecha1";
			this.dtpFecha1.Size = new System.Drawing.Size(131, 22);
			this.dtpFecha1.TabIndex = 52;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.BackColor = System.Drawing.Color.Transparent;
			this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label8.ForeColor = System.Drawing.Color.SteelBlue;
			this.label8.Location = new System.Drawing.Point(256, 32);
			this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(49, 16);
			this.label8.TabIndex = 45;
			this.label8.Text = "Hasta";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.BackColor = System.Drawing.Color.Transparent;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.SteelBlue;
			this.label1.Location = new System.Drawing.Point(23, 31);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(54, 16);
			this.label1.TabIndex = 26;
			this.label1.Text = "Desde";
			// 
			// frmParamKardexArticulo
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
			this.ClientSize = new System.Drawing.Size(637, 252);
			this.Controls.Add(this.groupBox4);
			this.Controls.Add(this.btnCancelar);
			this.Controls.Add(this.btnReporte);
			this.Controls.Add(this.groupBox1);
			this.DoubleBuffered = true;
			this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Margin = new System.Windows.Forms.Padding(4);
			this.Name = "frmParamKardexArticulo";
			this.ShowIcon = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "KARDEX POR PRODUCTO";
			this.Load += new System.EventHandler(this.frmParamKardexArticulo_Load);
			this.groupBox4.ResumeLayout(false);
			this.groupBox4.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox4;
        public System.Windows.Forms.TextBox txtArticulo;
        public System.Windows.Forms.TextBox txtUnArt;
        private System.Windows.Forms.RadioButton rbArt;
        private System.Windows.Forms.RadioButton rbTodosArt;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnReporte;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpFecha2;
        private System.Windows.Forms.DateTimePicker dtpFecha1;
    }
}