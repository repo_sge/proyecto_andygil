﻿namespace SIGEFA.Reportes
{
    partial class frmParamVentasMesArticulo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.btnCancelar = new System.Windows.Forms.Button();
			this.btnReporte = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.cmbAños = new System.Windows.Forms.ComboBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.cmbMes2 = new System.Windows.Forms.ComboBox();
			this.cmbMes1 = new System.Windows.Forms.ComboBox();
			this.label8 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.cmbCriterio = new System.Windows.Forms.ComboBox();
			this.label7 = new System.Windows.Forms.Label();
			this.cmbMoneda = new System.Windows.Forms.ComboBox();
			this.label2 = new System.Windows.Forms.Label();
			this.cmbFormaPago = new System.Windows.Forms.ComboBox();
			this.label9 = new System.Windows.Forms.Label();
			this.cmbEmpresa = new System.Windows.Forms.ComboBox();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.txtArticulo = new System.Windows.Forms.TextBox();
			this.txtUnArt = new System.Windows.Forms.TextBox();
			this.rbArt = new System.Windows.Forms.RadioButton();
			this.rbTodosArt = new System.Windows.Forms.RadioButton();
			this.groupBox1.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnCancelar
			// 
			this.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnCancelar.ImageIndex = 0;
			this.btnCancelar.Location = new System.Drawing.Point(516, 260);
			this.btnCancelar.Margin = new System.Windows.Forms.Padding(4);
			this.btnCancelar.Name = "btnCancelar";
			this.btnCancelar.Size = new System.Drawing.Size(100, 28);
			this.btnCancelar.TabIndex = 66;
			this.btnCancelar.Text = "Cancelar";
			this.btnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.btnCancelar.UseVisualStyleBackColor = true;
			this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
			// 
			// btnReporte
			// 
			this.btnReporte.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnReporte.ImageIndex = 1;
			this.btnReporte.Location = new System.Drawing.Point(408, 260);
			this.btnReporte.Margin = new System.Windows.Forms.Padding(4);
			this.btnReporte.Name = "btnReporte";
			this.btnReporte.Size = new System.Drawing.Size(100, 28);
			this.btnReporte.TabIndex = 65;
			this.btnReporte.Text = "Reporte";
			this.btnReporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.btnReporte.UseVisualStyleBackColor = true;
			this.btnReporte.Click += new System.EventHandler(this.btnReporte_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.cmbAños);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.cmbMes2);
			this.groupBox1.Controls.Add(this.cmbMes1);
			this.groupBox1.Controls.Add(this.label8);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.cmbCriterio);
			this.groupBox1.Controls.Add(this.label7);
			this.groupBox1.Controls.Add(this.cmbMoneda);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.cmbFormaPago);
			this.groupBox1.Controls.Add(this.label9);
			this.groupBox1.Controls.Add(this.cmbEmpresa);
			this.groupBox1.Location = new System.Drawing.Point(12, 12);
			this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
			this.groupBox1.Size = new System.Drawing.Size(603, 140);
			this.groupBox1.TabIndex = 63;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Datos";
			// 
			// cmbAños
			// 
			this.cmbAños.FormattingEnabled = true;
			this.cmbAños.Location = new System.Drawing.Point(297, 50);
			this.cmbAños.Margin = new System.Windows.Forms.Padding(4);
			this.cmbAños.Name = "cmbAños";
			this.cmbAños.Size = new System.Drawing.Size(133, 24);
			this.cmbAños.TabIndex = 60;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.BackColor = System.Drawing.Color.Transparent;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.ForeColor = System.Drawing.Color.SteelBlue;
			this.label4.Location = new System.Drawing.Point(154, 82);
			this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(96, 16);
			this.label4.TabIndex = 59;
			this.label4.Text = "Tipo Articulo";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.BackColor = System.Drawing.Color.Transparent;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.ForeColor = System.Drawing.Color.SteelBlue;
			this.label3.Location = new System.Drawing.Point(294, 28);
			this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(35, 16);
			this.label3.TabIndex = 57;
			this.label3.Text = "Año";
			// 
			// cmbMes2
			// 
			this.cmbMes2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cmbMes2.FormattingEnabled = true;
			this.cmbMes2.Items.AddRange(new object[] {
            "ENERO",
            "FEBRERO",
            "MARZO",
            "ABRIL",
            "MAYO",
            "JUNIO",
            "JULIO",
            "AGOSTO",
            "SEPTIEMBRE",
            "OCTUBRE",
            "NOVIEMBRE",
            "DICIEMBRE"});
			this.cmbMes2.Location = new System.Drawing.Point(157, 50);
			this.cmbMes2.Margin = new System.Windows.Forms.Padding(4);
			this.cmbMes2.Name = "cmbMes2";
			this.cmbMes2.Size = new System.Drawing.Size(132, 24);
			this.cmbMes2.TabIndex = 56;
			// 
			// cmbMes1
			// 
			this.cmbMes1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cmbMes1.FormattingEnabled = true;
			this.cmbMes1.Items.AddRange(new object[] {
            "ENERO",
            "FEBRERO",
            "MARZO",
            "ABRIL",
            "MAYO",
            "JUNIO",
            "JULIO",
            "AGOSTO",
            "SEPTIEMBRE",
            "OCTUBRE",
            "NOVIEMBRE",
            "DICIEMBRE"});
			this.cmbMes1.Location = new System.Drawing.Point(17, 50);
			this.cmbMes1.Margin = new System.Windows.Forms.Padding(4);
			this.cmbMes1.Name = "cmbMes1";
			this.cmbMes1.Size = new System.Drawing.Size(132, 24);
			this.cmbMes1.TabIndex = 55;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.BackColor = System.Drawing.Color.Transparent;
			this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label8.ForeColor = System.Drawing.Color.SteelBlue;
			this.label8.Location = new System.Drawing.Point(155, 28);
			this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(49, 16);
			this.label8.TabIndex = 54;
			this.label8.Text = "Hasta";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.BackColor = System.Drawing.Color.Transparent;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.SteelBlue;
			this.label1.Location = new System.Drawing.Point(14, 28);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(54, 16);
			this.label1.TabIndex = 51;
			this.label1.Text = "Desde";
			// 
			// cmbCriterio
			// 
			this.cmbCriterio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cmbCriterio.FormattingEnabled = true;
			this.cmbCriterio.Items.AddRange(new object[] {
            "ARTICULO",
            "FAMILIA",
            "LINEA",
            "GRUPO",
            "CLIENTE",
            "VENDEDOR"});
			this.cmbCriterio.Location = new System.Drawing.Point(157, 102);
			this.cmbCriterio.Margin = new System.Windows.Forms.Padding(4);
			this.cmbCriterio.Name = "cmbCriterio";
			this.cmbCriterio.Size = new System.Drawing.Size(132, 24);
			this.cmbCriterio.TabIndex = 50;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.BackColor = System.Drawing.Color.Transparent;
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label7.ForeColor = System.Drawing.Color.SteelBlue;
			this.label7.Location = new System.Drawing.Point(15, 83);
			this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(64, 16);
			this.label7.TabIndex = 47;
			this.label7.Text = "Moneda";
			// 
			// cmbMoneda
			// 
			this.cmbMoneda.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cmbMoneda.FormattingEnabled = true;
			this.cmbMoneda.Location = new System.Drawing.Point(18, 102);
			this.cmbMoneda.Margin = new System.Windows.Forms.Padding(4);
			this.cmbMoneda.Name = "cmbMoneda";
			this.cmbMoneda.Size = new System.Drawing.Size(131, 24);
			this.cmbMoneda.TabIndex = 46;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.BackColor = System.Drawing.Color.Transparent;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.ForeColor = System.Drawing.Color.SteelBlue;
			this.label2.Location = new System.Drawing.Point(294, 82);
			this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(114, 16);
			this.label2.TabIndex = 43;
			this.label2.Text = "Forma de pago";
			// 
			// cmbFormaPago
			// 
			this.cmbFormaPago.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cmbFormaPago.FormattingEnabled = true;
			this.cmbFormaPago.Location = new System.Drawing.Point(297, 102);
			this.cmbFormaPago.Margin = new System.Windows.Forms.Padding(4);
			this.cmbFormaPago.Name = "cmbFormaPago";
			this.cmbFormaPago.Size = new System.Drawing.Size(272, 24);
			this.cmbFormaPago.TabIndex = 42;
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.BackColor = System.Drawing.Color.Transparent;
			this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label9.ForeColor = System.Drawing.Color.SteelBlue;
			this.label9.Location = new System.Drawing.Point(15, 144);
			this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(70, 16);
			this.label9.TabIndex = 37;
			this.label9.Text = "Empresa";
			this.label9.Visible = false;
			// 
			// cmbEmpresa
			// 
			this.cmbEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cmbEmpresa.FormattingEnabled = true;
			this.cmbEmpresa.Location = new System.Drawing.Point(18, 166);
			this.cmbEmpresa.Margin = new System.Windows.Forms.Padding(4);
			this.cmbEmpresa.Name = "cmbEmpresa";
			this.cmbEmpresa.Size = new System.Drawing.Size(131, 24);
			this.cmbEmpresa.TabIndex = 36;
			this.cmbEmpresa.Visible = false;
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.txtArticulo);
			this.groupBox4.Controls.Add(this.txtUnArt);
			this.groupBox4.Controls.Add(this.rbArt);
			this.groupBox4.Controls.Add(this.rbTodosArt);
			this.groupBox4.Location = new System.Drawing.Point(13, 160);
			this.groupBox4.Margin = new System.Windows.Forms.Padding(4);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Padding = new System.Windows.Forms.Padding(4);
			this.groupBox4.Size = new System.Drawing.Size(603, 92);
			this.groupBox4.TabIndex = 67;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Por Artículo";
			// 
			// txtArticulo
			// 
			this.txtArticulo.Enabled = false;
			this.txtArticulo.Location = new System.Drawing.Point(258, 52);
			this.txtArticulo.Margin = new System.Windows.Forms.Padding(4);
			this.txtArticulo.Name = "txtArticulo";
			this.txtArticulo.Size = new System.Drawing.Size(329, 22);
			this.txtArticulo.TabIndex = 63;
			// 
			// txtUnArt
			// 
			this.txtUnArt.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtUnArt.Enabled = false;
			this.txtUnArt.Location = new System.Drawing.Point(139, 52);
			this.txtUnArt.Margin = new System.Windows.Forms.Padding(4);
			this.txtUnArt.Name = "txtUnArt";
			this.txtUnArt.Size = new System.Drawing.Size(111, 22);
			this.txtUnArt.TabIndex = 61;
			this.txtUnArt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtUnArt_KeyDown);
			this.txtUnArt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtUnArt_KeyPress);
			// 
			// rbArt
			// 
			this.rbArt.AutoSize = true;
			this.rbArt.BackColor = System.Drawing.Color.Transparent;
			this.rbArt.Location = new System.Drawing.Point(25, 52);
			this.rbArt.Margin = new System.Windows.Forms.Padding(4);
			this.rbArt.Name = "rbArt";
			this.rbArt.Size = new System.Drawing.Size(90, 20);
			this.rbArt.TabIndex = 57;
			this.rbArt.Text = "Un Artículo";
			this.rbArt.UseVisualStyleBackColor = false;
			this.rbArt.CheckedChanged += new System.EventHandler(this.rbArt_CheckedChanged);
			// 
			// rbTodosArt
			// 
			this.rbTodosArt.AutoSize = true;
			this.rbTodosArt.BackColor = System.Drawing.Color.Transparent;
			this.rbTodosArt.Checked = true;
			this.rbTodosArt.Location = new System.Drawing.Point(25, 23);
			this.rbTodosArt.Margin = new System.Windows.Forms.Padding(4);
			this.rbTodosArt.Name = "rbTodosArt";
			this.rbTodosArt.Size = new System.Drawing.Size(140, 20);
			this.rbTodosArt.TabIndex = 54;
			this.rbTodosArt.TabStop = true;
			this.rbTodosArt.Text = "Todos los artículos";
			this.rbTodosArt.UseVisualStyleBackColor = false;
			this.rbTodosArt.CheckedChanged += new System.EventHandler(this.rbTodosArt_CheckedChanged);
			// 
			// frmParamVentasMesArticulo
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
			this.ClientSize = new System.Drawing.Size(631, 299);
			this.Controls.Add(this.groupBox4);
			this.Controls.Add(this.btnCancelar);
			this.Controls.Add(this.btnReporte);
			this.Controls.Add(this.groupBox1);
			this.DoubleBuffered = true;
			this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Margin = new System.Windows.Forms.Padding(4);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmParamVentasMesArticulo";
			this.ShowIcon = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Reporte de Ventas de Articulo por Mes";
			this.Load += new System.EventHandler(this.frmParamVentasMesArticulo_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox4.ResumeLayout(false);
			this.groupBox4.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnReporte;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbMoneda;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbFormaPago;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cmbEmpresa;
        private System.Windows.Forms.ComboBox cmbCriterio;
        private System.Windows.Forms.GroupBox groupBox4;
        public System.Windows.Forms.TextBox txtArticulo;
        public System.Windows.Forms.TextBox txtUnArt;
        private System.Windows.Forms.RadioButton rbArt;
        private System.Windows.Forms.RadioButton rbTodosArt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbMes2;
        private System.Windows.Forms.ComboBox cmbMes1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbAños;
    }
}