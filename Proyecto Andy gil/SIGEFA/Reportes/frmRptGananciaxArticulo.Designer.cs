﻿namespace SIGEFA.Reportes
{
    partial class frmRptGananciaxArticulo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.crvRptGananciaxArticulo = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.SuspendLayout();
            // 
            // crvRptGananciaxArticulo
            // 
            this.crvRptGananciaxArticulo.ActiveViewIndex = -1;
            this.crvRptGananciaxArticulo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crvRptGananciaxArticulo.Cursor = System.Windows.Forms.Cursors.Default;
            this.crvRptGananciaxArticulo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crvRptGananciaxArticulo.Location = new System.Drawing.Point(0, 0);
            this.crvRptGananciaxArticulo.Name = "crvRptGananciaxArticulo";
            this.crvRptGananciaxArticulo.Size = new System.Drawing.Size(391, 357);
            this.crvRptGananciaxArticulo.TabIndex = 1;
            // 
            // frmRptGananciaxArticulo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(391, 357);
            this.Controls.Add(this.crvRptGananciaxArticulo);
            this.Name = "frmRptGananciaxArticulo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reporte Ganancia por Articulo";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);

        }

        #endregion

        public CrystalDecisions.Windows.Forms.CrystalReportViewer crvRptGananciaxArticulo;
    }
}