﻿namespace SIGEFA.Reportes
{
    partial class frmRptGananciaxCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.crvRptGananciaxCliente = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.SuspendLayout();
            // 
            // crvRptGananciaxCliente
            // 
            this.crvRptGananciaxCliente.ActiveViewIndex = -1;
            this.crvRptGananciaxCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crvRptGananciaxCliente.Cursor = System.Windows.Forms.Cursors.Default;
            this.crvRptGananciaxCliente.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crvRptGananciaxCliente.Location = new System.Drawing.Point(0, 0);
            this.crvRptGananciaxCliente.Name = "crvRptGananciaxCliente";
            this.crvRptGananciaxCliente.Size = new System.Drawing.Size(419, 356);
            this.crvRptGananciaxCliente.TabIndex = 1;
            // 
            // frmRptGananciaxCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(419, 356);
            this.Controls.Add(this.crvRptGananciaxCliente);
            this.Name = "frmRptGananciaxCliente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reporte Ganancia por Cliente";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmRptGananciaxCliente_Load);
            this.ResumeLayout(false);

        }

        #endregion

        public CrystalDecisions.Windows.Forms.CrystalReportViewer crvRptGananciaxCliente;
    }
}